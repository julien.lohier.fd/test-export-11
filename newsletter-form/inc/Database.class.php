<?php
class Database
{
    private static $dbName = 'marketplace_api_newsletter';
    private static $dbHost = 'FCXDEVMPSRV1';
    private static $dbUsername = 'devadmin';
    private static $dbUserPassword = 'devadmin';

    private static $cont = null;

    public function __construct() {
        die('Init function is not allowed');
    }

    public static function connect() {
        if (null === self::$cont) {
            try {
                self::$cont =  new PDO('mysql:host='.self::$dbHost.'; dbname='.self::$dbName, self::$dbUsername, self::$dbUserPassword);
            } catch(PDOException $e) {
                die($e->getMessage());
            }
        }
        return self::$cont;
    }

    public static function disconnect() {
        self::$cont = null;
    }
}