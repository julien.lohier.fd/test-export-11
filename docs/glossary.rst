:orphan:

Glossary
========

.. glossary::
   :sorted:

   Partner
		    A *Partner* is the client of the webservice. It is a partner of fnac.com.

   Shop
 		    A shop is the reference to the offers displayed on fnac.com marketplace. Note : a partner can handle one or several shops. 
   
   Server 
 		    The *Server* hosts the webservice. It is the fnac.com marketplace.

   Service
        A *Service* is a generic term for any Api request  that performs a
        specific task. 

   HTTP Specification
        The *Http Specification* is a document that describes the Hypertext
        Transfer Protocol - a set of rules laying out the classic client-server
        request-response communication. The specification defines the format
        used for a request and response as well as the possible HTTP headers
        that each may have. For more information, read the `Http Wikipedia`_
        article or the `HTTP 1.1 RFC`.

   Environment
        An *Environment* is a string (e.g. ``prod`` or ``staging``) that corresponds
        to a specific set of configuration. The same application can be run
        on the same machine using different configuration by running the application
        in different environments. This is useful as it allows a single application
        to have a ``dev`` environment built for debugging and a ``prod`` environment
        that's optimized for speed.


.. _`HTTP Wikipedia`: http://en.wikipedia.org/wiki/Hypertext_Transfer_Protocol
