.. FnacMarketplace API documentation master file, created by
   sphinx-quickstart on Wed Mar 30 14:24:57 2011.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

HOME
====

FnacMarketplace in a nutshell 
-----------------------------

FnacMarketplace API is a tool to connect your own sales application to your FnacMarketplace seller account. It uses the REST WebService protocol to exchange data. The following document will show you how to use this API directly by helping you building your own requests.
     
Available environments
-----------------------

We provide 2 :term:`environments <environment>`, both working on https :

 * Staging : Use this environment only for test purpose. It is available through domain https://partners-test.mp.fnacdarty.com/api.php.
 * Production : When your application works correctly with the staging environment you can contact our :ref:`technical support <support>`. We will then provide you the required information to work on this environment.

What's next ?
-------------

Now you know the basics of our api. You can either dive into the :ref:`api documentation <services>`.

.. admonition:: 
                :ref:`services`
   :class: admonition-see-also admonition seealso

   This section provides a detailed documentation for each FnacMarketplace API Service.

.. admonition:: 
                XSD Schema files
   :class: admonition-see-also admonition seealso
   
   You can download the latest XSD files of our services by `clicking here <https://partners-test.mp.fnacdarty.com/docs/api/2.6/download/fnac_mp_api_xsd.tgz>`__.

.. admonition:: 
                :ref:`test_environment_tools`
   :class: admonition-see-also admonition seealso
   
   This section will show you how to create and update orders on your account for test purpose.

.. toctree::
   :hidden:
   
   services/index
   index-table
   services/testenvironmenttools

Quick links
------------

 * :doc:`Contact <support>`
 * `Search <search.html>`_
 * :doc:`Faq <faq>`
 * :doc:`Glossary <glossary>`
 * :doc:`Error codes <services/errors/errors>`
 * :doc:`Changelog <changelog>`

.. toctree::
   :hidden:

   support
   faq
   glossary

