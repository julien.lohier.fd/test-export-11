Fnac support
============

.. _support:

Technical support
-----------------

You can contact our technical support by phone or mail from monday to friday 9:30pm - 6am GMT+2 .

 * Mail : dl-fd-api.mp@fnac.com
 * Phone number : +33155212825