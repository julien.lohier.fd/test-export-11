How to use this documentation
=============================

Each service has its own page which describes the following points :

 * The service purpose
 * The process to use the service
 * Detailed xml description about request and response
 
The following image is an exemple of xml description for a request :

.. image:: ApiHowToUse.png