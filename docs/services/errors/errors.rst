Error Codes
===========

+------------------------+--------------------------------------------------------------------------------------------------+
|          Code          |                                    Description                                                   |
+========================+==================================================================================================+
| ERR_001                | **Access denied to this order**                                                                  |
|                        |                                                                                                  |
|                        | You are trying to process an order which is not related to your shop account.                    |
+------------------------+--------------------------------------------------------------------------------------------------+
| ERR_002                | **Order status is not valid for action confirm_to_send.**                                        |
|                        |                                                                                                  |
|                        | This error occurs when you submit an invalid attribute action                                    |
+------------------------+--------------------------------------------------------------------------------------------------+
| ERR_003                | **Order status is not valid for action accept_order.**                                           |
|                        |                                                                                                  |
|                        | This error occurs when the order status doesn't allow the update (The order is                   |
|                        | in a later status than the update to perform).                                                   |
+------------------------+--------------------------------------------------------------------------------------------------+
| ERR_004                | **Number of order_detail(s) in xml not corresponding number of order_detail(s) of order.**       |
|                        |                                                                                                  |
|                        | This error occurs when you request an order update. You must specify the action you want to      |
|                        | perform on every Order Details related to the order. The Order update is one shot action, it is  |
|                        | not possible to update only a part of the Order details.                                         |
+------------------------+--------------------------------------------------------------------------------------------------+
| ERR_005                | **Order Detail status is not valid for action confirm_to_send.**                                 |
|                        |                                                                                                  |
|                        | This error occurs when the order detail status doesn't allow the update (The order detail        |
|                        | status is not valid for the order action update).                                                |
+------------------------+--------------------------------------------------------------------------------------------------+
| ERR_006                | **Order Detail status is not valid for action accept_order.**                                    |
|                        |                                                                                                  |
|                        | This error occurs when the order detail status doesn't allow the update (The order detail        |
|                        | status is not valid for the order action update).                                                |
+------------------------+--------------------------------------------------------------------------------------------------+
| ERR_007                | **Order not found.**                                                                             |
|                        |                                                                                                  |
|                        | The order id doesn't exist or is incorrect. Please check and submit the request again.           |
+------------------------+--------------------------------------------------------------------------------------------------+
| ERR_008                | **Order action inconsistency.**                                                                  |
|                        |                                                                                                  |
|                        | This error may result from an internal Fnac error. Please resubmit your data. If this error      |
|                        | persists, please contact us for technical support (info.mp@fnac.com).                            |
+------------------------+--------------------------------------------------------------------------------------------------+
| ERR_009                | **Order Detail not found in Fnac database.**                                                     |
|                        |                                                                                                  |
|                        | You have attempted to match against existing order detail using just a standard order detail     |
|                        | identifier that does not exist in your Fnac shop.                                                |
|                        | If you entered the wrong standard order detail identifier, you can correct it and resubmit       |
|                        | the order again.                                                                                 |
|                        | If the standard order detail identifier is correct, you will contact info.mp@fnac.com            |
|                        | to add your reference to the Fnac catalog.                                                       |
+------------------------+--------------------------------------------------------------------------------------------------+
| ERR_010                | **Order Detail not matching with the order.**                                                    |
|                        |                                                                                                  |
+------------------------+--------------------------------------------------------------------------------------------------+
| ERR_011                | **This product does not exist in our catalog.**                                                  |
|                        |                                                                                                  |
|                        | You have attempted to match against existing products using just a standard product_reference    |
|                        | type (Ean / Isbn / PartnerId / FnacId) that does not exist in our Fnac catalog.                  |
|                        | If you entered the wrong standard product_reference, you can correct it and resubmit             |
|                        | the offer again.                                                                                 |
|                        | If the standard  product_reference is correct, you will have to contact info.mp@fnac.com to add  |
|                        | your reference into the Fnac catalog.                                                            |
+------------------------+--------------------------------------------------------------------------------------------------+
| ERR_012                | **This product can't be inserted into our catalog.**                                             |
|                        |                                                                                                  |
|                        | The product_reference you tried to use can't be inserted in our catalog. It means that the       |
|                        | product doesn't have the right status in our Fnac catalog to be available on the marketplace.    |
|                        | Please contact us to solve the problem at info.mp@fnac.com                                       |
+------------------------+--------------------------------------------------------------------------------------------------+
| ERR_013                | **No quantity was specified, set to default (1).**                                               |
|                        |                                                                                                  |
|                        | Quantity value must be a valid integer value.                                                    |
|                        | This error occurs when the quantity value is null or not specified. We assume the quantity       |
|                        | is set to default (integer 1).                                                                   |
|                        | To resolve this error, please resubmit your data with the right integer quantity value.          |
+------------------------+--------------------------------------------------------------------------------------------------+
| ERR_014                | **The quantity was superior to 9999, set to (9999).**                                            |
|                        |                                                                                                  |
|                        | This error occurs when the available inventory you declare is greater than 9999.                 |
|                        | If you entered the wrong standard quantity, you can correct it and resubmit your data            |
|                        | with a quantity less than or equal to 9999.                                                      |
|                        | If the standard quantity is correct , the default value will be set for quantity (9999)          |
+------------------------+--------------------------------------------------------------------------------------------------+
| ERR_015                | **This product is not authorized for your shop.**                                                |
|                        |                                                                                                  |
|                        | The product_reference you tried to use is not authorized to be inserted for your shop.           |
|                        | It means your shop hasn't been associated to your reference catalog in the Fnac catalog.         |
|                        | Please contact us to solve the problem at info.mp@fnac.com                                       |
+------------------------+--------------------------------------------------------------------------------------------------+
| ERR_016                | **This product is empty.**                                                                       |
|                        |                                                                                                  |
|                        | The product identifier value must be a non empty value.                                          |
|                        | This error occurs when the product identifier value is null or not specified.                    |
|                        | To resolve this error, please resubmit your data with the right integer product identifier value.|
+------------------------+--------------------------------------------------------------------------------------------------+
| ERR_017                | **This price is negative or inferior to 0.90 euro(s).**                                          |
|                        |                                                                                                  |
|                        | This error occurs when you indicate a price with a value less than 0.9 or negative.              |
|                        | For a price to be valid, it must be equal or greater than 0.9 euro.                              |
|                        | To resolve this error, please resubmit your data with a price value greater than 0.9 euro.       |
+------------------------+--------------------------------------------------------------------------------------------------+
| ERR_018                | **This offer reference is empty.**                                                               |
|                        |                                                                                                  |
|                        | The offer_reference value must be a non empty value.                                             |
|                        | This error occurs when the offer_reference value is null or not specified.                       |
|                        | To resolve this error, please resubmit your data with the right integer offer_reference value.   |
+------------------------+--------------------------------------------------------------------------------------------------+
| ERR_019                | **This offer reference is empty.**                                                               |
|                        |                                                                                                  |
+------------------------+--------------------------------------------------------------------------------------------------+
| ERR_020                | **Offer status can't change in update mode.**                                                    |
|                        |                                                                                                  |
+------------------------+--------------------------------------------------------------------------------------------------+
| ERR_021                | **Offer status of the offer is empty.**                                                          |
|                        |                                                                                                  |
+------------------------+--------------------------------------------------------------------------------------------------+
| ERR_022                | **The change status date is not a date format.**                                                 |
|                        |                                                                                                  |
+------------------------+--------------------------------------------------------------------------------------------------+
| ERR_023                | **Batch not found**                                                                              |
|                        |                                                                                                  |
|                        | This error occurs when you try to request Batch status service with an unknown batch identifier  |
|                        | To resolve this error, please resubmit your data with a right batch identifier.                  |
+------------------------+--------------------------------------------------------------------------------------------------+
| ERR_024                | **Batch not found**                                                                              |
|                        |                                                                                                  |
|                        | This error occurs when you try to request Batch status service with a batch identifier which     |
|                        | is not related to your account. To resolve this error, please resubmit your data with a right    |
|                        | batch identifier.                                                                                |
+------------------------+--------------------------------------------------------------------------------------------------+
| ERR_025                | **Product state can't be changed.**                                                              |
|                        |                                                                                                  |
|                        | Once an offer is created with a state, you can't change this state.                              |
|                        | The only solution is to create a new offer with a new SKU.                                       |
+------------------------+--------------------------------------------------------------------------------------------------+
| ERR_026                | **Product not found.**                                                                           |
|                        |                                                                                                  |
|                        | We got a problem while retrieving the product you reference in your offers_update request.       |
|                        | It appears that this product doesn't exist in our catalog. Please contact us to resolve this     |
|                        | point at info.mp@fnac.com                                                                        |
+------------------------+--------------------------------------------------------------------------------------------------+
| ERR_027                | **Product not found.**                                                                           |
|                        |                                                                                                  |
|                        | You can't create an offer by only using an offer_reference, we also need a product_reference     |
|                        | node. You are trying to add a product without giving a product_reference node. We need this node |
|                        | to match your new offer with your product. With no product_reference specified, the existing     |
|                        | offer will be updated.                                                                           |
|                        | To resolve this error, please add a product_reference node.                                      |
+------------------------+--------------------------------------------------------------------------------------------------+
| ERR_028                | **Product not found.**                                                                           |
|                        |                                                                                                  |
|                        | Order status is not valid for action update.                                                     |
|                        | This error occurs when you submit an invalid attribute action.                                   |
|                        | The order must be on "Shipped" status to perform this Order Update action.                       |
+------------------------+--------------------------------------------------------------------------------------------------+
| ERR_029                | **Order Detail status is not valid for action update.**                                          |
|                        |                                                                                                  |
|                        | This error occurs when the order detail status doesn't allow the update (The order detail        |
|                        | status is not valid for the order action update).                                                |
|                        |                                                                                                  |
|                        | The order must be on "Shipped" status to perform this Order Update action.                       |
+------------------------+--------------------------------------------------------------------------------------------------+
| ERR_030                | **Product state can't be found.**                                                                |
|                        |                                                                                                  |
|                        | You are trying to add a product without giving a product_state node.                             |
|                        | To resolve this error, please add a product_state node.                                          |
+------------------------+--------------------------------------------------------------------------------------------------+
| ERR_031                | **EAN structure isn't valid.**                                                                   |
|                        |                                                                                                  |
|                        | An EAN (UPC) should contain digits only and must be 10-13 characters long                        |
+------------------------+--------------------------------------------------------------------------------------------------+
| ERR_032                | **Product can't be sold in used state.**                                                         |
|                        |                                                                                                  |
|                        | The product_reference you tried to use can't be sold in used state. It means that the product    |
|                        | must be sold with a new state.                                                                   |
+------------------------+--------------------------------------------------------------------------------------------------+
| ERR_033                | **You can't use this partner id.**                                                               |
|                        |                                                                                                  |
|                        | You can't use the specified product_reference.                                                   |
|                        | Please contact us to resolve this point at info.mp@fnac.com                                      |
+------------------------+--------------------------------------------------------------------------------------------------+
| ERR_034                | **The Adherent Price is not valid.**                                                             |
|                        |                                                                                                  |
|                        | The Adherent Price must be superior to 0.90€ and inferior to 20000€                              |
+------------------------+--------------------------------------------------------------------------------------------------+
| ERR_035                | **The Adherent Price can't be greater than the default price.**                                  |
|                        |                                                                                                  |
|                        | The Adherent Price must be superior to the default price.                                        |
+------------------------+--------------------------------------------------------------------------------------------------+
| ERR_093                | **Invalid showcase number.**                                                                     |
|                        |                                                                                                  |
|                        | This field must be set with a value between 1 and 100.                                           |
|                        | To resolve this error, change the showcase number.                                               |
+------------------------+--------------------------------------------------------------------------------------------------+
| ERR_094                | **Invalid characters in SKU.**                                                                   |
|                        |                                                                                                  |
|                        | You have invalid characters in the SKU, SKU can contains alphanumeric characters and space.      |
|                        | To resolve this error, change your SKU.                                                          |
+------------------------+--------------------------------------------------------------------------------------------------+
| ERR_095                | **Duplicated SKU.**                                                                              |
|                        |                                                                                                  |
|                        | This error occurs when you submit the same SKU twice in a file. The first one is processed and   |
|                        | the next one isn't.                                                                              |
|                        | To resolve this error, remove the duplicated SKU in your file.                                   |
+------------------------+--------------------------------------------------------------------------------------------------+
| ERR_096                | **Validation failed: XML.**                                                                      |
|                        |                                                                                                  |
|                        | This error occurs when you submit invalid XML data according to the XSD file. The structure      |
|                        | isn't valid.                                                                                     |
|                        | To resolve this error, try to validate your data with the last version of XSD file provided by   |
|                        | Fnac API Marketplace.                                                                            |
+------------------------+--------------------------------------------------------------------------------------------------+
| ERR_097                | **Authentication failed: one of the parameters (partner_id , key, shop_id) is invalid.**         |
|                        |                                                                                                  |
|                        | This error occurs when you try to submit data with wrong identifiers. To resolve this error,     |
|                        | please resubmit your data with valid partner_id / key / shop_id set of identifiers.              |
|                        | If you still get this error , please contact our support team (info.mp@fnac.com).                |
+------------------------+--------------------------------------------------------------------------------------------------+
| ERR_098                | **Authentication Failure: token session time is expired, please authenticate.**                  |
|                        |                                                                                                  |
|                        | This error occurs when your token value (authentication token) is outdated and no longer valid.  |
|                        | To resolve this error, please re-authenticate with the Auth service and get a new valid token.   |
+------------------------+--------------------------------------------------------------------------------------------------+
| ERR_099                | **This Service is not available.**                                                               |
|                        |                                                                                                  |
+------------------------+--------------------------------------------------------------------------------------------------+
| ERR_100                | **Functional Error.**                                                                            |
|                        |                                                                                                  |
|                        | This error may be result of an internal Fnac error. Please resubmit your data. If this error     |
|                        | persists please contact us for technical support (info.mp@fnac.com).                             |
+------------------------+--------------------------------------------------------------------------------------------------+
| ERR_101                | **This service is not available for this request method.**                                       |
|                        |                                                                                                  |
+------------------------+--------------------------------------------------------------------------------------------------+
| ERR_102                | **Partner is not authenticated. Please authenticate with 'auth' service.**                       |
|                        |                                                                                                  |
+------------------------+--------------------------------------------------------------------------------------------------+
| ERR_103                | **The product reference doesn't match with the offer reference.**                                |
|                        |                                                                                                  |
|                        | It means you are attempting to create an offer with an already existing SKU and/or product       |
|                        | reference. and now you try tochange the associated product which isn't possible.                 |
+------------------------+--------------------------------------------------------------------------------------------------+
| ERR_104                | **Service not found.**                                                                           |
|                        |                                                                                                  |
|                        | This error occurs when you requesting an unknown service. To resolve this error, please resubmit |
|                        | your data on a valid API Fnac Marketplace service.                                               |
+------------------------+--------------------------------------------------------------------------------------------------+
| ERR_105                | **The limit of requested elements is reached, try with fewer elements.**                         |
|                        |                                                                                                  |
|                        | This error occurs when the limit of requested elements for services is reached (see the          |
|                        | documentation for further details → pagination). To resolve this error :                         |
|                        | * Resubmit your data with more restrictive constraint query                                      |
|                        | or resubmit your data with the pagination system.                                                |
+------------------------+--------------------------------------------------------------------------------------------------+
| ERR_106                | **No offers found for this product.**                                                            |
|                        |                                                                                                  |
+------------------------+--------------------------------------------------------------------------------------------------+
| ERR_107                | **You reached the maximum API calls available in one hour.**                                     |
|                        |                                                                                                  |
|                        | This error occurs when the number of request per hour has exceeded the limits of the Fnac        |
|                        | marketplace API (see documentation → limits for further information).                            |
|                        | To resolve this error, please resubmit your data later (the next hour).                          |
+------------------------+--------------------------------------------------------------------------------------------------+
| ERR_108                | **Product Reference can't be changed.**                                                          |
|                        |                                                                                                  |
|                        | You can't change the product associated to an existing offer.                                    |
|                        | @DEPRECATED.                                                                                     |
+------------------------+--------------------------------------------------------------------------------------------------+
| ERR_109                | **Order status is not valid for action refund_order.**                                           |
|                        |                                                                                                  |
|                        | This order is not available to be refund.                                                        |
+------------------------+--------------------------------------------------------------------------------------------------+
| ERR_110                | **Sorry, this product is not sellable at the moment.**                                           |
|                        |                                                                                                  |
|                        | The product is not sellable right now, it might be waiting a publishing validation at our end.   |
+------------------------+--------------------------------------------------------------------------------------------------+
| ERR_111                | **Sorry, the product is no more sellable at the moment.**                                        |
|                        |                                                                                                  |
|                        | The product was formerly sellable. It might be waiting a new publishing validation at our end.   |
+------------------------+--------------------------------------------------------------------------------------------------+
| ERR_112                | **Refund was rejected because you do not have enough money on your balance for it.               |
|                        | You can send a check to the client for refund**                                                  |
|                        |                                                                                                  |
|                        | Refund was rejected because you do not have enough funds on your balance.                        |
|                        | Please contact us at info.mp@fnac.com for further information.                                   |
+------------------------+--------------------------------------------------------------------------------------------------+
| ERR_113                | **Refund : internal error.**                                                                     |
|                        |                                                                                                  |
|                        | The application occurred an error when trying to refund this order. Try to refund it using the   |
|                        | seller interface or contact us at info.mp@fnac.com.                                              |
+------------------------+--------------------------------------------------------------------------------------------------+
| ERR_114                | **Service Message : The Description must be set.**                                               |
|                        |                                                                                                  |
|                        | You need to provide a description to create the message. Please try again by setting the         |
|                        | description                                                                                      |
+------------------------+--------------------------------------------------------------------------------------------------+
| ERR_115                | **Service Message : Message not found.**                                                         |
|                        |                                                                                                  |
|                        | We can't retrieve the message you want to request. Try with different parameters.                |
+------------------------+--------------------------------------------------------------------------------------------------+
| ERR_116                | **Service Message : Message answer is already set.**                                             |
|                        |                                                                                                  |
|                        | You already answered this message.                                                               |
+------------------------+--------------------------------------------------------------------------------------------------+
| ERR_117                | **Service Message [ORDER] : To send a message to the callcenter , the order must be in a         |
|                        | claim state.**                                                                                   |
|                        |                                                                                                  |
|                        | You tried to send a message to the callcenter but the order doesn't have a any claim. You need   |
|                        | to create a claim/incident before trying to send a message to the callcenter.                    |
+------------------------+--------------------------------------------------------------------------------------------------+
| ERR_118                | **Service Message [ORDER] : the message_to description is mandatory**                            |
|                        |                                                                                                  |
|                        | The message_to element is not set.                                                               |
+------------------------+--------------------------------------------------------------------------------------------------+
| ERR_119                | **Service Message [OFFER] : The message has no offer associated with.**                          |
|                        |                                                                                                  |
|                        | The message you tried to create isn't associated to an offer.                                    |
+------------------------+--------------------------------------------------------------------------------------------------+
| ERR_120                | **Service Pricing : Product not found.**                                                         |
|                        |                                                                                                  |
|                        | The requested product doesn't exist.                                                             |
+------------------------+--------------------------------------------------------------------------------------------------+
| ERR_121                | **[search] Query must be superior to 3 chars.**                                                  |
|                        |                                                                                                  |
|                        | You have to provide a search query with at least 3 characters.                                   |
+------------------------+--------------------------------------------------------------------------------------------------+
| ERR_122                | **[search] We can't find the category given.**                                                   |
|                        |                                                                                                  |
|                        | The selected category you use for this search can't be found. Please check the documentation     |
|                        | to see which categories are available.                                                           |
+------------------------+--------------------------------------------------------------------------------------------------+
| ERR_123                | **[search] Something goes wrong with the search (generic error).**                               |
|                        |                                                                                                  |
|                        | A problem appear when we tried to search your query. It could be a problem of the search engine  |
|                        | (like host can't be found, etc ). Please try again later or contact us for technical support     |
|                        | (info.mp@fnac.com).                                                                              |
+------------------------+--------------------------------------------------------------------------------------------------+
| ERR_124                | **[search] Wrong product_id_type given.**                                                        |
|                        |                                                                                                  |
|                        | The product_id_type you use can't be found. Please check the documentation to see witch          |
|                        | product_id_type are available.                                                                   |
+------------------------+--------------------------------------------------------------------------------------------------+
| ERR_125                | **[search] The retrieved product isn't authorized for your shop.**                               |
|                        |                                                                                                  |
|                        | The product you try to search is not authorized to be inserted for your shop. It means your      |
|                        | shop hasn't been associated to your reference in the Fnac catalog.                               |
|                        | Please contact us to solve the problem at info.mp@fnac.com.                                      |
+------------------------+--------------------------------------------------------------------------------------------------+
| ERR_126                | **[search] The retrieved product is authorized but can't be insertable.**                        |
|                        |                                                                                                  |
|                        | The product you tried to search isn't insertable in our catalog.                                 |
|                        | Please contact us to solve the problem at info.mp@fnac.com.                                      |
+------------------------+--------------------------------------------------------------------------------------------------+
| ERR_127                | **[search] Product can't be sold (generic error).**                                              |
|                        |                                                                                                  |
|                        | The product you tried to search can't be sold. Maybe it has some restriction.                    |
|                        | Please contact us to solve the problem at info.mp@fnac.com.                                      |
+------------------------+--------------------------------------------------------------------------------------------------+
| ERR_128                | **[ClientOrderComment Update] Comment not found.**                                               |
|                        |                                                                                                  |
|                        | The client order comment was not found in our database.                                          |
+------------------------+--------------------------------------------------------------------------------------------------+
| ERR_129                | **[ClientOrderComment Update] Reply is null or empty.**                                          |
|                        |                                                                                                  |
|                        | Reply message must be set. You tried to set an empty message.                                    |
+------------------------+--------------------------------------------------------------------------------------------------+
| ERR_130                | **[OffersUpdate] This product is only sellable in collection state.**                            |
|                        |                                                                                                  |
|                        | The product you tried to insert is only sellable in collection state. Try to reinsert your       |
|                        | product with a state different from new (11).                                                    |
+------------------------+--------------------------------------------------------------------------------------------------+
| ERR_131                | **[ShopUpdate] Date not valid.**                                                                 |
|                        |                                                                                                  |
|                        | Date format is not valid, try with ATOM format date.                                             |
+------------------------+--------------------------------------------------------------------------------------------------+
| ERR_132                | **[ShopUpdate] Start date must be greater than or equal to the current date.**                   |
|                        |                                                                                                  |
|                        | The date that you tried to update must be greater than or equal to the current date.             |
+------------------------+--------------------------------------------------------------------------------------------------+
| ERR_133                | **[ShopUpdate] The end date must be strictly greater than the start date.**                      |
|                        |                                                                                                  |
|                        | The date you tried to update must be strictly greater than the start date.                       |
+------------------------+--------------------------------------------------------------------------------------------------+
| ERR_134                | **[ShopUpdate] Seller must be professional to be able to change name.**                          |
|                        |                                                                                                  |
|                        | If you are a non-professional shop, you are not allowed to change your shop name. If you want to,|
|                        | upgrade with professional advantages by subscribing a professional account.                      |
+------------------------+--------------------------------------------------------------------------------------------------+
| ERR_135                | **[ShopUpdate] Shop name must be at least 4 characters and maximum 11.**                         |
|                        |                                                                                                  |
|                        | The string name you tried to update  must be at least 4 characters and maximum 11.               |
+------------------------+--------------------------------------------------------------------------------------------------+
| ERR_136                | **[ShopUpdate] Shop name can contain only alphanumeric characters.**                             |
|                        |                                                                                                  |
|                        | The string name you tried to update can contain only alphanumeric characters.                    |
+------------------------+--------------------------------------------------------------------------------------------------+
| ERR_137                | **[ShopUpdate] Sorry! the shop name is already used by another shop,                             |
|                        | please to choose another one.**                                                                  |
|                        |                                                                                                  |
|                        | The shop name you tried to update is already used by another shop.                               |
+------------------------+--------------------------------------------------------------------------------------------------+
| ERR_138                | **[ShopUpdate] E-mail is not valid.**                                                            |
|                        |                                                                                                  |
|                        |  The email you tried to update is no valid, try to validate your email field before to update it.|
+------------------------+--------------------------------------------------------------------------------------------------+
| ERR_139                | **[ShopUpdate] E-mail is already used.**                                                         |
|                        |                                                                                                  |
|                        | The email you tried to update is already used. Try with a new one.                               |
+------------------------+--------------------------------------------------------------------------------------------------+
| ERR_140                | **[ShopUpdate] E-mail should not be longer than 255 characters.**                                |
|                        |                                                                                                  |
|                        | The email you tried to update is not valid. E-mail should not be longer than 255 characters.     |
+------------------------+--------------------------------------------------------------------------------------------------+
| ERR_141                | **[ShopUpdate] Phone number is not valid.**                                                      |
|                        |                                                                                                  |
|                        | The phone number you tried to update is not valid.                                               |
+------------------------+--------------------------------------------------------------------------------------------------+
| ERR_142                | **[ShopUpdate] Fax number is invalid.**                                                          |
|                        |                                                                                                  |
|                        | The fax number you tried to update is not valid.                                                 |
+------------------------+--------------------------------------------------------------------------------------------------+
| ERR_143                | **[ShopUpdate] An address line must not exceed 255 characters.**                                 |
|                        |                                                                                                  |
|                        | The address you tried to update is not valid.                                                    |
+------------------------+--------------------------------------------------------------------------------------------------+
| ERR_144                | **[ShopUpdate] ZipCode code must be between 5 and 8 characters.**                                |
|                        |                                                                                                  |
|                        | The zipcode you tried to update is not valid. ZipCode code must contain between 5 and 8          |
|                        | characters.                                                                                      |
+------------------------+--------------------------------------------------------------------------------------------------+
| ERR_145                | **[ShopUpdate] Country not found.**                                                              |
|                        |                                                                                                  |
|                        | Country not found in our database.                                                               |
+------------------------+--------------------------------------------------------------------------------------------------+
| ERR_146                | **XML Schema Validation failed.**                                                                |
|                        |                                                                                                  |
|                        | XML content does not comply with the XSD schema definition.                                      |
+------------------------+--------------------------------------------------------------------------------------------------+
| ERR_147                | **[ClientOrderComment Update] Already replied.**                                                 |
|                        |                                                                                                  |
|                        | You already sent a reply to this comment.  Editing replies is not possible                       |
+------------------------+--------------------------------------------------------------------------------------------------+
| ERR_148                | **This product cannot be sold in this product state.**                                           |
|                        |                                                                                                  |
|                        | Selling this product with the submitted state is not allowed.                                    |
+------------------------+--------------------------------------------------------------------------------------------------+
| ERR_149                | **SKU not found.**                                                                               |
|                        |                                                                                                  |
|                        | When using quick mode, the submitted sku could not be found and therefore could not be updated.  |
+------------------------+--------------------------------------------------------------------------------------------------+
| ERR_150                | **SKU not found.**                                                                               |
|                        |                                                                                                  |
|                        | An error occured while processing this batch. Please submit it again.                            |
|                        | The queried batch could not be processed. You can resend this batch through another offersUpdate |
|                        | call.                                                                                            |
+------------------------+--------------------------------------------------------------------------------------------------+
| ERR_163                | **Logistic type not found.**                                                                     |
|                        |                                                                                                  |
|                        | The logistic type id doesn't exist or is incorrect. Please check and submit the request again.   |
+------------------------+--------------------------------------------------------------------------------------------------+
| ERR_166                | **Client order comment not found.**                                                              |
|                        |                                                                                                  |
|                        | The client order comment id doesn't exist or is incorrect. Please check and submit the request   |
|                        | again.                                                                                           |
+------------------------+--------------------------------------------------------------------------------------------------+
| ERR_167                | **Order not found.**                                                                             |
|                        |                                                                                                  |
|                        | The provided offer_fnac_id, offer_seller_id or product_fnac_id doesn't exist or is incorrect.    |
|                        | We could not find any order with it. Please check and submit the request again.                  |
+------------------------+--------------------------------------------------------------------------------------------------+
| ERR_168                | **Incident not found.**                                                                          |
|                        |                                                                                                  |
|                        | The provided incident_id doesn't exist or is incorrect.                                          |
|                        | We could not find any incident with it. Please check and submit the request again.               |
+------------------------+--------------------------------------------------------------------------------------------------+
| ERR_169                | **Missing or not found tracking company name.**                                                  |
|                        |                                                                                                  |
|                        | When shipping the order with action "confirm_to_send", or updating with action "update",         |
|                        | the tracking_company field is mandatory whenever the tracking_number is filled,                  |
|                        | but when the tracking_company field is filled, the tracking_number is optional.                  |
+------------------------+--------------------------------------------------------------------------------------------------+
| ERR_170                | **Incorrect character encoding in product description**                                          |
|                        |                                                                                                  |
|                        | The description provided contains a badly encoded character. Please correct and submit your      |
|                        | request again.                                                                                   |
+------------------------+--------------------------------------------------------------------------------------------------+
| ERR_171                | **Promotion type not found.**                                                                    |
|                        |                                                                                                  |
|                        | The provided promotion type doesn't exist or is incorrect.                                       |
|                        | Please check the promotion types available in the documentation and submit the request again.    |
+------------------------+--------------------------------------------------------------------------------------------------+
| ERR_172                | **Sales period reference required for promotion of type Sales.**                                 |
|                        |                                                                                                  |
|                        | In order to create a promotion of type Sales on an offer you must provide the reference of       |
|                        | the matching Sales Period. You can find this reference by calling the SalesPeriodQuery.          |
+------------------------+--------------------------------------------------------------------------------------------------+
| ERR_173                | **Sales Period not found or promotion's dates do not match.**                                    |
|                        |                                                                                                  |
|                        | The provided Sales Period reference doesn't exist or is incorrect.                               |
|                        | Please check the documentation of OffersUpdate and/or SalesPeriodQuery.                          |
+------------------------+--------------------------------------------------------------------------------------------------+
| ERR_174                | **Sales period reference not allowed.**                                                          |
|                        |                                                                                                  |
|                        | Sales period reference is not allowed for this type of promotion.                                |
+------------------------+--------------------------------------------------------------------------------------------------+
| ERR_175                | **Discount settings not allowed.**                                                               |
|                        |                                                                                                  |
|                        | Discount settings are not allowed for promotion of type FreeShipping.                            |
+------------------------+--------------------------------------------------------------------------------------------------+
| ERR_176                | **Promotion code too long or invalid.**                                                          |
|                        |                                                                                                  |
|                        | The trigger_promotion_code max length is 50 characters and must be alphanumeric.                 |
+------------------------+--------------------------------------------------------------------------------------------------+
| ERR_177                | **Trigger quantity must be a positive integer.**                                                 |
|                        |                                                                                                  |
|                        | The provided trigger_cart MinSeller/OfferQuantity is lower than 2 or is not an integer.          |
+------------------------+--------------------------------------------------------------------------------------------------+
| ERR_178                | **Trigger cart element invalid.**                                                                |
|                        |                                                                                                  |
|                        | The trigger_cart element is invalid for this type of promotion.                                  |
+------------------------+--------------------------------------------------------------------------------------------------+
| ERR_179                | **Triggers not allowed.**                                                                        |
|                        |                                                                                                  |
|                        | Triggers are not allowed for this promotion type.                                                |
+------------------------+--------------------------------------------------------------------------------------------------+
| ERR_180                | **Discount settings not found.**                                                                 |
|                        |                                                                                                  |
|                        | Discount settings must be set for this type of promotion.                                        |
+------------------------+--------------------------------------------------------------------------------------------------+
| ERR_181                | **Discount percentage invalid.**                                                                 |
|                        |                                                                                                  |
|                        | For a percentage discount you must provide a positive integer lower than 100 as value.           |
+------------------------+--------------------------------------------------------------------------------------------------+
| ERR_182                | **Discount value invalid.**                                                                      |
|                        |                                                                                                  |
|                        | The discount's value make your price lower than the allowed minimum of 0.90€.                    |
+------------------------+--------------------------------------------------------------------------------------------------+
| ERR_183                | **Promotion's start date must be before its end date.**                                          |
|                        |                                                                                                  |
|                        | The provided dates for this promotion are incorrect.                                             |
+------------------------+--------------------------------------------------------------------------------------------------+
| ERR_184                | **Trigger customer type not set or invalid.**                                                    |
|                        |                                                                                                  |
|                        | The customer type trigger must be set.                                                           |
+------------------------+--------------------------------------------------------------------------------------------------+
| ERR_185                | **Both trigger cart type and value must be set.**                                                |
|                        |                                                                                                  |
|                        | One of trigger cart type or value could not be retrieved.                                        |
+------------------------+--------------------------------------------------------------------------------------------------+
| ERR_186                | **Promotion duration is too long.**                                                              |
|                        |                                                                                                  |
|                        | Promotion duration is too long. Flash sale duration is limited to 7 days.                        |
+------------------------+--------------------------------------------------------------------------------------------------+
