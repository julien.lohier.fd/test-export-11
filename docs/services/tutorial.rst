Tutorial - From the creation of an offer to the shipping of an order
====================================================================

The purpose of this tutorial is to show how to construct the main XML requests needed to manage a shop through the Fnac Marketplace API.
At the end of this tutorial you will know how to manage your catalog and how to handle orders.
We are going to create a catalog of 3 products, and update 2 orders.

Authentication - https://partners-test.mp.fnacdarty.com/api.php/auth
-------------------------------------------------------------------

The first thing to do is to authenticate to our API and get a token.

**Request**

.. code-block:: xml

    <?xml version="1.0" encoding="utf-8"?>
    <auth xmlns='http://www.fnac.com/schemas/mp-dialog.xsd'>
      <partner_id>26FAB269-7094-B0C3-4D82-7CFCAF627519</partner_id>
      <shop_id>307D5158-4596-EAD7-8103-9F7D9804E5D4</shop_id>
      <key>8DBB1124-A2CF-92C7-04C8-8115D444ECE7</key>
    </auth>

**Response**

.. code-block:: xml

    <?xml version="1.0" encoding="utf-8"?>
    <auth_response status="OK" xmlns="http://www.fnac.com/schemas/mp-dialog.xsd">
      <token>4D859AC6-24EA-23DD-9D97-6A75DE843433</token>
      <validity>2013-05-13T11:03:28+02:00</validity>
      <version>2.1.0</version>
    </auth_response>

We are now going to use the returned token to call the services.
Using the same token until its expiration could make you gain some performance and processing speed as you would not repeat an authentication request at each of your calls.

We'll assume for the rest of the tutorial that we have a valid token for all the described services.


Offers update - https://partners-test.mp.fnacdarty.com/api.php/offers_update
---------------------------------------------------------------------------

We would want to create a catalog containing 3 offers. The mandatory information to create an offer is:

 * a :ref:`product reference <product_reference_type>`, such as an EAN
 * a :ref:`seller offer reference <offer_reference_type>`, such as a SKU
 * a :ref:`product state  <product_state>`
 * a price
 * a quantity

You can add other information such as:
 * a description of the product
 * a showcase number, that number is the position of the offer in the showcase
 * an internal comment visible only by yourself in the seller account
 
**Request**

.. code-block:: xml

    <?xml version="1.0" encoding="utf-8"?>
    <offers_update xmlns="http://www.fnac.com/schemas/mp-dialog.xsd" shop_id="307D5158-4596-EAD7-8103-9F7D9804E5D4" partner_id="26FAB269-7094-B0C3-4D82-7CFCAF627519" token="4D859AC6-24EA-23DD-9D97-6A75DE843433">

      <offer>
        <product_reference type="Ean">0711719247159</product_reference>
        <offer_reference type="SellerSku"><![CDATA[B76A-CD5-153]]></offer_reference>
        <price>15</price>
        <product_state>11</product_state>
        <quantity>10</quantity>
        <description_fr><![CDATA[New product - 2-3 days shipping, from France]]></description_fr>
      </offer>

      <offer>
        <product_reference type="Ean">5030917077418</product_reference>
        <offer_reference type="SellerSku"><![CDATA[B067-F0D-75E]]></offer_reference>
        <price>20</price>
        <product_state>11</product_state>
        <quantity>20</quantity>
        <description_fr><![CDATA[Nouveau Produit]]></description_fr>
      </offer>

      <offer>
        <product_reference type="Ean">5051889022091</product_reference>
        <offer_reference type="SellerSku"><![CDATA[561C-385-9BE]]></offer_reference>
        <price>10.55</price>
        <product_state>11</product_state>
        <quantity>16</quantity>
        <description_fr><![CDATA[Nouveau Produit]]></description_fr>
      </offer>

    </offers_update>


**Response**

.. code-block:: xml

    <?xml version="1.0" encoding="utf-8"?>
    <offers_update_response xmlns="http://www.fnac.com/schemas/mp-dialog.xsd" status="OK">
        <batch_id>DC3F089A-8E98-4B98-9434-0DF0EE937DC8</batch_id>
    </offers_update_response>

Once the request is submitted, a "batch_id" is returned. You can use it to know the processing status of the request.
Let's have a look on the workflow followed by the batch.

:ref:`More details about offers_update... <offers_update_service>`

Batch status - https://partners-test.mp.fnacdarty.com/api.php/batch_status
-------------------------------------------------------------------------

The batch can have the following statuses :

ACTIVE > RUNNING > OK | WARNING | ERROR | FATAL

For more details on these statuses please follow this :ref:`link <status_code>`.

**Request**

.. code-block:: xml

  <?xml version="1.0" encoding="utf-8"?>
  <batch_status partner_id="26FAB269-7094-B0C3-4D82-7CFCAF627519" shop_id="307D5158-4596-EAD7-8103-9F7D9804E5D4" token="4D859AC6-24EA-23DD-9D97-6A75DE843433" xmlns="http://www.fnac.com/schemas/mp-dialog.xsd">
    <batch_id>DC3F089A-8E98-4B98-9434-0DF0EE937DC8</batch_id>
  </batch_status>


**Response**

.. code-block:: xml

  <?xml version="1.0" encoding="utf-8"?>
  <batch_status_response status="ACTIVE">
    <batch_id>DC3F089A-8E98-4B98-9434-0DF0EE937DC8</batch_id>
  </batch_status_response>

Here the batch is queued, and ready to be processed.

.. code-block:: xml

  <?xml version="1.0" encoding="utf-8"?>
  <batch_status_response status="RUNNING">
    <batch_id>DC3F089A-8E98-4B98-9434-0DF0EE937DC8</batch_id>
  </batch_status_response>

The batch is being processed.

.. code-block:: xml

  <?xml version="1.0" encoding="utf-8"?>
  <batch_status_response status="OK">

    <batch_id>DC3F089A-8E98-4B98-9434-0DF0EE937DC8</batch_id>

    <offer status="OK">
      <product_fnac_id>2499187</product_fnac_id>
      <offer_fnac_id>E473361D-683B-2A26-E878-F43816B9E111</offer_fnac_id>
      <offer_seller_id>561C-385-9BE</offer_seller_id>
    </offer>

    <offer status="OK">
      <product_fnac_id>9784515</product_fnac_id>
      <offer_fnac_id>4EF5F02D-8A87-9502-95F5-14881CDEEF8A</offer_fnac_id>
      <offer_seller_id>B067-F0D-75E</offer_seller_id>
    </offer>

    <offer status="OK">
      <product_fnac_id>20005761</product_fnac_id>
      <offer_fnac_id>B42974D3-D5E0-DAF1-629C-60541775D944</offer_fnac_id>
      <offer_seller_id>B76A-CD5-153</offer_seller_id>
    </offer>

  </batch_status_response>

The batch has been processed. In the response, the 'product_fnac_id' is returned. It can be used in place of the EAN in offers update requests. This id is the one used by Fnac to identify the product.

When errors are found during the processed, they would all be listed in this response. It could help you to correct your offers update requests by yourself.

:ref:`More details about batch_status... <batch_status_service>`


Offers query - https://partners-test.mp.fnacdarty.com/api.php/offers_query
-------------------------------------------------------------------------

When all seems to be fine with the offers update, you would want to check if the offers actually created.

Here, we are requesting any offers created between 2013/01/01 and 2013/06/30.

**Request**

.. code-block:: xml

  <?xml version="1.0" encoding="utf-8"?>
  <offers_query results_count="50" partner_id="26FAB269-7094-B0C3-4D82-7CFCAF627519" shop_id="307D5158-4596-EAD7-8103-9F7D9804E5D4" token="4D859AC6-24EA-23DD-9D97-6A75DE843433" xmlns="http://www.fnac.com/schemas/mp-dialog.xsd">
    <paging>1</paging>
    <date type="Created">
      <min>2013-01-01T00:00:00+02:00</min>
      <max>2013-06-30T23:59:59+02:00</max>
    </date>
  </offers_query>


The offers query works with a paging system that allows you to browse your catalog easily, especially when it is very large. Using that paging system can lower the responding time as you are giving a limit on the number of items to retrieve. If you're facing slowings on offers retrieving, try to change the paging parameters.

Here is how you can set the paging parameters:

 * attribute 'results_count': set the number of offers to retrieve per page
 * element <paging>: set the number of the page to retrieve

In the service response, you will get the total number of pages and the total number of offers in your catalog.


**Response**

.. code-block:: xml

  <?xml version="1.0" encoding="utf-8"?>
  <offers_query_response status="OK" xmlns="http://www.fnac.com/schemas/mp-dialog.xsd">

    <page>1</page>
    <total_paging>1</total_paging>
    <nb_total_per_page>50</nb_total_per_page>
    <nb_total_result>5</nb_total_result>

    <offer>
      <product_name><![CDATA[MotorStorm Pacific Rift - Gamme Essentials ( Jeux Vidéo  ) - ]]></product_name>
      <product_fnac_id>20005761</product_fnac_id>
      <offer_fnac_id>B42974D3-D5E0-DAF1-629C-60541775D944</offer_fnac_id>
      <offer_seller_id><![CDATA[B76A-CD5-153]]></offer_seller_id>
      <product_state>11</product_state>
      <price>15</price>
      <quantity>10</quantity>
      <description_fr><![CDATA[Nouveau Produit]]></description_fr>
      <internal_comment><![CDATA[]]></internal_comment>
      <product_url><![CDATA[http://www4.fnac.com/articleoffers.aspx?prid=5024283&ref=Fnac.com]]></product_url>
      <image><![CDATA[http://static.fnac-static.com/multimedia/FR/Images_Produits/FR/fnac.com/Grandes/9/5/1/0711719247159.jpg]]></image>
      <nb_messages>0</nb_messages>
      <showcase></showcase>
    </offer>

    <offer>
      <product_name><![CDATA[Call of Duty - Modern Warfare 2 ( Jeux Vidéo  ) - ]]></product_name>
      <product_fnac_id>9784515</product_fnac_id>
      <offer_fnac_id>4EF5F02D-8A87-9502-95F5-14881CDEEF8A</offer_fnac_id>
      <offer_seller_id><![CDATA[B067-F0D-75E]]></offer_seller_id>
      <product_state>11</product_state>
      <price>20</price>
      <quantity>20</quantity>
      <description_fr><![CDATA[Nouveau Produit]]></description_fr>
      <internal_comment><![CDATA[]]></internal_comment>
      <product_url><![CDATA[http://www4.fnac.com/articleoffers.aspx?prid=9784515&ref=MarketPlace]]></product_url>
      <image><![CDATA[]]></image>
      <nb_messages>0</nb_messages>
      <showcase></showcase>
    </offer>

    <offer>
      <product_name><![CDATA[The Dark Knight - Blu-Ray ( DVD / Blu-Ray ) - ]]></product_name>
      <product_fnac_id>2499187</product_fnac_id>
      <offer_fnac_id>E473361D-683B-2A26-E878-F43816B9E111</offer_fnac_id>
      <offer_seller_id><![CDATA[561C-385-9BE]]></offer_seller_id>
      <product_state>11</product_state>
      <price>10.55</price>
      <quantity>16</quantity>
      <description_fr><![CDATA[Nouveau Produit]]></description_fr>
      <internal_comment><![CDATA[]]></internal_comment>
      <product_url><![CDATA[http://www4.fnac.com/articleoffers.aspx?prid=3712323&ref=Fnac.com]]></product_url>
      <image><![CDATA[http://multimedia.fnac.com/multimedia/FR/images_produits/FR/Fnac.com/Grandes/1/9/0/5051889022091.gif]]></image>
      <nb_messages>0</nb_messages>
      <showcase></showcase>
    </offer>

  </offers_query>

The response contains a link to the offer's Fnac page and the associated image that you can display in your own system.

:ref:`More details about offers_query... <offers_query_service>`

Orders query - https://partners-test.mp.fnacdarty.com/api.php/orders_query
-------------------------------------------------------------------------

Your catalog is now created and you may want to test our orders management services.
You first need to create orders. You can do it by using our orders creating tool described on :ref:`this link <test_environment_tools>`.

Now that you have some test orders created, here is how you can retrieve them.

**Request**

.. code-block:: xml

  <?xml version="1.0" encoding="utf-8"?>
  <orders_query results_count="50" partner_id="26FAB269-7094-B0C3-4D82-7CFCAF627519" shop_id="307D5158-4596-EAD7-8103-9F7D9804E5D4" token="4D859AC6-24EA-23DD-9D97-6A75DE843433" xmlns="http://www.fnac.com/schemas/mp-dialog.xsd">
    <paging>1</paging>
      <date type="CreatedAt">
      <min>2013-05-22T00:00:00+02:00</min>
      <max>2013-05-25T00:00:00+02:00</max>
    </date>

    <states>
      <state>Created</state>
    </states>

  </orders_query>

Depending on your own workflow or management, you can set several parameters or conditions to retrieve orders (such as creation date, current state, etc). Here, we want to retrieve new orders which are set on the 'Created' status.


**Response**

.. code-block:: xml

  <?xml version="1.0" encoding="utf-8"?>
  <orders_query_response status="OK" xmlns="http://www.fnac.com/schemas/mp-dialog.xsd">

    <page>1</page>
    <total_paging>1</total_paging>
    <nb_total_per_page>50</nb_total_per_page>
    <nb_total_result>2</nb_total_result>

    <order>
      <shop_id>307D5158-4596-EAD7-8103-9F7D9804E5D4</shop_id>
      <client_id>C2650DB9-6155-44F3-9A80-3FC1967F6A46</client_id>
      <client_firstname><![CDATA[Somaninn]]></client_firstname>
      <client_lastname><![CDATA[Prok]]></client_lastname>
      <order_id>003ECCA1YVFBW</order_id>
      <state>Created</state>
      <created_at>2013-05-23T14:58:39+02:00</created_at>
      <fees>2.5</fees>
      <nb_messages>0</nb_messages>

      <shipping_address>
        <firstname><![CDATA[Somaninn]]></firstname>
        <lastname><![CDATA[Prok]]></lastname>
        <company><![CDATA[]]></company>
        <address1><![CDATA[9 rue des bateaux lavoirs]]></address1>
        <address2><![CDATA[]]></address2>
        <address3><![CDATA[]]></address3>
        <zipcode><![CDATA[93160]]></zipcode>
        <city><![CDATA[Ivry]]></city>
        <country><![CDATA[FRA]]></country>
      </shipping_address>

      <billing_address>
        <firstname><![CDATA[Somaninn]]></firstname>
        <lastname><![CDATA[Prok]]></lastname>
        <company><![CDATA[]]></company>
        <address1><![CDATA[9 rue des bateaux lavoirs]]></address1>
        <address2><![CDATA[]]></address2>
        <address3><![CDATA[]]></address3>
        <zipcode><![CDATA[93160]]></zipcode>
        <city><![CDATA[Ivry]]></city>
        <country><![CDATA[FRA]]></country>
      </billing_address>

      <order_detail>
        <product_name><![CDATA[MotorStorm Pacific Rift - Gamme Essentials ( Jeux Vidéo  ) - ]]></product_name>
        <order_detail_id>1</order_detail_id>
        <state>ToAccept</state>
        <quantity>1</quantity>
        <price>15</price>
        <fees>0</fees>
        <product_fnac_id>20005761</product_fnac_id>
        <offer_fnac_id>B42974D3-D5E0-DAF1-629C-60541775D944</offer_fnac_id>
        <offer_seller_id><![CDATA[B76A-CD5-153]]></offer_seller_id>
        <product_state>11</product_state>
        <description_fr><![CDATA[Nouveau Produit]]></description_fr>
        <internal_comment><![CDATA[]]></internal_comment>
        <shipping_price>2.39</shipping_price>
        <shipping_method>20</shipping_method>
        <created_at>2013-05-23T14:58:39+02:00</created_at>
        <received_at></received_at>
        <product_url><![CDATA[http://www4.fnac.com/articleoffers.aspx?prid=5024283&ref=Fnac.com]]></product_url>
        <image><![CDATA[http://static.fnac-static.com/multimedia/FR/Images_Produits/FR/fnac.com/Grandes/9/5/1/0711719247159.jpg]]></image>
        <tracking_number><![CDATA[]]></tracking_number>
      </order_detail>
    </order>

    <order>
      <shop_id>307D5158-4596-EAD7-8103-9F7D9804E5D4</shop_id>
      <client_id>C2650DB9-6155-44F3-9A80-3FC1967F6A46</client_id>
      <client_firstname><![CDATA[Somaninn]]></client_firstname>
      <client_lastname><![CDATA[Prok]]></client_lastname>
      <order_id>0MVXJL41Q9658</order_id>
      <state>Created</state>
      <created_at>2013-05-23T14:59:15+02:00</created_at>
      <fees>4.84</fees>
      <nb_messages>0</nb_messages>

      <shipping_address>
        <firstname><![CDATA[Somaninn]]></firstname>
        <lastname><![CDATA[Prok]]></lastname>
        <company><![CDATA[]]></company>
        <address1><![CDATA[9 rue des bateaux lavoirs]]></address1>
        <address2><![CDATA[]]></address2>
        <address3><![CDATA[]]></address3>
        <zipcode><![CDATA[93160]]></zipcode>
        <city><![CDATA[Ivry]]></city>
        <country><![CDATA[FRA]]></country>
      </shipping_address>

      <billing_address>
        <firstname><![CDATA[Somaninn]]></firstname>
        <lastname><![CDATA[Prok]]></lastname>
        <company><![CDATA[]]></company>
        <address1><![CDATA[9 rue des bateaux lavoirs]]></address1>
        <address2><![CDATA[]]></address2>
        <address3><![CDATA[]]></address3>
        <zipcode><![CDATA[93160]]></zipcode>
        <city><![CDATA[Ivry]]></city>
        <country><![CDATA[FRA]]></country>
      </billing_address>

      <order_detail>
        <product_name><![CDATA[Call of Duty - Modern Warfare 2 ( Jeux Vidéo  ) - ]]></product_name>
        <order_detail_id>1</order_detail_id>
        <state>ToAccept</state>
        <quantity>1</quantity>
        <price>20</price>
        <fees>0</fees>
        <product_fnac_id>9784515</product_fnac_id>
        <offer_fnac_id>4EF5F02D-8A87-9502-95F5-14881CDEEF8A</offer_fnac_id>
        <offer_seller_id><![CDATA[B067-F0D-75E]]></offer_seller_id>
        <product_state>11</product_state>
        <description_fr><![CDATA[Nouveau Produit]]></description_fr>
        <internal_comment><![CDATA[]]></internal_comment>
        <shipping_price>0.8</shipping_price>
        <shipping_method>20</shipping_method>
        <created_at>2013-05-23T14:59:15+02:00</created_at>
        <received_at></received_at>
        <product_url><![CDATA[http://www4.fnac.com/articleoffers.aspx?prid=9784515&ref=MarketPlace]]></product_url>
        <image><![CDATA[]]></image>
        <tracking_number><![CDATA[]]></tracking_number>
      </order_detail>

      <order_detail>
        <product_name><![CDATA[The Dark Knight - Blu-Ray ( DVD / Blu-Ray ) - ]]></product_name>
        <order_detail_id>2</order_detail_id>
        <state>ToAccept</state>
        <quantity>1</quantity>
        <price>10.55</price>
        <fees>0</fees>
        <product_fnac_id>2499187</product_fnac_id>
        <offer_fnac_id>E473361D-683B-2A26-E878-F43816B9E111</offer_fnac_id>
        <offer_seller_id><![CDATA[561C-385-9BE]]></offer_seller_id>
        <product_state>11</product_state>
        <description_fr><![CDATA[Nouveau Produit]]></description_fr>
        <internal_comment><![CDATA[]]></internal_comment>
        <shipping_price>2.39</shipping_price>
        <shipping_method>20</shipping_method>
        <created_at>2013-05-23T14:59:15+02:00</created_at>
        <received_at></received_at>
        <product_url><![CDATA[http://www4.fnac.com/articleoffers.aspx?prid=3712323&ref=Fnac.com]]></product_url>
        <image><![CDATA[http://multimedia.fnac.com/multimedia/FR/images_produits/FR/Fnac.com/Grandes/1/9/0/5051889022091.gif]]></image>
        <tracking_number><![CDATA[]]></tracking_number>   
      </order_detail>
    </order>

  </orders_query_response>

In this example, we have retrieved 2 orders with one with only one item and the other with 2 items.
Each order line is identified by an order_detail_id, so you can specify what action to apply for each one of them.

:ref:`More details about orders_query... <orders_query_service>`

Let's see how to update these orders.


Orders update - https://partners-test.mp.fnacdarty.com/api.php/orders_update
---------------------------------------------------------------------------

Orders statuses are following this workflow:

Created > Accepted > ToShip > Shipped > Received

The seller acts only at acceptation and shipping steps.

In the following request we are going to accept the first order.

**Requests**

.. code-block:: xml

  <?xml version="1.0" encoding="utf-8"?>
  <orders_update partner_id="26FAB269-7094-B0C3-4D82-7CFCAF627519" shop_id="307D5158-4596-EAD7-8103-9F7D9804E5D4" token="4D859AC6-24EA-23DD-9D97-6A75DE843433" xmlns="http://www.fnac.com/schemas/mp-dialog.xsd">
    <order order_id="003ECCA1YVFBW" action="accept_order">
      <order_detail>
        <order_detail_id>1</order_detail_id>
        <action>Accepted</action>
      </order_detail>
    </order>
  </orders_update>

You have to specify the "accept_order" action and set the status of the order line only (order_detail_id) as 'Accepted'.
To refuse an order detail, you still have to use the "accept_order" action, but you will have to put 'Refused' on the order detail.


For the second order, here is how you can write the request to accept both of the order lines.

.. code-block:: xml

  <?xml version="1.0" encoding="utf-8"?>
  <orders_update partner_id="26FAB269-7094-B0C3-4D82-7CFCAF627519" shop_id="307D5158-4596-EAD7-8103-9F7D9804E5D4" token="4D859AC6-24EA-23DD-9D97-6A75DE843433" xmlns="http://www.fnac.com/schemas/mp-dialog.xsd">

    <order order_id="0MVXJL41Q9658" action="accept_order">
      <order_detail>
      <order_detail_id>1</order_detail_id>
      <action>Accepted</action>
    </order_detail>

    <order_detail>
      <order_detail_id>2</order_detail_id>
      <action>Accepted</action>
      </order_detail>
    </order>

  </orders_update>


When all the order lines have to be updated to the same state as above, you can use *global* actions by specifying the status once for all of them.
The global action to use is "accept_all_orders".

More details about :ref:`orders_update actions... <order_update_action>`

.. code-block:: xml

  <?xml version="1.0" encoding="utf-8"?>
  <orders_update partner_id="26FAB269-7094-B0C3-4D82-7CFCAF627519" shop_id="307D5158-4596-EAD7-8103-9F7D9804E5D4" token="4D859AC6-24EA-23DD-9D97-6A75DE843433" xmlns="http://www.fnac.com/schemas/mp-dialog.xsd">
    <order order_id="0MVXJL41Q9658" action="accept_all_orders">
      <order_detail>
        <action>Accepted</action>
      </order_detail>
    </order>
  </orders_update>


Here are the responses that you should get if all went fine:

**Response**

.. code-block:: xml

  <?xml version="1.0" encoding="utf-8"?>
  <orders_update_response xmlns="http://www.fnac.com/schemas/mp-dialog.xsd" status="OK">
    <order>
      <status>OK</status>
      <order_id>003ECCA1YVFBW</order_id>
      <state>Accepted</state>

      <order_detail>
        <order_detail_id>1</order_detail_id>
        <status>OK</status>
        <state>Accepted</state>
      </order_detail>
    </order>

  </orders_update_response>

  <?xml version="1.0" encoding="utf-8"?>
  <orders_update_response xmlns="http://www.fnac.com/schemas/mp-dialog.xsd" status="OK">
    <order>
      <status>OK</status>
      <order_id>0MVXJL41Q9658</order_id>
      <state>Accepted</state>

      <order_detail>
        <order_detail_id>1</order_detail_id>
        <status>OK</status>
        <state>Accepted</state>
      </order_detail>

      <order_detail>
        <order_detail_id>2</order_detail_id>
        <status>OK</status>
        <state>Accepted</state>
      </order_detail>
    </order>

  </orders_update_response>

From this step, your orders are accepted and will be validated and updated by Fnac to the 'ToShip' status. Please remember that you must wait for that status update to send your products, as it means that the order is actually paid.

To be sure that your orders are correctly updated, make a new orders_query request on the wanted status.

**Request (orders_query)**

.. code-block:: xml

  <?xml version="1.0" encoding="utf-8"?>
  <orders_query results_count="50" partner_id="26FAB269-7094-B0C3-4D82-7CFCAF627519" shop_id="307D5158-4596-EAD7-8103-9F7D9804E5D4" token="4D859AC6-24EA-23DD-9D97-6A75DE843433" xmlns="http://www.fnac.com/schemas/mp-dialog.xsd">
    <paging>1</paging>
      <date type="CreatedAt">
      <min>2013-05-22T00:00:00+02:00</min>
      <max>2013-05-25T00:00:00+02:00</max>
    </date>

    <states>
      <state>ToShip</state>
    </states>

  </orders_query>

**Response**

.. code-block:: xml

  <?xml version="1.0" encoding="utf-8"?>
  <orders_query_response status="OK" xmlns="http://www.fnac.com/schemas/mp-dialog.xsd">

    <page>1</page>
    <total_paging>1</total_paging>
    <nb_total_per_page>50</nb_total_per_page>
    <nb_total_result>2</nb_total_result>

    <order>
      <shop_id>307D5158-4596-EAD7-8103-9F7D9804E5D4</shop_id>
      <client_id>C2650DB9-6155-44F3-9A80-3FC1967F6A46</client_id>
      <client_firstname><![CDATA[Somaninn]]></client_firstname>
      <client_lastname><![CDATA[Prok]]></client_lastname>
      <order_id>003ECCA1YVFBW</order_id>
      <state>ToShip</state>
      <created_at>2013-05-23T14:58:39+02:00</created_at>
      <fees>2.5</fees>
      <nb_messages>0</nb_messages>
      <delivery_note>https://partners-test.mp.fnacdarty.com/api.php/delivery-note/26FAB269-7094-B0C3-4D82-7CFCAF627519/307D5158-4596-EAD7-8103-9F7D9804E5D4/8DBB1124-A2CF-92C7-04C8-8115D444ECE7/003ECCA1YVFBW</delivery_note>

      <shipping_address>
        <firstname><![CDATA[Somaninn]]></firstname>
        <lastname><![CDATA[Prok]]></lastname>
        <company><![CDATA[]]></company>
        <address1><![CDATA[9 rue des bateaux lavoirs]]></address1>
        <address2><![CDATA[]]></address2>
        <address3><![CDATA[]]></address3>
        <zipcode><![CDATA[93160]]></zipcode>
        <city><![CDATA[Ivry]]></city>
        <country><![CDATA[FRA]]></country>
      </shipping_address>

      <billing_address>
        <firstname><![CDATA[Somaninn]]></firstname>
        <lastname><![CDATA[Prok]]></lastname>
        <company><![CDATA[]]></company>
        <address1><![CDATA[9 rue des bateaux lavoirs]]></address1>
        <address2><![CDATA[]]></address2>
        <address3><![CDATA[]]></address3>
        <zipcode><![CDATA[93160]]></zipcode>
        <city><![CDATA[Ivry]]></city>
        <country><![CDATA[FRA]]></country>
      </billing_address>

      <order_detail>
        <product_name><![CDATA[MotorStorm Pacific Rift - Gamme Essentials ( Jeux Vidéo  ) - ]]></product_name>
        <order_detail_id>1</order_detail_id>
        <state>ToShip</state>
        <quantity>1</quantity>
        <price>15</price>
        <fees>2.5</fees>
        <product_fnac_id>20005761</product_fnac_id>
        <offer_fnac_id>B42974D3-D5E0-DAF1-629C-60541775D944</offer_fnac_id>
        <offer_seller_id><![CDATA[B76A-CD5-153]]></offer_seller_id>
        <product_state>11</product_state>
        <description_fr><![CDATA[Nouveau Produit]]></description_fr>
        <internal_comment><![CDATA[]]></internal_comment>
        <shipping_price>2.39</shipping_price>
        <shipping_method>20</shipping_method>
        <created_at>2013-05-23T14:58:39+02:00</created_at>
        <debited_at>2013-05-23T18:41:43+02:00</debited_at>
        <received_at></received_at>
        <product_url><![CDATA[http://www4.fnac.com/articleoffers.aspx?prid=5024283&ref=Fnac.com]]></product_url>
        <image><![CDATA[http://static.fnac-static.com/multimedia/FR/Images_Produits/FR/fnac.com/Grandes/9/5/1/0711719247159.jpg]]></image>
        <tracking_number><![CDATA[]]></tracking_number>
      </order_detail>
    </order>

    <order>
      <shop_id>307D5158-4596-EAD7-8103-9F7D9804E5D4</shop_id>
      <client_id>C2650DB9-6155-44F3-9A80-3FC1967F6A46</client_id>
      <client_firstname><![CDATA[Somaninn]]></client_firstname>
      <client_lastname><![CDATA[Prok]]></client_lastname>
      <order_id>0MVXJL41Q9658</order_id>
      <state>ToShip</state>
      <created_at>2013-05-23T14:59:15+02:00</created_at>
      <fees>4.84</fees>
      <nb_messages>0</nb_messages>
      <delivery_note>https://partners-test.mp.fnacdarty.com/api.php/delivery-note/26FAB269-7094-B0C3-4D82-7CFCAF627519/307D5158-4596-EAD7-8103-9F7D9804E5D4/8DBB1124-A2CF-92C7-04C8-8115D444ECE7/0MVXJL41Q9658</delivery_note>

      <shipping_address>
        <firstname><![CDATA[Somaninn]]></firstname>
        <lastname><![CDATA[Prok]]></lastname>
        <company><![CDATA[]]></company>
        <address1><![CDATA[9 rue des bateaux lavoirs]]></address1>
        <address2><![CDATA[]]></address2>
        <address3><![CDATA[]]></address3>
        <zipcode><![CDATA[93160]]></zipcode>
        <city><![CDATA[Ivry]]></city>
        <country><![CDATA[FRA]]></country>
      </shipping_address>

      <billing_address>
        <firstname><![CDATA[Somaninn]]></firstname>
        <lastname><![CDATA[Prok]]></lastname>
        <company><![CDATA[]]></company>
        <address1><![CDATA[9 rue des bateaux lavoirs]]></address1>
        <address2><![CDATA[]]></address2>
        <address3><![CDATA[]]></address3>
        <zipcode><![CDATA[93160]]></zipcode>
        <city><![CDATA[Ivry]]></city>
        <country><![CDATA[FRA]]></country>
      </billing_address>

      <order_detail>
        <product_name><![CDATA[Call of Duty - Modern Warfare 2 ( Jeux Vidéo  ) - ]]></product_name>
        <order_detail_id>1</order_detail_id>
        <state>ToShip</state>
        <quantity>1</quantity>
        <price>20</price>
        <fees>2.99</fees>
        <product_fnac_id>9784515</product_fnac_id>
        <offer_fnac_id>4EF5F02D-8A87-9502-95F5-14881CDEEF8A</offer_fnac_id>
        <offer_seller_id><![CDATA[B067-F0D-75E]]></offer_seller_id>
        <product_state>11</product_state>
        <description_fr><![CDATA[Nouveau Produit]]></description_fr>
        <internal_comment><![CDATA[]]></internal_comment>
        <shipping_price>0.8</shipping_price>
        <shipping_method>20</shipping_method>
        <created_at>2013-05-23T14:59:15+02:00</created_at>
        <debited_at>2013-05-23T18:41:16+02:00</debited_at>
        <received_at></received_at>
        <product_url><![CDATA[http://www4.fnac.com/articleoffers.aspx?prid=9784515&ref=MarketPlace]]></product_url>
        <image><![CDATA[]]></image>
        <tracking_number><![CDATA[]]></tracking_number>
      </order_detail>

      <order_detail>
        <product_name><![CDATA[The Dark Knight - Blu-Ray ( DVD / Blu-Ray ) - ]]></product_name>
        <order_detail_id>2</order_detail_id>
        <state>ToShip</state>
        <quantity>1</quantity>
        <price>10.55</price>
        <fees>1.85</fees>
        <product_fnac_id>2499187</product_fnac_id>
        <offer_fnac_id>E473361D-683B-2A26-E878-F43816B9E111</offer_fnac_id>
        <offer_seller_id><![CDATA[561C-385-9BE]]></offer_seller_id>
        <product_state>11</product_state>
        <description_fr><![CDATA[Nouveau Produit]]></description_fr>
        <internal_comment><![CDATA[]]></internal_comment>
        <shipping_price>2.39</shipping_price>
        <shipping_method>20</shipping_method>
        <created_at>2013-05-23T14:59:15+02:00</created_at>
        <debited_at>2013-05-23T18:41:16+02:00</debited_at>
        <received_at></received_at>
        <product_url><![CDATA[http://www4.fnac.com/articleoffers.aspx?prid=3712323&ref=Fnac.com]]></product_url>
        <image><![CDATA[http://multimedia.fnac.com/multimedia/FR/images_produits/FR/Fnac.com/Grandes/1/9/0/5051889022091.gif]]></image>
        <tracking_number><![CDATA[]]></tracking_number>
      </order_detail>
    </order>
  </orders_query_response>


As the orders are now marked as 'ToShip', we are now able to ship them. The requests will look quite the same as the acceptation ones, but this time, we are using the sending confirmation action.
Remember that you can update order details individually or globally. Moreover, you can update all your orders (up to 25 orders) with a single request as below.

**Request**

.. code-block:: xml

  <?xml version="1.0" encoding="utf-8"?>
  <orders_update partner_id="26FAB269-7094-B0C3-4D82-7CFCAF627519" shop_id="307D5158-4596-EAD7-8103-9F7D9804E5D4" token="4D859AC6-24EA-23DD-9D97-6A75DE843433" xmlns="http://www.fnac.com/schemas/mp-dialog.xsd">
    <order order_id="003ECCA1YVFBW" action="confirm_to_send">
      <order_detail>
        <order_detail_id>1</order_detail_id>
        <action>Shipped</action>
      </order_detail>
    </order>

    <order order_id="0MVXJL41Q9658" action="confirm_all_to_send">
      <order_detail>
        <order_detail_id>1</order_detail_id>
        <action>Shipped</action>
      </order_detail>
    </order>
  </orders_update>

**Response**

.. code-block:: xml

  <?xml version="1.0" encoding="utf-8"?>
  <orders_update_response xmlns="http://www.fnac.com/schemas/mp-dialog.xsd" status="OK">
    <order>
      <status>OK</status>
      <order_id>003ECCA1YVFBW</order_id>
      <state>Shipped</state>
      <order_detail>
        <order_detail_id>1</order_detail_id>
        <status>OK</status>
        <state>Shipped</state>
      </order_detail>
    </order>

    <order>
      <status>OK</status>
      <order_id>0MVXJL41Q9658</order_id>
      <state>Shipped</state>
      <order_detail>
        <order_detail_id>1</order_detail_id>
        <status>OK</status>
        <state>Shipped</state>
      </order_detail>
      <order_detail>
        <order_detail_id>2</order_detail_id>
        <status>OK</status>
        <state>Shipped</state>
      </order_detail>
    </order>
  </orders_update_response>


All the orders are now shipped. The customers will then have to update the orders form their customer account at reception. The status will then change to "Received".

You now know how to write XML requests to manage your catalog and orders. The other services works on quite the same logic. We invite you to browse the documentation to get more details that will help you to make our API fit to your system.