Making your first request
=========================

Using our API is consisting in 3 steps:
 * Authenticating to our platform
 * Sending an XML request to the right service
 * Handling the given response

Here is below a quick PHP implementation example to make an offers_update request to create an offer.
 
.. code-block:: php

  <?php
    // Fnac Marketplace offers_update example

    // Set the API authentication parameters
    $partner_id = '509491E8-7341-AE98-F708-11768C65539C';
    $shop_id    = '0EA474AC-BE18-5F77-D7F0-E509DE2AABC7';
    $key        = '0D00AF3D-D33F-B5F1-3A5F-BF153C8DAE06';

    $url        = 'https://partners-test.mp.fnacdarty.com/api.php/';

    // STEP 1: Authenticate to the API
    // Generate the authentication request
    $auth_request_xml = <<<XML
  <?xml version='1.0' encoding='utf-8'?>
  <auth xmlns='http://www.fnac.com/schemas/mp-dialog.xsd'>
    <partner_id>00000000-0000-0000-0000-000000000000</partner_id>
    <shop_id>00000000-0000-0000-0000-000000000000</shop_id>
    <key>00000000-0000-0000-0000-000000000000</key>
  </auth>
  XML;


    $xmlAuthentication              = simplexml_load_string($auth_request_xml);
    // Load authentication parameters within request
    $xmlAuthentication->partner_id  = $partner_id;
    $xmlAuthentication->shop_id     = $shop_id;
    $xmlAuthentication->key         = $key;

    // Send xml request to webservice auth
    $response    = do_post_request($url . "auth", $xmlAuthentication->asXML());
    $xmlResponse = simplexml_load_string(trim($response));

    // Display auth response
    var_dump($xmlResponse);
    echo "\n<hr/>";

    // Get token for session authentication
    $token = $xmlResponse->token;

    // STEP 2: Send an offers_update request
    // Generate the offers_update request
    $offers_update_request_xml = <<<XML
  <?xml version='1.0' encoding="utf-8"?>
  <offers_update partner_id="00000000-0000-0000-0000-000000000000" shop_id="00000000-0000-0000-0000-000000000000" token="00000000-0000-0000-0000-000000000000" xmlns='http://www.fnac.com/schemas/mp-dialog.xsd'>
    <offer>
      <product_reference type='Ean'>0886971942323</product_reference>
      <offer_reference type='SellerSku'>SKU_TEST_1</offer_reference>
      <price>50</price>
      <product_state>11</product_state>
      <quantity>999</quantity>
      <description_fr>Ceci est une offre de test.</description_fr>
    </offer>
  </offers_update>
  XML;


    $xmlOffersUpdate               = simplexml_load_string($offers_update_request_xml);
    // Load authentication parameters and token within request
    $xmlOffersUpdate['partner_id'] = $partner_id;
    $xmlOffersUpdate['shop_id']    = $shop_id;
    $xmlOffersUpdate['token']      = $token;

    libxml_use_internal_errors(true);
    $dom = new DOMDocument;
    $dom->loadXML($xmlOffersUpdate->asXML());

    // Check XSD compliance of the generated request. This step is very useful to spot possible formatting errors.
    $valid = $dom->schemaValidate('../xsd/OffersUpdateService.xsd');
    $error = libxml_get_errors();
    if(!$valid)
    {
      // Display found errors
      var_dump($error);
      die();
    }

    // Send xml to webservice OffersUpdate
    $response    = do_post_request($url . "offers_update", $xmlOffersUpdate->asXML());
    $xmlResponse = simplexml_load_string(trim($response));

    // STEP 3: Handle the response
    // Offers_update gives the batch id which can be used with batch_status service to get status of your import. Here, we are simply displaying it.
    var_dump($xmlResponse);



   /***
    * do_post_request
    * ===============
    * @param string $url contains the service url to call for the request.
    * @param string $data contains the request to send. In this purpose, XML data are sent.
    *
    * This function sends request by POST. Any method to send the request can be used, here we are using a cURL session.
    */

    function do_post_request($url, $data)
    {
      $ch = curl_init();

      // Depending on your system, you may add other options or modify the following ones.
      curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1); 
      curl_setopt($ch, CURLOPT_POST, 1); 
      curl_setopt($ch, CURLOPT_URL, $url);
      curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
      curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
      curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
      curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);

      $response = curl_exec($ch);
      curl_close($ch);

      return $response;
    }

  ?>

This example can be applied to any other of the existing services.
For understanding purpose, the XML requests are loaded as strings in the example above. To make the code cleaner, you should use the simplexml_load_file() function and load xml external files.