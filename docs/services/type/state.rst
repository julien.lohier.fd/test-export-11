State Type
==========

.. _commercial_product_reference: 

commercial_product_reference
----------------------------

Description
^^^^^^^^^^^

Represents a product reference (id and type)

Restriction
^^^^^^^^^^^

* Extends: :ref:`RestrictedType`
* Pattern: ([0-9X]|[A-Z])*


.. _RestrictedType:

RestrictedType
--------------

Description
^^^^^^^^^^^

Restriction
^^^^^^^^^^^

* Extends: xs:string
* Pattern: ([0-9X]|[A-Z])*

Attributes
^^^^^^^^^^

+------------------------+------------------------------------------------+-------------------------------+----------+-----------+
|          Name          |                    Description                 |        Type                   | Required | Default   |
+========================+================================================+===============================+==========+===========+
| type                   |                                                | :ref:`product_reference_type` | required | Ean       |
+------------------------+------------------------------------------------+-------------------------------+----------+-----------+


.. _commercial_product_reference_type: 

commercial_product_reference_type
---------------------------------

Description
^^^^^^^^^^^

Represents the types of product identifiers accepted by the platform for the pricing

Restriction
^^^^^^^^^^^

* Extends: xs:string
* Enumerate:

  * FnacId: The product identifier given is the Fnac product identifier
  * Ean: The product identifier given is the global Ean of this product
  * Isbn: The product identifier given is the global Isbn of this product
  * CnetId: The product identifier given is the global Cnet reference of this product

.. _product_reference_type: 

product_reference_type
----------------------

Description
^^^^^^^^^^^

Represents the types of product identifiers accepted by the platform

Restriction
^^^^^^^^^^^

* Extends: xs:string
* Enumerate:

  * FnacId: The product identifier given is the Fnac product identifier
  * PartnerId: The product identifier given is the id from a partner, if this value is set, a partner id will be mandatory
  * Ean: The product identifier given is the global Ean of this product
  * Isbn: The product identifier given is the global Isbn of this product
  * PartNumber: The product identifier given is the global partNumber of this product
  * CnetId: The product identifier given is the global Cnet reference of this product
  
.. _product_reference: 

product_reference
-----------------

Description
^^^^^^^^^^^

Represents a product reference (id and type)

Restriction
^^^^^^^^^^^

* Extends: xs:string

Attributes
^^^^^^^^^^

+------------------------+------------------------------------------------+-------------------------------+----------+-----------+
|          Name          |                    Description                 |        Type                   | Required | Default   |
+========================+================================================+===============================+==========+===========+
| type                   |                                                | :ref:`product_reference_type` | optional | Ean       |
+------------------------+------------------------------------------------+-------------------------------+----------+-----------+

.. _offer_reference_type: 

offer_reference_type
--------------------

Description
^^^^^^^^^^^

Defines which source is used for identifying an offer

Restriction
^^^^^^^^^^^

* Extends: xs:string
* Enumerate:

  * FnacId: The offer id provided is the Fnac Id generated when the offer was recorded
  * SellerSku: The offer id provided is the seller's sku (linked to the shop_id)
  
  
.. _offer_reference: 

offer_reference
---------------

Description
^^^^^^^^^^^

Represents an offer reference (id and type)

Restriction
^^^^^^^^^^^

* Extends: xs:string

Attributes
^^^^^^^^^^

+------------------------+------------------------------------------------+-------------------------------+----------+-----------+
|          Name          |                    Description                 |        Type                   | Required | Default   |
+========================+================================================+===============================+==========+===========+
| type                   |                                                | :ref:`offer_reference_type`   | optional | SellerSku |
+------------------------+------------------------------------------------+-------------------------------+----------+-----------+

.. _picture: 

picture
-------

Description
^^^^^^^^^^^

Target picture url.


Restriction
^^^^^^^^^^^

* Extends: xs:string


.. _offer_pictures: 

offer_pictures
--------------

Description
^^^^^^^^^^^

Contains the pictures list. At least one picture is required.


Contents
^^^^^^^^

+------------------------+------------------------------------------------+-------------------------------+----------+
|          Name          |                    Description                 |        Type                   | Required |
+========================+================================================+===============================+==========+
| picture_1              |                                                | :ref:`picture`                | optional |
+------------------------+------------------------------------------------+-------------------------------+----------+
| picture_2              |                                                | :ref:`picture`                | optional |
+------------------------+------------------------------------------------+-------------------------------+----------+
| picture_3              |                                                | :ref:`picture`                | optional |
+------------------------+------------------------------------------------+-------------------------------+----------+
| picture_4              |                                                | :ref:`picture`                | optional |
+------------------------+------------------------------------------------+-------------------------------+----------+




.. _order_update_action:

order_update_action
-------------------

Description
^^^^^^^^^^^

Defines the actions available on an order from the seller's side

Restriction
^^^^^^^^^^^

* Extends: xs:string
* Enumerate:

  * accept_order: The action for the order is accepting orders by the seller
  * confirm_to_send: The action for the order is confirming sending orders by the seller
  * update: The action for the order is updating orders by the seller
  * accept_all_orders: The action for the order is accepting or refusing all order_details of the order by the seller
  * confirm_all_to_send: The action for the order is confirming sending all order_details by the seller
  * update_all: The action for the order is to update tracking information for all order_details

.. _incident_order_update_action:

incident_order_update_action
----------------------------

Description
^^^^^^^^^^^

Defines the actions available on an refund order from the seller's side

Restriction
^^^^^^^^^^^

* Extends: xs:string
* Enumerate:

  * refund: The action for the order is refunding order_details by the seller

.. _order_detail_state:

order_detail_state
------------------

Description
^^^^^^^^^^^

Defines the different states of an order line

Restriction
^^^^^^^^^^^

* Extends: xs:string
* Enumerate: 

  * ToAccept: The order line is waiting for approval from the seller
  * Accepted: The order line has been approved by the seller and is pending for billing and payment
  * Refused: The order line has been completely refused by the seller (terminal state)
  * ToShip: The order line has been approved by the seller and billed, and is waiting for shipment by the seller
  * Shipped: The order line has been approved by the seller, billed and shipped, and is waiting for aknowledgement by the customer
  * NotReceived: The customer claimed that some or all products have not been received. This case will be managed by the customer service
  * Received: The order line is finished, the customer received all the products and acknowledged reception (terminal state)
  * Refunded: The order line has been refunded by the seller (terminal state)
  * Cancelled: The order line has been cancelled by the customer (terminal state)
  
.. _offer_quantity_constraint:

offer_quantity_constraint
-------------------------

Description
^^^^^^^^^^^

Defines the query constraints that can be set on an offer query

Restriction
^^^^^^^^^^^

* Extends: xs:string
* Enumerate

  * Equals: The quantity left for this offer must be equal to the quantity specified in the query
  * LessThan: The quantity left for this offer must be less than the quantity specified in the query
  * GreaterThan: The quantity left for this offer must be greater than the quantity specified in the query
  * LessThanOrEquals: The quantity left for this offer must be less than or equal to the quantity specified in the query
  * GreaterThanOrEquals: The quantity left for this offer must be greater than or equal to the quantity specified in the query
  
.. _error:

error
-----

Description
^^^^^^^^^^^

Defines an error while processing an element

Restriction
^^^^^^^^^^^

* Extends: xs:string

Attributes
^^^^^^^^^^

+------------------------+------------------------------------------------+-------------------------------+----------+-----------+
|          Name          |                    Description                 |        Type                   | Required | Default   |
+========================+================================================+===============================+==========+===========+
| severity               | Defines the severity of this error             | :ref:`severity_code`          |          |           |
+------------------------+------------------------------------------------+-------------------------------+----------+-----------+
| code                   | Defines an optional error code for this error  | xs:string                     |          |           |
+------------------------+------------------------------------------------+-------------------------------+----------+-----------+

.. _address:

address
-------

Description
^^^^^^^^^^^

Defines an adress (billing or shipping). Fields being self explanatory, they won't be further described. 
Elements phone and mobile are displayed only on registered shippings (shipping method '22').

Elements
^^^^^^^^

+------------------------+--------------------------------------------------------------+--------------------+-------------------+
|          Name          |                    Description                               |        Type        |    Occurrence     | 
+========================+==============================================================+====================+===================+
| firstname              |                                                              | :ref:`string100`   | 1-1               |
+------------------------+--------------------------------------------------------------+--------------------+-------------------+
| lastname               |                                                              | :ref:`string100`   | 1-1               |
+------------------------+--------------------------------------------------------------+--------------------+-------------------+
| company                |                                                              | :ref:`string100`   | 0-1               |
+------------------------+--------------------------------------------------------------+--------------------+-------------------+
| address1               |                                                              | :ref:`string255`   | 0-1               |
+------------------------+--------------------------------------------------------------+--------------------+-------------------+
| address2               |                                                              | :ref:`string255`   | 0-1               |
+------------------------+--------------------------------------------------------------+--------------------+-------------------+
| address3               |                                                              | :ref:`string255`   | 0-1               |
+------------------------+--------------------------------------------------------------+--------------------+-------------------+
| zipcode                |                                                              | :ref:`string15`    | 1-1               |
+------------------------+--------------------------------------------------------------+--------------------+-------------------+
| city                   |                                                              | :ref:`string100`   | 1-1               |
+------------------------+--------------------------------------------------------------+--------------------+-------------------+
| state                  |                                                              | :ref:`string100`   | 0-1               |
+------------------------+--------------------------------------------------------------+--------------------+-------------------+
| country                |                                                              | :ref:`string100`   | 1-1               |
+------------------------+--------------------------------------------------------------+--------------------+-------------------+
| phone                  |                                                              | :ref:`string15`    | 0-1               |
+------------------------+--------------------------------------------------------------+--------------------+-------------------+
| mobile                 |                                                              | :ref:`string15`    | 0-1               |
+------------------------+--------------------------------------------------------------+--------------------+-------------------+
| nif                    | Client NIF, appears for Spanish and Portuguese accounts only | :ref:`string15`    | 0-1               |
+------------------------+--------------------------------------------------------------+--------------------+-------------------+

.. _shipping_method:

shipping_method
---------------

Description
^^^^^^^^^^^

Represents the shipping methods accepted by the platform

Restriction
^^^^^^^^^^^

* Extends: xs:int
* Enumerate:

  * 20: Standard Shipping method
  * 21: Standard Shipping method with tracking number
  * 22: Certified Shipping method
  * 28: Pickup at the seller's
  * 50: Fast delivery Shipping method
  * 51: Custom Shipping large products method #1
  * 52: Custom Shipping large products method #2
  * 53: Custom Shipping large products method #3
  * 54: Custom Shipping large products method #4
  * 55: Delivery point (partner Relais Colis) Shipping method
  * 63: Delivery point (partner EnvoiMoinsCher) Shipping method

.. _refund_reason:

refund_reason
-------------

Description
^^^^^^^^^^^

Represents the different reasons for refunding a product

Restriction
^^^^^^^^^^^

* Extends: xs:string
* Enumerate:


  * no_stock: The product is no longer in stock with the seller 
  * cancelled_client: Order cancelled by customer
  * returned_article: Article was returned by the client
  * other: Other reason that is not listed above

.. _message_update_type:

message_update_type
-------------------

Description
^^^^^^^^^^^

Represents the different kinds of message

Restriction
^^^^^^^^^^^

* Extends: xs:string
* Enumerate:

  * order: Add a new message to an order
  * offer: Answer a message to an offer

.. _product_state:

product_state
-------------

Description
^^^^^^^^^^^

Defines the state of a product

Restriction
^^^^^^^^^^^

* Extends: xs:string
* Enumerate:

 * 1: This product is a used product like as new
 * 2: This product is a used product in very good state
 * 3: This product is a used in good state
 * 4: This product is a used product in correct state
 * 5: This product is a collection product like as new
 * 6: This product is a collection product in very good state
 * 7: This product is a collection product in good state
 * 8: This product is a collection product in correct state
 * 10: This product is a refurbished product
 * 11: This product is a new product

.. _offer_state_constraint:

offer_state_constraint
----------------------

Description
^^^^^^^^^^^

Allows specifying constraints when querying an offer

Restriction
^^^^^^^^^^^

* Extends: xs:string
* Enumerate:

  * Created: The query will be made against the offer creation date
  * Modified: The query will be made against the offer modification date

.. _order_state_constraint:

order_state_constraint
----------------------

Description
^^^^^^^^^^^

Allows specifying constraints when querying an order

Restriction
^^^^^^^^^^^

* Extends: xs:string
* Enumerate:

  * CreatedAt: The query will be made against the order creation date
  * UpdatedAt: The query will be made against the order modification date
  * AcceptedAt: The query will be made against the order "Accepted" date
  * ReceivedAt: The query will be made on orders in the "Received" state
  * DebitedAt: The query will be made on orders in the "Debited" state
  * ShippedAt: The query will be made on orders in the "Shipped" state
  * RefusedAt: The query will be made on orders in the "Refused" state
  * CancelledAt: The query will be made on orders in the "Cancelled" state

.. _order_state:

order_state
-----------

Description
^^^^^^^^^^^

Defines the different states of an order. The order is in the earliest stage found in order lines

Restriction
^^^^^^^^^^^

* Extends: xs:string
* Enumerate:

  * Created: The order is waiting for approval from the seller
  * Accepted: The order has been approved (even partially) by the seller and is waiting for billing and payment
  * Refused: The order has been completely refused by the seller (terminal state)
  * ToShip: The order has been approved by the seller and billed, and is waiting for shipment by the seller
  * Shipped: The order has been approved by the seller, billed and shipped, and is waiting for aknowledgement by the customer
  * NotReceived: The customer claimed that some or all products have not been received. This case will be managed by the customer service
  * Received: The order is finished, the customer received all the products and acknowledged reception (terminal state)
  * Cancelled: The order has been cancelled by the customer (terminal state) or by the fnac service
  * Refunded: The order has been refunded by the seller (terminal state) or by the fnac service
  * Error: The order status is in a non authorized or illogic state

.. _order_detail_state_update:

order_detail_state_update
-------------------------

Description
^^^^^^^^^^^

Defines the actions available on an order line from the seller's side

Restriction
^^^^^^^^^^^

* Extends: xs:string
* Enumerate:

  * Accepted: The line has been accepted by the seller
  * Refused: The line has been refused by the seller
  * Shipped: The line has been shipped by the seller, an optional tracking number can be specified
  * Refunded: The line has been refunded by the seller or by fnac
  * Updated: The line has been updated by the seller

.. _message_date_constraint:

message_date_constraint
-----------------------

Description
^^^^^^^^^^^

Allows specifying constraints when querying messages

Restriction
^^^^^^^^^^^

* Extends: xs:string
* Enumerate:

  * CreatedAt: The query will be made against the message creation date
  * UpdatedAt: The query will be made against the message modification date

.. _incident_date_constraint:

incident_date_constraint
------------------------

Description
^^^^^^^^^^^

Allows specifying constraints when querying an incident

Restriction
^^^^^^^^^^^

* Extends: xs:string
* Enumerate:

  * CreatedAt: The query will be made against the incident creation date
  * UpdatedAt: The query will be made against the incident modification date
  * ClosedAt: The query will be made against the incident "Closed" date

.. _incident_close_state:

incident_close_state
--------------------

Description
^^^^^^^^^^^

Defines incidents closing states

Restriction
^^^^^^^^^^^

* Extends: xs:integer
* Enumerate

  * 0: No incident
  * 1: Item referred to the seller
  * 2: Refund by check
  * 3: Seller aggreement
  * 4: Received item
  * 5: Good item
  * 6: Item not received
  * 7: Item Faulty
  * 8: Item unlike description
  * 9: Client doesn't answer
  * 10: Refund
  * 11: Others
  * 12: Issue solved by Fnac
  * 13: No Answer
  * 14: No news
  * 15: No stock
  * 16: Item returned
  * 17: Item cancelled
  * 18: Impossible send
  * 19: Revocation
  * 20: Counterfeit
  * 21: Longer period
  * 22: Seller's customer service

.. _incident_open_state:

incident_open_state
-------------------

Description
^^^^^^^^^^^

Defines incidents open states

Restriction
^^^^^^^^^^^

* Extends: xs:integer
* Enumerate:

  * 0: No incident
  * 1: Article not received
  * 2: Inconsistent with item
  * 3: Defective item
  * 4: Rectraction
  * 5: Infriging item
  * 6: Client others
  * 7: Refund
  * 8: No stock
  * 9: Returned by client
  * 10: Cancel by client
  * 11: Impossible shipping
  * 12: Item not received
  * 13: Item faulty
  * 14: Item unlike description
  * 15: Seller others
  * 16: Longer period
  * 17: Refund Seller
  * 18: Refund CallCenter

.. _pricing_sellers:

pricing_sellers
---------------

Description
^^^^^^^^^^^

Defines which sellers to use

Restriction
^^^^^^^^^^^

* Extends: xs:string
* Enumerate:

  * all: All sellers including me
  * others: All sellers NOT including me


.. _incident_seller_waiting_answer:

incident_seller_waiting_answer
------------------------------

Description
^^^^^^^^^^^

Defines if incident needs an answer from the seller

Restriction
^^^^^^^^^^^

* Extends: xs:string
* Enumerate

  * TRUE
  * FALSE


.. _incident_state:

incident_state
--------------

Description
^^^^^^^^^^^

Defines the different incident's states.

Restriction
^^^^^^^^^^^

* Extends: xs:string
* Enumerate

  * OPENED
  * CLOSED

.. _incident_by:

incident_by
-----------

Description
^^^^^^^^^^^

Defines the different user who could open or close an incident.

Restriction
^^^^^^^^^^^

* Extends: xs:string
* Enumerate

  * CLIENT
  * CALLCENTER
  * SELLER


.. _sort_constraint_type:

sort_constraint_type
--------------------

Description
^^^^^^^^^^^

Defines the different sort constraints .

Restriction
^^^^^^^^^^^

* Extends: xs:string
* Enumerate
  
  * DESC
  * ASC

.. _query_string:

query_string
------------

Description
^^^^^^^^^^^

Presents the value of carriers query.

Restriction
^^^^^^^^^^^

* Extends: xs:string
* Enumerate

  * all  : all the carrier your shop is allowed to see

.. _sales_period_date_constraint:

sales_period_date_constraint
----------------------------

Description
^^^^^^^^^^^

Allows to specify date constraints when querying sales period

Restriction
^^^^^^^^^^^

* Extends: xs:string
* Enumerate:

  * StartAt: The query will be made against the sales periods which start between provided min an max dates
  * EndAt: The query will be made against the sales periods which end between provided min an max dates
