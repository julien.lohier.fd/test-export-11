.. contents::
   :depth: 2
   :backlinks: top

Common Type
===========

.. _string255:

string255
---------

Restriction
^^^^^^^^^^^

* Extends: xs:string
* Max length: 255

.. _string100:

string100
---------

Restriction
^^^^^^^^^^^

* Extends: xs:string
* Max length: 100

.. _string32:

string32
--------

Restriction
^^^^^^^^^^^

* Extends: xs:string
* Max length: 32

.. _string15:

string15
--------

Restriction
^^^^^^^^^^^

* Extends: xs:string
* Max length: 15


.. _string5:

string5
-------

Restriction
^^^^^^^^^^^

* Extends: xs:string
* Max length: 5
* Min length: 5

.. _uuid:

uuid
----

Description
^^^^^^^^^^^

Fnac.com unique user identifier

Restriction
^^^^^^^^^^^

* Extends: xs:string
* Match pattern: [0-9a-fA-F]{8}-[0-9a-fA-F]{4}-[0-9a-fA-F]{4}-[0-9a-fA-F]{4}-[0-9a-fA-F]{12}

.. _status_code:

status_code
-----------

Description
^^^^^^^^^^^

Presents the status of the last operation. More details are available in the sub levels presenting errors

Restriction
^^^^^^^^^^^

* Extends: xs:string
* Enumeration: 
  
  * RUNNING: Process is still running
  * OK: Process finished without any error or warning
  * WARNING: Process finished with at least a warning and no error
  * ERROR: Process finished with at least an error, but no blocking error was encountered
  * FATAL: A fatal error occured while processing, the process was not done completely or even completely aborted
  
.. _country_code:  

country_code
------------

Description
^^^^^^^^^^^

ISO_3166-1 alpha-3 (3 characters) representation of the country (cf. https://www.iso.org/iso-3166-country-codes.html)

Restriction
^^^^^^^^^^^

* Extends: xs:string
* Match pattern: [a-zA-Z]{3}

.. _severity_code: 

severity_code
-------------

Description
^^^^^^^^^^^

Describes the severity of an error that was encountered during processing

Restriction
^^^^^^^^^^^

* Extends: xs:string
* Enumerate:
  
  * WARNING: Some non mandatory data may have been invalid, this data was ignored, the process ignored this error and continued
  * ERROR: Some mandatory data may have been invalid, or a processing error occured, the parent element (order line or offer) may not have been processed, a retry will be necessary
  * FATAL: A fatal error occured while processing the file, some or all data in the stream may not have been processed, check the stream and resend it
  
  
.. _quantity_0_9999:

quantity_0_9999
---------------

Restriction
^^^^^^^^^^^

* Extends: xs:nonNegativeInteger
* Greater than or equal to: 0
* Lesser than or equal to: 9999

.. _price_090_20000:

price_090_20000
---------------

Restriction
^^^^^^^^^^^

* Extends: xs:decimal
* Greater than or equal to: 0.89
* Lesser than or equal to: 20000

.. _order_id_restriction:

order_id_restriction
--------------------

Restriction
^^^^^^^^^^^

* Extends: xs:string
* Min length: 13
* Max length: 13

.. _offer_treatment:

offer_treatment
---------------

Description
^^^^^^^^^^^

Defines a specific treatment for an offer

Restriction
^^^^^^^^^^^

* Extends: xs:string
* Enumerate:

  * delete: Delete the offer

.. _logistic_type_id:

logistic_type_id
----------------

Description
^^^^^^^^^^^

Sets the logistic type of a product (generally according to the category, the kind, the size or the weight of the product).
Setting this value will override the default logistic type defined by Fnac on the product.
Please note that 5 of these logistic types are available to qualified sellers only (types 206 to 210).

Restriction
^^^^^^^^^^^

* Extends: xs:string
* Enumerate:

  * 101: Logistic type / Category A
  * 102: Logistic type / Category B
  * 103: Logistic type / Category C
  * 104: Logistic type / Category D
  * 105: Logistic type / Category E
  * 106: Logistic type / Category F
  * 107: Logistic type / Category G
  * 108: Logistic type / Category H
  * 109: Logistic type / Category I
  * 110: Logistic type / Category J
  * 111: Logistic type / Category K
  * 201: Logistic type / Category custom #1
  * 202: Logistic type / Category custom #2
  * 203: Logistic type / Category custom #3
  * 204: Logistic type / Category custom #4
  * 205: Logistic type / Category custom #5
  * 206: Logistic type / Category large products #1 (qualified sellers only)
  * 207: Logistic type / Category large products #2 (qualified sellers only)
  * 208: Logistic type / Category large products #3 (qualified sellers only)
  * 209: Logistic type / Category large products #4 (qualified sellers only)
  * 210: Logistic type / Category large products #5 (qualified sellers only)

.. _promotion:

promotion
---------

Attributes
----------

+----------+-------------------------------------------+-----------------------------+----------+---------+
| Name     | Description                               | Type                        | Required | Default |
+==========+===========================================+=============================+==========+=========+
| type     | Type of promotion                         | :ref:`promotion_type`       | required |         |
+----------+-------------------------------------------+-----------------------------+----------+---------+


Elements
--------

+------------------------+---------------------------------------------+----------------------------+-------------+-+
| Name                   | Description                                 | Type                       | Occurrence  | |
+========================+=============================================+============================+=============+=+
| sales_period_reference | Mandatory for Sales to indicate legal dates | xs:string255               | 0-1         | |
+------------------------+---------------------------------------------+----------------------------+-------------+-+
| promotion_uid          | Personal reference just for you             | :ref:`promotion_uid`       | 0-1         | |
+------------------------+---------------------------------------------+----------------------------+-------------+-+
| starts_at              | Promotion's start date                      | xs:dateTime                | 1-1         | |
+------------------------+---------------------------------------------+----------------------------+-------------+-+
| ends_at                | Promotion's end date                        | xs:dateTime                | 1-1         | |
+------------------------+---------------------------------------------+----------------------------+-------------+-+
| discount_type          | Type of discount                            | :ref:`discount_type`       | 0-1         | |
+------------------------+---------------------------------------------+----------------------------+-------------+-+
| discount_value         | Discount's value                            | xs:decimal                 | 0-1         | |
+------------------------+---------------------------------------------+----------------------------+-------------+-+
| triggers               | Rules that will trigger the promotion       | :ref:`triggers`            | 0-1         | |
+------------------------+---------------------------------------------+----------------------------+-------------+-+

.. note::

  Notice on sales_period_reference

  You must retrieve the reference from the :ref:`sales_periods_query service <sales_periods_query_service>`

.. _promotion_type:

promotion_type
--------------

Description
^^^^^^^^^^^

Used to indicate which type of promotion you want to add to an offer.

Restriction
^^^^^^^^^^^

* Extends: xs:string
* Enumerate:

  * Sales: to add a promotion of type Sales, those are managed by our teams to match legal dates.
  * Destocking: to add a promotion of type Destocking
  * GoodDeal: to add a promotion of type Good Deal
  * Batch: to add a promotion of type Batch. This one requires that you provide a number of items of the same offer that must be in the client's cart to trigger the promotion
  * FlashSale: to add a promotion of type Flash Sale. This one is activated just by adding the offer to the cart, no specific triggers.
  * FreeShipping: to add a promotion of type Free Shipping, in case you only want adherents to have free shipping or for a certain period for example.

.. _promotion_uid:

promotion_uid
-------------

Description
^^^^^^^^^^^

This is a reference that is not required and which you and you only will be able to use to organize your promotions for example. It must be alphanumeric.

Restriction
^^^^^^^^^^^

* Extends: xs:string
* Match pattern: [a-zA-Z0-9]+
* Max length: 50

.. _discount_type:

discount_type
-------------

Description
^^^^^^^^^^^

Used to indicate which type of discount you want to sub to the offer's price.

Restriction
^^^^^^^^^^^

* Extends: xs:string
* Enumerate:

  * percentage: discount_value will be used as a percentage.
  * fixed: discount_value will be used as an amount in euros

.. _triggers:

triggers
========

Elements
--------

+------------------------+-------------------------------------------------------------+-------------------------------+-------------+-+
| Name                   | Description                                                 | Type                          | Occurrence  | |
+========================+=============================================================+===============================+=============+=+
| trigger_cart           | Mandatory for Batch                                         | :ref:`trigger_cart`           | 0-1         | |
+------------------------+-------------------------------------------------------------+-------------------------------+-------------+-+
| trigger_promotion_code | Code that the client will have to give to trigger promotion | :ref:`trigger_promotion_code` | 0-1         | |
+------------------------+-------------------------------------------------------------+-------------------------------+-------------+-+
| trigger_customer_type  | Client segmentation                                         | :ref:`trigger_customer_type`  | 1-1         | |
+------------------------+-------------------------------------------------------------+-------------------------------+-------------+-+

.. _trigger_cart:

trigger_cart
------------

Restriction
^^^^^^^^^^^

* Extends: xs:decimal

Attributes
----------

+----------+------------------------------------+-----------------------------+----------+---------+
| Name     | Description                        | Type                        | Required | Default |
+==========+====================================+=============================+==========+=========+
| type     | Type of cart trigger               | :ref:`trigger_cart_type`    | required |         |
+----------+------------------------------------+-----------------------------+----------+---------+

.. _trigger_cart_type:

trigger_cart_type
-----------------

Restriction
^^^^^^^^^^^

* Extends: xs:string
* Enumerate:

  * MinSellerAmount: Total amount of your offers in the client's cart (decimal)
  * MinSellerQuantity: Total quantity of your offers in the client's cart (integer)
  * MinOfferQuantity: Total quantity of ONE of your offers in the client's cart (integer) - Mandatory for Batch promotion

.. _trigger_promotion_code:

trigger_promotion_code
----------------------

Description
^^^^^^^^^^^

Code that the client will have to give to trigger the promotion. It must be alphanumeric.

Restriction
^^^^^^^^^^^

* Extends: xs:string
* Match pattern: [a-zA-Z0-9]+
* Max length: 50

.. _trigger_customer_type:

trigger_customer_type
---------------------

Description
^^^^^^^^^^^

You chose if you want the promotion to be for all clients or only adherents.

Restriction
^^^^^^^^^^^

* Extends: xs:string
* Enumerate:

  * all
  * adherent
