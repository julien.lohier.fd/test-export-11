.. contents::
   :depth: 3
   :backlinks: none

.. _orders_query_service:

Orders Query
^^^^^^^^^^^^

Informations
============

Description
-----------

This is service is used to retrieve orders made on your offers according to submitted criteria.

Process
-------

#. A valid authentification token is needed.
#. The partners calls the service with criteria.
#. The service returns a list of orders according to the submitted criteria, with their order details.

Limitation
----------

You can get a maximum of 50 orders per call, above that amount an error is returned.

Request
=======

URL to use
----------

* Staging: https://partners-test.mp.fnacdarty.com/api.php/orders_query
* Production: https://vendeur.fnac.com/api.php/orders_query

Root element
------------

* Name: orders_query
* Extends: :ref:`base_request`

Attributes
----------

+---------------+--------------------------------+--------------------+----------+---------+
| Name          | Description                    | Type               | Required | Default |
+===============+================================+====================+==========+=========+
| results_count | Max number of results per page | xs:positiveInteger | optional | 100     |
+---------------+--------------------------------+--------------------+----------+---------+


Elements
--------

+-----------------+------------------------------------------+-----------------------------------+------------+
| Name            | Description                              | Type                              | Occurrence |
+=================+==========================================+===================================+============+
| paging          | Page number to fetch                     | xs:int                            | 0-1        |
+-----------------+------------------------------------------+-----------------------------------+------------+
| date            | Date filter                              | :ref:`order_date_constraint`      | 0-1        |
+-----------------+------------------------------------------+-----------------------------------+------------+
| sort_by         | Quantity filter                          | :ref:`order_sort_constraint_type` | 0-1        |
+-----------------+------------------------------------------+-----------------------------------+------------+
| product_fnac_id | Product unique identifier from fnac      | xs:string                         | 0-1        |
+-----------------+------------------------------------------+-----------------------------------+------------+
| offer_fnac_id   | Offer unique identifier from fnac        | xs:string                         | 0-1        |
+-----------------+------------------------------------------+-----------------------------------+------------+
| offer_seller_id | Offer unique identifier from seller      | xs:string                         | 0-1        |
+-----------------+------------------------------------------+-----------------------------------+------------+
| state           | Order state filter                       | :ref:`order_state`                |            |
+-----------------+------------------------------------------+-----------------------------------+------------+
| states          | Multiple order state filter              | :ref:`orders_state`               |            |
+-----------------+------------------------------------------+-----------------------------------+------------+
| order_fnac_id   | Order unique identifier filter           | :ref:`order_id_restriction`       |            |
+-----------------+------------------------------------------+-----------------------------------+------------+
| orders_fnac_id  | Multiple orders unique identifier filter | :ref:`orders_fnac_id_type`        |            |
+-----------------+------------------------------------------+-----------------------------------+------------+

XML Sample
----------

.. code-block:: xml
   
   <!-- Retrieve the 100 first new orders -->
   <?xml version="1.0" encoding="utf-8"?>
   <orders_query results_count="100" shop_id="BBBFA40E-3A94-2EE1-762A-2858EDE4F9BB" partner_id="C906104B-9B13-611D-6104-261780F88E38" token="AE2F633D-48E4-CEB8-39C7-8713AAE28DE3" xmlns="http://www.fnac.com/schemas/mp-dialog.xsd">
     <paging>1</paging>
     <states>
       <state><![CDATA[Created]]></state>
     </states>
   </orders_query>


.. code-block:: xml

   <!-- Retrieve 10 orders that have been set in ToShip state between 2 given dates -->
   <?xml version="1.0" encoding="utf-8"?>
   <orders_query results_count="10" partner_id="789026A2-5B4B-D30A-CD4F-8132B9621A79" shop_id="D6266346-F481-D3BB-2FDF-8BCF22F7B9FF" token="9E778BC4-492B-99FD-5091-EF2F837A8D97" xmlns="http://www.fnac.com/schemas/mp-dialog.xsd">
     <paging>1</paging>
     <states>
       <state><![CDATA[ToShip]]></state>
     </states>
     <date type="UpdatedAt">
       <min>2013-11-18T00:00:00+01:00</min>
       <max>2013-11-18T12:58:43+01:00</max>
     </date>
   </orders_query>


.. code-block:: xml

  <!-- Retrieve orders by their Fnac id -->
  <?xml version="1.0" encoding="utf-8"?>
  <orders_query partner_id="789026A2-5B4B-D30A-CD4F-8132B9621A79" shop_id="D6266346-F481-D3BB-2FDF-8BCF22F7B9FF" token="9E778BC4-492B-99FD-5091-EF2F837A8D97" xmlns="http://www.fnac.com/schemas/mp-dialog.xsd">
    <orders_fnac_id>
      <order_fnac_id>1OYAJ593GPYG4</order_fnac_id>
      <order_fnac_id>00DP7KK837SUO</order_fnac_id>
      <order_fnac_id>0FI06VER1XBMW</order_fnac_id>
      <order_fnac_id>0UEF9MJP88A14</order_fnac_id>
      <order_fnac_id>0MVFKIH60L76M</order_fnac_id>
    </orders_fnac_id>
  </orders_query>

Response
========

Root element
------------

* Name: orders_query_response
* Extends: :ref:`base_response`


Elements
--------

+-------------------+----------------------------+--------------+-------------+-+
| Name              | Description                | Type         | Occurrence  | |
+===================+============================+==============+=============+=+
| error             | Errors                     | :ref:`error` | 0-unbounded | |
+-------------------+----------------------------+--------------+-------------+-+
| page              | Page number                | xs:int       | 0-1         | |
+-------------------+----------------------------+--------------+-------------+-+
| total_paging      | Number of pages available  | xs:int       | 0-1         | |
+-------------------+----------------------------+--------------+-------------+-+
| nb_total_per_page | Number of results per page | xs:int       | 0-1         | |
+-------------------+----------------------------+--------------+-------------+-+
| nb_total_result   | Number of results          | xs:int       | 0-1         | |
+-------------------+----------------------------+--------------+-------------+-+
| order             | Orders                     | :ref:`order` | 0-unbounded | |
+-------------------+----------------------------+--------------+-------------+-+

XML Sample
----------

.. code-block:: xml
   
    <?xml version="1.0" encoding="utf-8"?>
    <orders_query_response xmlns="http://www.fnac.com/schemas/mp-dialog.xsd" status="OK">
        <page>1</page>
        <total_paging>4</total_paging>
        <nb_total_per_page>1</nb_total_per_page>
        <nb_total_result>4</nb_total_result>
        <order>
            <order_id>1PHUEDUQFOAAA</order_id>
            <state>ToShip</state>
            <order_detail>
                <order_detail_id>1</order_detail_id>
                <state>ToShip</state>
                <product_name><![CDATA[2Lor en moi ( Audio Variété ) - CD album]]></product_name>
                <quantity>1</quantity>
                <price>12</price>
                <product_vat_rate>5.5</product_vat_rate>
                <unit_product_vat_amount>0.63</unit_product_vat_amount>
                <fees>0.84</fees>
                <product_fnac_id>1495709</product_fnac_id>
                <offer_fnac_id>8D38BEF6-DCD7-5628-7288-AAAAAAAAAAAA</offer_fnac_id>
                <offer_seller_id><![CDATA[poney]]></offer_seller_id>
                <product_state>11</product_state>
                <description><![CDATA[Description on the offer]]></description>
                <internal_comment><![CDATA[]]></internal_comment>
                <shipping_price>3.99</shipping_price>
                <shipping_vat_amount>0.21</shipping_vat_amount>
                <shipping_method>20</shipping_method>
                <created_at>2021-04-07T11:39:32+02:00</created_at>
                <debited_at>2021-04-07T11:40:38+02:00</debited_at>
                <product_url><![CDATA[http://musique.fnac.com/2Lor-en-moi-Lorie-neuf-occasion?PRID=2066124&REF=FnacDirect]]></product_url>
                <image><![CDATA[http://multimedia.fnac.com/multimedia/images_produits/Grandes/3/2/3/0886971942323.gif]]></image>
                <expedition_country_code><![CDATA[TLS]]></expedition_country_code>
                <tracking_number><![CDATA[]]></tracking_number>
                <type_label><![CDATA[Livre]]></type_label>
            </order_detail>
            <platform_vat_number>987654321</platform_vat_number>
            <platform_ioss_number>123456789</platform_ioss_number>
            <shop_id>3E8B2A30-E0F5-6A7B-47B0-AAAAAAAAAAAA</shop_id>
            <client_id>b53fb9b6-7fae-4b32-84c3-aaaaaaaaaaaa</client_id>
            <client_firstname><![CDATA[Antoine]]></client_firstname>
            <client_lastname><![CDATA[DUPONT]]></client_lastname>
            <client_email>client-278-ca904e9c554afe1f68f7892aaaaaaaaa@marketplace.fnac.com</client_email>
            <created_at>2021-04-07T11:39:32+02:00</created_at>
            <updated_at>2021-04-07T11:40:39+02:00</updated_at>
            <fees>0.84</fees>
            <vat_rate>20</vat_rate>
            <nb_messages>0</nb_messages>
            <delivery_note><![CDATA[https://URLtoDeliveryNote/token]]></delivery_note>
            <max_expedition_date>2021-04-13</max_expedition_date>
            <max_delivery_date>2021-04-16</max_delivery_date>
            <shipping_address>
                <firstname><![CDATA[Antoine]]></firstname>
                <lastname><![CDATA[DUPONT]]></lastname>
                <company><![CDATA[]]></company>
                <address1><![CDATA[1 rue du test]]></address1>
                <address2><![CDATA[]]></address2>
                <address3><![CDATA[]]></address3>
                <zipcode><![CDATA[75001]]></zipcode>
                <city><![CDATA[PARIS]]></city>
                <country><![CDATA[FRA]]></country>
                <state><![CDATA[]]></state>
            </shipping_address>
            <billing_address>
                <firstname><![CDATA[Antoine]]></firstname>
                <lastname><![CDATA[DUPONT]]></lastname>
                <company><![CDATA[]]></company>
                <address1><![CDATA[7 rue du test]]></address1>
                <address2><![CDATA[]]></address2>
                <address3><![CDATA[]]></address3>
                <zipcode><![CDATA[75001]]></zipcode>
                <city><![CDATA[PARIS]]></city>
                <country><![CDATA[FRA]]></country>
            </billing_address>
        </order>
    </orders_query_response>
   
   
.. _order_sort_constraint_type:

order_sort_constraint_type
==========================

Restriction
-----------

* Extends: xs:string
* Enumerate:
  
  * DESC: Descending sort
  * ASC: Ascending sort
  
.. _orders_state:

order_states
============

Elements
--------

+-------+-------------+--------------------+-------------+-+
| Name  | Description | Type               | Occurrence  | |
+=======+=============+====================+=============+=+
| state | Order state | :ref:`order_state` | 0-unbounded | |
+-------+-------------+--------------------+-------------+-+

.. _orders_fnac_id_type:

orders_fnac_id_type
===================

Elements
--------

+---------------+-----------------------------------+-----------------------------+-------------+-+
| Name          | Description                       | Type                        | Occurrence  | |
+===============+===================================+=============================+=============+=+
| order_fnac_id | Order unique identifier from fnac | :ref:`order_id_restriction` | 0-unbounded | |
+---------------+-----------------------------------+-----------------------------+-------------+-+

.. _order_date_constraint:

order_date_constraint
=====================

Attributes
----------

+------+----------------------+-------------------------------+----------+
| Name | Description          | Type                          | Required |
+======+======================+===============================+==========+
| type | Date type for filter | :ref:`order_state_constraint` | required |
+------+----------------------+-------------------------------+----------+


Elements
--------

+------+-------------+-------------+------------+-+
| Name | Description | Type        | Occurrence | |
+======+=============+=============+============+=+
| min  | Min date    | xs:dateTime | 0-1        | |
+------+-------------+-------------+------------+-+
| max  | Max date    | xs:dateTime | 0-1        | |
+------+-------------+-------------+------------+-+

.. _order:

order
=====

Elements
--------

+-------------------------+---------------------------------------------+-----------------------+-------------+-+
| Name                    | Description                                 | Type                  | Occurrence  | |
+=========================+=============================================+=======================+=============+=+
| shop_id                 | Shop unique identifier from fnac            | :ref:`uuid`           | 1-1         | |
+-------------------------+---------------------------------------------+-----------------------+-------------+-+
| client_id               | Client unique identifier from fnac          | :ref:`uuid`           | 1-1         | |
+-------------------------+---------------------------------------------+-----------------------+-------------+-+
| client_firstname        | Client firstname                            | :ref:`string100`      | 1-1         | |
+-------------------------+---------------------------------------------+-----------------------+-------------+-+
| client_lastname         | Client lastname                             | :ref:`string100`      | 1-1         | |
+-------------------------+---------------------------------------------+-----------------------+-------------+-+
| client_email            | Client email (crypted)                      | :ref:`string100`      | 0-1         | |
+-------------------------+---------------------------------------------+-----------------------+-------------+-+
| adherent_number         | Client adherent number                      | xs:string             | 0-1         | |
+-------------------------+---------------------------------------------+-----------------------+-------------+-+
| order_id                | Order unique identifier from fnac           | xs:string             | 1-1         | |
+-------------------------+---------------------------------------------+-----------------------+-------------+-+
| client_culture          | Client culture                              | :ref:`string5`        | 0-1         | |
+-------------------------+---------------------------------------------+-----------------------+-------------+-+
| state                   | Order state                                 | :ref:`order_state`    | 1-1         | |
+-------------------------+---------------------------------------------+-----------------------+-------------+-+
| created_at              | Order creation date                         | xs:dateTime           | 1-1         | |
+-------------------------+---------------------------------------------+-----------------------+-------------+-+
| updated_at              | Last order update date                      | xs:dateTime           | 1-1         | |
+-------------------------+---------------------------------------------+-----------------------+-------------+-+
| fees                    | Order fees, taxes included                  | xs:decimal            | 0-1         | |
+-------------------------+---------------------------------------------+-----------------------+-------------+-+
| nb_messages             | Number of messages associated to this order | xs:nonNegativeInteger | 0-1         | |
+-------------------------+---------------------------------------------+-----------------------+-------------+-+
| invoice_number_reference| Invoice reference                           | xs:string             | 0-1         | |
+-------------------------+---------------------------------------------+-----------------------+-------------+-+
| vat_rate                | VAT rate applied on the order fees          | xs:decimal            | 0-1         | |
+-------------------------+---------------------------------------------+-----------------------+-------------+-+
| delivery_note           | Url of delivery note in PDF                 | xs:string             | 0-1         | |
+-------------------------+---------------------------------------------+-----------------------+-------------+-+
| shipping_address        | Client shipping address                     | :ref:`address`        | 0-1         | |
+-------------------------+---------------------------------------------+-----------------------+-------------+-+
| billing_address         | Client billing address                      | :ref:`address`        | 0-1         | |
+-------------------------+---------------------------------------------+-----------------------+-------------+-+
| max_expedition_date     | Maximum expected expedition date            | xs:dateTime           | 1-1         | |
+-------------------------+---------------------------------------------+-----------------------+-------------+-+
| max_delivery_date       | Maximum expected delivery date              | xs:dateTime           | 1-1         | |
+-------------------------+---------------------------------------------+-----------------------+-------------+-+
| platform_vat_number     | Platform VAT number (VIES)                  | xs:string             | 0-1         | |
+-------------------------+---------------------------------------------+-----------------------+-------------+-+
| platform_ioss_number    | Platform IOSS number                        | xs:string             | 0-1         | |
+-------------------------+---------------------------------------------+-----------------------+-------------+-+
| order_detail            | Orders details                              | :ref:`order_detail`   | 0-unbounded | |
+-------------------------+---------------------------------------------+-----------------------+-------------+-+

.. _order_detail:

order_detail
============


Elements
--------

+---------------------------+------------------------------------------------+---------------------------+------------+-+
| Name                      | Description                                    | Type                      | Occurrence | |
+===========================+================================================+===========================+============+=+
| order_detail_id           | Order detail unique identifier from fnac       | xs:positiveInteger        | 1-1        | |
+---------------------------+------------------------------------------------+---------------------------+------------+-+
| state                     | Order detail state                             | :ref:`order_detail_state` | 1-1        | |
+---------------------------+------------------------------------------------+---------------------------+------------+-+
| product_fnac_id           | Product unique identifier from fnac            | xs:string                 | 1-1        | |
+---------------------------+------------------------------------------------+---------------------------+------------+-+
| product_name              | Product name                                   | xs:string                 | 1-1        | |
+---------------------------+------------------------------------------------+---------------------------+------------+-+
| offer_fnac_id             | Offer unique identifier from fnac              | xs:string                 | 1-1        | |
+---------------------------+------------------------------------------------+---------------------------+------------+-+
| offer_seller_id           | Offer unique identitifer from seller           | xs:string                 | 1-1        | |
+---------------------------+------------------------------------------------+---------------------------+------------+-+
| price                     | Product price                                  | xs:decimal                | 1-1        | |
+---------------------------+------------------------------------------------+---------------------------+------------+-+
| fees                      | Order detail fees with all tax                 | xs:decimal                | 1-1        | |
+---------------------------+------------------------------------------------+---------------------------+------------+-+
| product_state             | Product state                                  | :ref:`product_state`      | 1-1        | |
+---------------------------+------------------------------------------------+---------------------------+------------+-+
| quantity                  | Product quantity ordered                       | xs:nonNegativeInteger     | 1-1        | |
+---------------------------+------------------------------------------------+---------------------------+------------+-+
| description               | Offer description                              | :ref:`string255`          | 0-1        | |
+---------------------------+------------------------------------------------+---------------------------+------------+-+
| internal_comment          | Order detail internal comment for personal use | :ref:`string255`          | 0-1        | |
+---------------------------+------------------------------------------------+---------------------------+------------+-+
| debited_at                | Debited date of order detail                   | xs:dateTime               | 0-1        | |
+---------------------------+------------------------------------------------+---------------------------+------------+-+
| created_at                | Creation date of order detail                  | xs:dateTime               | 0-1        | |
+---------------------------+------------------------------------------------+---------------------------+------------+-+
| received_at               | Receiving date of order detail                 | xs:dateTime               | 0-1        | |
+---------------------------+------------------------------------------------+---------------------------+------------+-+
| refused_at                | Refusing date of order detail                  | xs:dateTime               | 0-1        | |
+---------------------------+------------------------------------------------+---------------------------+------------+-+
| product_url               | Product url on fnac.com                        | xs:string                 | 0-1        | |
+---------------------------+------------------------------------------------+---------------------------+------------+-+
| image                     | Product image url on fnac.com                  | xs:string                 | 0-1        | |
+---------------------------+------------------------------------------------+---------------------------+------------+-+
| tracking_number           | Order detail tracking number                   | :ref:`string32`           | 0-1        | |
+---------------------------+------------------------------------------------+---------------------------+------------+-+
| xeett                     | xeett code (Delivery point mode)               | :ref:`string100`          | 0-1        | |
+---------------------------+------------------------------------------------+---------------------------+------------+-+
| pseudorvc                 | pseudorvc (Delivery point mode)                | :ref:`string100`          | 0-1        | |
+---------------------------+------------------------------------------------+---------------------------+------------+-+
| tracking_client_id        | client id needed by Delivery point mode        | :ref:`string100`          | 0-1        | |
+---------------------------+------------------------------------------------+---------------------------+------------+-+
| shipping_price            | Shipping price with all tax for all product    | xs:decimal                | 0-1        | |
+---------------------------+------------------------------------------------+---------------------------+------------+-+
| shipping_method           | Shipping method                                | :ref:`shipping_method`    | 0-1        | |
+---------------------------+------------------------------------------------+---------------------------+------------+-+
| promotion_type            | Type of promotion                              | :ref:`promotion_type`     | 0-1        | |
+---------------------------+------------------------------------------------+---------------------------+------------+-+
| promotion_uid             | Your reference for this promotion              | :ref:`promotion_uid`      | 0-1        | |
+---------------------------+------------------------------------------------+---------------------------+------------+-+
| product_vat_rate          | VAT rate used for VAT collection               | xs:decimal                | 0-1        | |
+---------------------------+------------------------------------------------+---------------------------+------------+-+
| unit_product_vat_amount   | Unit product VAT amount collected              | xs:decimal                | 0-1        | |
+---------------------------+------------------------------------------------+---------------------------+------------+-+
| shipping_vat_amount       | Shipping VAT amount collected                  | xs:decimal                | 0-1        | |
+---------------------------+------------------------------------------------+---------------------------+------------+-+
