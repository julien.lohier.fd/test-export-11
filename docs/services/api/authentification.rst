.. contents::
   :depth: 3
   :backlinks: none

Authentification
^^^^^^^^^^^^^^^^

Informations
============

Description
-----------

This service is used to retrieve a connection token from our server which is necessary to call any authenticated services.


Process
-------

#. Each partner is provided a unique partner_id, a unique shop_id and a unique key. The association (partner_id, shop_id, key) allows the partner to authenticate on FnacMarketPlace API.
#. The partner calls this authentication webservice with his partner_id, his shop_id and his key.
#. The service returns a temporary token and an expiration date.

Request
=======

URL to use
----------

* Staging: https://partners-test.mp.fnacdarty.com/api.php/auth
* Production: https://vendeur.fnac.com/api.php/auth


Root element
------------

* Name: auth

Elements
--------

Following elements are provided by Fnac support. They differ from staging to production environment. 

+------------+----------------------------+-------------+-------------+-+
| Name       | Description                | Type        | Occurrences | |
+============+============================+=============+=============+=+
| partner_id | Fnac partner unique id     | :ref:`uuid` | 1-1         | |
+------------+----------------------------+-------------+-------------+-+
| shop_id    | Fnac shop unique id        | :ref:`uuid` | 1-1         | |
+------------+----------------------------+-------------+-------------+-+
| key        | Partner authentication key | xs:string   | 1-1         | |
+------------+----------------------------+-------------+-------------+-+

XML Sample
----------

.. literalinclude:: ../examples/auth_request.xml
   :language: xml

Response
========

Root element
------------

* Name: auth_response
* Extends: :ref:`base_response`

Elements
--------

+----------+---------------------------------+-------------+-------------+-+
| Name     | Description                     | Type        | Occurrences | |
+==========+=================================+=============+=============+=+
| token    | Valid token for current session | :ref:`uuid` | 0-1         | |
+----------+---------------------------------+-------------+-------------+-+
| validity | Date when session will expire   | xs:dateTime | 0-1         | |
+----------+---------------------------------+-------------+-------------+-+
| version  | Current version of the API      | xs:string   | 0-1         | |
+----------+---------------------------------+-------------+-------------+-+

XML Sample
----------

.. literalinclude:: ../examples/auth_response.xml
   :language: xml
   