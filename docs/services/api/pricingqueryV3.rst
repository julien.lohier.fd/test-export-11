.. contents::
   :depth: 4
   :backlinks: none

.. _pricing-query-v3:

Pricing Query (V3)
^^^^^^^^^^^^^^^^^^

.. note::
   
   Be advised that requests from shops operating outside of France have accessed only to :ref:`pricing_query v1 <pricing-query-v1>`. Shops operating in France have access to :ref:`pricing_query v2 <pricing-query-v2>` and :ref:`pricing_query v3 <pricing-query-v3>` as well.

Informations
============

Description
-----------

Retrieve the best prices applied to a given product within all Fnac marketplace sellers (Fnac included) with extra information such as the sellers' name, and quality indicators.

Process
-------

#. A valid authentification token is needed.
#. The partner calls service with a list of product reference
#. The service returns for each product reference a list of the lowest prices suggested by Fnac Marketplace sellers.

The number of product references to request is limited to 200.

Request
=======

URL to use
----------

* Staging: https://partners-test.mp.fnacdarty.com/api.php/prices_query
* Production: https://vendeur.fnac.com/api.php/prices_query

Root element
------------

* Name: pricing_query
* Extends: :ref:`base_request`

Attributes
----------

+------------------------+------------------------------------------------+-------------------------------+----------+-----------+
|          Name          |                    Description                 |        Type                   | Required | Default   |
+========================+================================================+===============================+==========+===========+
| sellers                |     Type of sellers                            |:ref:`seller_filter`           | optionnal|    all    |
+------------------------+------------------------------------------------+-------------------------------+----------+-----------+
| states                 |     Product states                             |:ref:`state_filter`            | optionnal|    all    |
+------------------------+------------------------------------------------+-------------------------------+----------+-----------+
| prices                 |     Type of prices                             |:ref:`price_filter`            | optionnal|    all    |
+------------------------+------------------------------------------------+-------------------------------+----------+-----------+


.. _seller_filter:

seller_filter
-------------

Description
"""""""""""

Type of sellers 


Restriction
"""""""""""

* Extends: xs:string
* Enumerate:

  * all: All sellers including your offer
  * all-pro: Only pro sellers including your offer
  * others: All sellers, your offer excluded
  * others-pro: Only pro sellers, your offer excluded


.. _state_filter:

state_filter
------------

Description
"""""""""""

Product states 


Restriction
"""""""""""

* Extends: xs:string
* Enumerate:

  * all: All states
  * new
  * refurbished
  * used-as-new
  * used-very-good
  * used-good
  * used-correct
  * collection-as-new
  * collection-very-good
  * collection-good
  * collection-correct


.. _price_filter:

price_filter
------------

Description
"""""""""""

Type of prices 

Restriction
"""""""""""

* Extends: xs:string
* Enumerate:

  * all: All offers
  * standard : Exclude "adherent" (Fnac member) offers
  * adherent : Exclude standard offers


Elements
--------

+-------------------+-------------------------------------+--------------------------------+-------------+-+
| Name              | Description                         | Type                           | Occurrence  | |
+===================+=====================================+================================+=============+=+
| product_reference | product_reference value             | :ref:`product_referencev3`     | 1-200       | |
|                   | for the type attribute value        |                                |             | |
|                   | (Ean, PartnerId, etc)               |                                |             | |
+-------------------+-------------------------------------+--------------------------------+-------------+-+


.. _product_referencev3:

product_reference
-----------------

Description
"""""""""""

Represents a product reference (id and type)

Restriction
"""""""""""

* Extends: xs:string

Attributes
""""""""""

+------------------------+----------------------------+------------------------------------------+----------+-----------+
|          Name          |         Description        |        Type                              | Required | Default   |
+========================+============================+==========================================+==========+===========+
| type                   |                            | :ref:`commercial_product_reference_type` | required |           |
+------------------------+----------------------------+------------------------------------------+----------+-----------+

XML Sample
----------

.. code-block:: xml
   
   <?xml version="1.0" encoding="utf-8"?>
   <pricing_query xmlns="http://www.fnac.com/schemas/mp-dialog.xsd" shop_id="BBBFA40E-3A94-2EE1-762A-2858EDE4F9BB" partner_id="C906104B-9B13-611D-6104-261780F88E38" token="6EB5F43F-33CF-5C33-D204-278907C0FD9D"
   sellers="all" states="all" prices="all" 
   >
     <product_reference type="Ean">0886971942323</product_reference>
   </pricing_query>


Response
========

Root element
------------

* Name: pricing_query_response
* Extends: :ref:`base_response`

Elements
--------

+-----------------+---------------------+------------------------+-------------+-+
| Name            | Description         | Type                   | Occurrence  | |
+=================+=====================+========================+=============+=+
| error           | Errors              | :ref:`error`           | 0-unbounded | |
+-----------------+---------------------+------------------------+-------------+-+
| pricing_products| Pricings of product | :ref:`prices_products` | 0-1         | |
+-----------------+---------------------+------------------------+-------------+-+

XML Sample
----------

.. code-block:: xml

    <?xml version="1.0" encoding="utf-8"?>
    <pricing_query_response status="OK" xmlns="http://www.fnac.com/schemas/mp-dialog.xsd">
        <pricing_products>
            <pricing_product>
                <product_reference type="FnacId">11212171</product_reference>
                <product_name>
                    <![CDATA[Accessoires pour toboggan à billes - Crampons de maintien et rampes]]>
                </product_name>
                <product_url>
                    <![CDATA[http://www.fnac.com/mp11212171/Accessoires-pour-toboggan-a-billes-Crampons-de-maintien-et-rampes/w-4]]>
                </product_url>
                <product_ean>4010168036502</product_ean>
                <standard>
                    <seller_offer>
                        <price>0</price>
                        <shipping_price>0</shipping_price>
                    </seller_offer>
                    <ranked_offers type="professional">
                        <seller_name>
                            <![CDATA[seller name]]>
                        </seller_name>
                        <seller_url>
                            <![CDATA[http://www.fnac.com/sref69caf457-28bf-8b32-f2ed-abfe912cb0c7]]>
                        </seller_url>
                        <seller_sales_number>13963</seller_sales_number>
                        <seller_reliability_rate>4.68</seller_reliability_rate>
                        <seller_expedition_country>FRA</seller_expedition_country>
                        <has_buybox>true</has_buybox>
                        <offer_status>1</offer_status>
                        <position>1</position>
                        <price>21.15</price>
                        <shipping_price>0.00</shipping_price>
                        <updated_at>2017-01-29T13:43:16.02</updated_at>
                    </ranked_offers>
                    <ranked_offers type="professional">
                        <seller_name>
                            <![CDATA[seller name]]>
                        </seller_name>
                        <seller_url>
                            <![CDATA[http://www.fnac.com/sreff2352265-03f8-b5b8-2357-a654fbfd4eb0]]>
                        </seller_url>
                        <seller_sales_number>70403</seller_sales_number>
                        <seller_reliability_rate>4.78</seller_reliability_rate>
                        <seller_expedition_country>FRA</seller_expedition_country>
                        <offer_status>1</offer_status>
                        <position>2</position>
                        <price>30.08</price>
                        <shipping_price>6.19</shipping_price>
                        <updated_at>2017-01-25T13:57:40.153</updated_at>
                    </ranked_offers>
                    <ranked_offers type="professional">
                        <seller_name>
                            <![CDATA[seller name]]>
                        </seller_name>
                        <seller_url>
                            <![CDATA[http://www.fnac.com/sref510899c6-cb5d-8563-d279-7ceedbd57592]]>
                        </seller_url>
                        <seller_sales_number>45730</seller_sales_number>
                        <seller_reliability_rate>4.76</seller_reliability_rate>
                        <seller_expedition_country>FRA</seller_expedition_country>
                        <offer_status>1</offer_status>
                        <position>3</position>
                        <price>30.99</price>
                        <shipping_price>6.49</shipping_price>
                        <updated_at>2017-01-16T18:12:05.317</updated_at>
                    </ranked_offers>
                </standard>
                <adherent>
                    <seller_offer>
                        <price>0</price>
                        <shipping_price>0</shipping_price>
                    </seller_offer>
                </adherent>
            </pricing_product>
        </pricing_products>
    </pricing_query_response>


Possible errors related pricing query
-------------------------------------

If no product is found for a product reference, the following error will be sent as a response :

.. code-block:: xml

   <?xml version="1.0" encoding="utf-8"?>
    <pricing_product>
      <product_reference type="Ean">0886971942323</product_reference>
      <error severity="ERROR" code="ERR_120">Service Pricing : Product not found.</error>
    </pricing_product>

If an undefined option for attributes type, sellers, states and prices is provided, you will receive an ERR_096 error 

If more than 200 product_reference per request are present in the query, you will receive an ERR_105 error 


.. _prices_products:

prices_products
---------------

Description
"""""""""""

Collection of price products 

Elements
""""""""

+-----------------+---------------------+------------------------+-------------+-+
| Name            | Description         | Type                   | Occurrence  | |
+=================+=====================+========================+=============+=+
+-----------------+---------------------+------------------------+-------------+-+
| pricing_product | Pricing of a product| :ref:`price_product`   | 0-unbounded | |
+-----------------+---------------------+------------------------+-------------+-+


.. _price_product:

price_product
-------------

Description
"""""""""""

Information about the product

Elements
""""""""

+-------------------+------------------------------------------+-------------------------------------+-------------+-+
| Name              | Description                              | Type                                | Occurrence  | |
+===================+==========================================+=====================================+=============+=+
+-------------------+------------------------------------------+-------------------------------------+-------------+-+
| product_reference | Reference used to match a product        | :ref:`commercial_product_reference` | 1-1         | |
+-------------------+------------------------------------------+-------------------------------------+-------------+-+
| product_name      | Name of the product                      | :ref:`string255`                    | 1-1         | |
+-------------------+------------------------------------------+-------------------------------------+-------------+-+
| product_url       | Url of the product on Fnac.com           | xs:string                           | 0-1         | |
+-------------------+------------------------------------------+-------------------------------------+-------------+-+
| product_ean       | Ean of the product                       | xs:string                           | 0-1         | |
+-------------------+------------------------------------------+-------------------------------------+-------------+-+
| standard          | Prices for non adherent                  | :ref:`standard_prices`              | 1-1         | |
+-------------------+------------------------------------------+-------------------------------------+-------------+-+
| adherent          | Prices for adherent                      | :ref:`adherent_prices`              | 1-1         | |
+-------------------+------------------------------------------+-------------------------------------+-------------+-+


.. _standard_prices:

standard_prices
---------------

Description
"""""""""""

Information about competitor's price for non "adherent" clients (non Fnac members)

Elements
""""""""

+-------------------+------------------------------------------+-------------------------------------+-------------+-+
| Name              | Description                              | Type                                | Occurrence  | |
+===================+==========================================+=====================================+=============+=+
+-------------------+------------------------------------------+-------------------------------------+-------------+-+
| seller_offer      | Seller's offer                           | :ref:`seller_offer`                 | 0-1         | |
+-------------------+------------------------------------------+-------------------------------------+-------------+-+
| ranked_offers     | Competitors prices                       | :ref:`ranked_offers`                | 0-unbounded | |
+-------------------+------------------------------------------+-------------------------------------+-------------+-+


.. _adherent_prices:

adherent_prices
---------------

Description
"""""""""""

Information about competitor's price for "adherent" clients (Fnac members)

Elements
""""""""

+-------------------+------------------------------------------+-------------------------------------+-------------+-+
| Name              | Description                              | Type                                | Occurrence  | |
+===================+==========================================+=====================================+=============+=+
+-------------------+------------------------------------------+-------------------------------------+-------------+-+
| seller_offer      | Seller's offer                           | :ref:`seller_offer`                 | 0-1         | |
+-------------------+------------------------------------------+-------------------------------------+-------------+-+
| ranked_offers     | Competitors prices                       | :ref:`ranked_offers`                | 0-unbounded | |
+-------------------+------------------------------------------+-------------------------------------+-------------+-+


.. _seller_offer:

seller_offer
------------

Description
"""""""""""

Information about the seller's offer

Elements
""""""""

+---------------------------+------------------------------------------+-------------------------------------+-------------+-+
| Name                      | Description                              | Type                                | Occurrence  | |
+===========================+==========================================+=====================================+=============+=+
+---------------------------+------------------------------------------+-------------------------------------+-------------+-+
| uid                       |                                          | xs:string                           | 0-1         | |
+---------------------------+------------------------------------------+-------------------------------------+-------------+-+
| offer_status              |                                          | :ref:`product_state`                | 0-1         | |
+---------------------------+------------------------------------------+-------------------------------------+-------------+-+
| price                     |                                          | :ref:`price_090_20000`              | 0-1         | |
+---------------------------+------------------------------------------+-------------------------------------+-------------+-+
| shipping_price            |                                          | xs:decimal                          | 0-1         | |
+---------------------------+------------------------------------------+-------------------------------------+-------------+-+
| updated_at                |                                          | xs:dateTime                         | 0-1         | |
+---------------------------+------------------------------------------+-------------------------------------+-------------+-+
| stock                     |                                          | xs:nonNegativeInteger               | 0-1         | |
+---------------------------+------------------------------------------+-------------------------------------+-------------+-+


.. _ranked_offers:

ranked_offers
-------------

Description
"""""""""""

Information about the competitor's offer

Elements
""""""""

+---------------------------+------------------------------------------+-------------------------------------+-------------+-+
| Name                      | Description                              | Type                                | Occurrence  | |
+===========================+==========================================+=====================================+=============+=+
+---------------------------+------------------------------------------+-------------------------------------+-------------+-+
| seller_name               |                                          | xs:string                           | 0-1         | |
+---------------------------+------------------------------------------+-------------------------------------+-------------+-+
| seller_url                |                                          | xs:string                           | 0-1         | |
+---------------------------+------------------------------------------+-------------------------------------+-------------+-+
| seller_sales_number       |                                          |  xs:nonNegativeInteger              | 0-1         | |
+---------------------------+------------------------------------------+-------------------------------------+-------------+-+
| seller_reliability_rate   |                                          | xs:decimal                          | 0-1         | |
+---------------------------+------------------------------------------+-------------------------------------+-------------+-+
| has_buybox                |                                          | xs:boolean                          | 0-1         | |
+---------------------------+------------------------------------------+-------------------------------------+-------------+-+
| offer_status              |                                          | :ref:`product_state`                | 0-1         | |
+---------------------------+------------------------------------------+-------------------------------------+-------------+-+
| position                  |                                          |  xs:nonNegativeInteger              | 0-1         | |
+---------------------------+------------------------------------------+-------------------------------------+-------------+-+
| price                     |                                          | :ref:`price_090_20000`              | 0-1         | |
+---------------------------+------------------------------------------+-------------------------------------+-------------+-+
| shipping_price            |                                          | xs:decimal                          | 0-1         | |
+---------------------------+------------------------------------------+-------------------------------------+-------------+-+
| updated_at                |                                          | xs:dateTime                         | 0-1         | |
+---------------------------+------------------------------------------+-------------------------------------+-------------+-+


Attributes
""""""""""

+------------------------+----------------------------+----------------------------------------+----+
|          Name          |         Description        |        Type                            |    |
+========================+============================+========================================+====+
| type                   |                            |:ref:`ranked_offers_type`               |    |
+------------------------+----------------------------+----------------------------------------+----+


.. _ranked_offers_type:

ranked_offers_type
------------------

Description
"""""""""""

Type of seller offer


Restriction
"""""""""""

* Extends: xs:string
* Enumerate:

  * professional: Professional seller offer
  * non-professional: Private seller offer
  * fnac: Fnac Offer