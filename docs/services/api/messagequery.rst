.. contents::
   :depth: 3
   :backlinks: none

Messages Query
^^^^^^^^^^^^^^

Informations
============

Description
-----------

This is service is used to retrieve messages related to your orders or offers.

Process
-------

#. A valid authentification token is needed.
#. The partner calls the service with criteria on the messages to retrieve.
#. The service returns a list of messages accordingly to the submitted criteria.

Request
=======

URL to use
----------

* Staging: https://partners-test.mp.fnacdarty.com/api.php/messages_query
* Production: https://vendeur.fnac.com/api.php/messages_query

Root element
------------

* Name: messages_query
* Extends: :ref:`base_request`

Attributes
----------

+---------------+--------------------------------+--------------------+----------+---------+
| Name          | Description                    | Type               | Required | Default |
+===============+================================+====================+==========+=========+
| results_count | Max number of results per page | xs:positiveInteger | optional | 100     |
+---------------+--------------------------------+--------------------+----------+---------+


Elements
--------

+--------------------+----------------------------------------------+--------------------------------+------------+
| Name               | Description                                  | Type                           | Occurrence |
+====================+==============================================+================================+============+
| paging             | Page number to fetch                         | xs:int                         | 0-1        |
+--------------------+----------------------------------------------+--------------------------------+------------+
| date               | Date filter                                  | message_date_constraint_in_    | 0-1        |
+--------------------+----------------------------------------------+--------------------------------+------------+
| message_type       | Messages' type filter                        | :ref:`message_type`            | 0-1        |
+--------------------+----------------------------------------------+--------------------------------+------------+
| message_archived   | Message is archived or not filter            | :ref:`message_archived_type`   | 0-1        |
+--------------------+----------------------------------------------+--------------------------------+------------+
| message_state      | Messages' state                              | :ref:`message_state_type`      | 0-1        |
+--------------------+----------------------------------------------+--------------------------------+------------+
| message_id         | Messages' unique identifier from fnac filter | :ref:`uuid`                    | 0-1        |
+--------------------+----------------------------------------------+--------------------------------+------------+
| order_fnac_id      | Order unique identifier from fnac filter     | xs:string                      | 0-1        |
+--------------------+----------------------------------------------+--------------------------------+------------+
| offer_fnac_id      | Offer unique identifier from fnac filter     | xs:string                      | 0-1        |
+--------------------+----------------------------------------------+--------------------------------+------------+
| offer_seller_id    | Offer unique identifier from seller filter   | xs:string                      | 0-1        |
+--------------------+----------------------------------------------+--------------------------------+------------+
| sort_by            | Sort filter                                  | :ref:`message_sort_by`         | 0-1        |
+--------------------+----------------------------------------------+--------------------------------+------------+
| message_from_types | Messages author filter                       | :ref:`from_types`              | 0-1        |
+--------------------+----------------------------------------------+--------------------------------+------------+

XML Sample
----------

.. code-block:: xml
   
   <?xml version="1.0" encoding="utf-8"?>
   <messages_query xmlns="http://www.fnac.com/schemas/mp-dialog.xsd" shop_id="73571111-3A94-2EE1-762A-2858EDE4F9BB" partner_id="F0A7BCF5-9B13-611D-6104-261780F88E38" token="469E283A-39B7-B0DA-6E7E-052B0ECB51E5" results_count="2">
     <paging>1</paging>
   </messages_query>

.. _message_type:

message_type
============

Type of object related to the message

* Extends: xs:string
* Enumerate:

  * OFFER: Message related to an offer
  * ORDER: Message related to an order
  
.. _message_date_type:

message_date_type
=================

Date constraint type of message

* Extends: xs:string
* Enumerate:
  
  * CREATED_AT: When the message has been created
  * UPDATED_AT: The last time the message was edited
  
.. _message_sort_type:

message_sort_type
=================

Sort constraint type of messages

* Extends: xs:string
* Enumerate:
  
  * DESC: Sort from recent to older date
  * ASC: Sort from older to recent date
  
.. _message_state_type:

message_state
=============

Current status of the message

* Extends: xs:string
* Enumerate:
  
  * READ: Message read by seller
  * UNREAD: Message unread by seller
  
.. _message_archived_type:

message_archived
================

Archived status of the message

* Extends: xs:string
* Enumerate:
  
  * TRUE: Message archived by seller
  * FALSE: Message not archived by seller
  
.. _from_type:

from_type
=========

Author type of messages

* Extends: xs:string
* Enumerate:
  
  * CLIENT: Message created by client
  * CALLCENTER: Message created by call center
  * SELLER: Message created by seller
  * SYSTEM: Message generated by system
  
.. _from_types:

from_types
==========

Elements
--------

+-----------+------------------------+------------------+-------------+
| Name      | Description            | Type             | Occurrence  |
+===========+========================+==================+=============+
| from_type | Author type of message | :ref:`from_type` | 0-unbounded |
+-----------+------------------------+------------------+-------------+


.. _message_sort_by:

message_sort_by
===============

Field on which messages are sorted

* Extends: :ref:`message_date_type`

Attributes
----------

+------+-------------+--------------------------+----------+---------+
| Name | Description | Type                     | Required | Default |
+======+=============+==========================+==========+=========+
| type | Sort order  | :ref:`message_sort_type` | optional |         |
+------+-------------+--------------------------+----------+---------+

.. _message_date_constraint_in:

message_date_constraint
=======================

Attributes
----------

+------+----------------------+--------------------------------+----------+
| Name | Description          | Type                           | Required |
+======+======================+================================+==========+
| type | Date type for filter | :ref:`message_date_constraint` | required |
+------+----------------------+--------------------------------+----------+


Elements
--------

+------+-------------+-------------+------------+
| Name | Description | Type        | Occurrence |
+======+=============+=============+============+
| min  | Min date    | xs:dateTime | 0-1        |
+------+-------------+-------------+------------+
| max  | Max date    | xs:dateTime | 0-1        |
+------+-------------+-------------+------------+

Response
========

Root element
------------

* Name: messages_query_response
* Extends: :ref:`base_response`


Elements
--------

+------------------+---------------------------+----------------+-------------+
| Name             | Description               | Type           | Occurrence  |
+==================+===========================+================+=============+
| error            | Errors                    | :ref:`error`   | 0-unbounded |
+------------------+---------------------------+----------------+-------------+
| page             | Page number               | xs:int         | 0-1         |
+------------------+---------------------------+----------------+-------------+
| total_paging     | Number of pages available | xs:int         | 0-1         |
+------------------+---------------------------+----------------+-------------+
| nb_total_per_page| Number of results per page| xs:int         | 0-1         |
+------------------+---------------------------+----------------+-------------+
| nb_total_result  | Number of results         | xs:int         | 0-1         |
+------------------+---------------------------+----------------+-------------+
| message          | Message list              | :ref:`message` | 0-unbounded |
+------------------+---------------------------+----------------+-------------+

XML Sample
----------

.. code-block:: xml
   
   <?xml version="1.0" encoding="utf-8"?>
   <messages_query_response xmlns="http://www.fnac.com/schemas/mp-dialog.xsd" status="OK">
      <page>1</page>
      <total_paging>50</total_paging>
      <nb_total_result>100</nb_total_result>
      <messages_unread_result>65</messages_unread_result>
      <messages_read_result>35</messages_read_result>
      <message state="READ" archived="FALSE">
        <message_id>B9CE8EEF-AC96-32C7-45EF-36B06C275302</message_id>
        <message_referer type="ORDER">1PEBVOQ3LFD60</message_referer>
        <message_from type="SELLER">UNKNOWN</message_from>
        <message_subject><![CDATA[Détail sur le produit]]></message_subject>
        <message_description><![CDATA[pourquoi pas ? ce soir 20 h ?]]></message_description>
        <created_at>2011-05-31 16:58:04</created_at>
        <updated_at>2011-05-31 16:58:04</updated_at>
      </message>
      <message state="UNREAD" archived="FALSE">
        <message_id>AE7A5365-79CF-71C6-BEB4-FA1492C1FB4D</message_id>
        <message_referer type="ORDER">0UPFR4H8PMF7A</message_referer>
        <message_from type="SELLER">UNKNOWN</message_from>
        <message_subject><![CDATA[Nouvelle réclamation]]></message_subject>
        <message_description><![CDATA[Une réclamation a été ouverte sur cette commande concernant le(s) article(s) suivant(s) : - 2Lor en moi, CD album [Audio Variété]]]></message_description>
        <created_at>2011-05-18 11:56:45</created_at>
        <updated_at>2011-05-18 11:56:45</updated_at>
      </message>
   </messages_query_response>

.. _message:

message
=======

Attributes
----------

+----------+------------------------------+------------------------------+----------+
| Name     | Description                  | Type                         | Required |
+==========+==============================+==============================+==========+
| state    | Message is read or not ?     | :ref:`message_state_type`    | required |
+----------+------------------------------+------------------------------+----------+
| archived | Message is archived or not ? | :ref:`message_archived_type` | required |
+----------+------------------------------+------------------------------+----------+

Elements
--------

+---------------------+----------------------------------------------+------------------------+------------+
| Name                | Description                                  | Type                   | Occurrence |
+=====================+==============================================+========================+============+
| message_id          | Message unique identifier                    | xs:string              | 1-1        |
+---------------------+----------------------------------------------+------------------------+------------+
| message_referer     | Message object reference                     | :ref:`message_referer` | 1-1        |
+---------------------+----------------------------------------------+------------------------+------------+
| message_offer_title | Message offer title if referer type is Offer | xs:string              | 0-1        |
+---------------------+----------------------------------------------+------------------------+------------+
| message_from        | Message sender                               | :ref:`message_from`    | 0-1        |
+---------------------+----------------------------------------------+------------------------+------------+
| message_subject     | Message subject                              | xs:string              | 1-1        |
+---------------------+----------------------------------------------+------------------------+------------+
| message_description | Message content                              | xs:string              | 1-1        |
+---------------------+----------------------------------------------+------------------------+------------+
| created_at          | Creation date of message                     | xs:dateTime            | 1-1        |
+---------------------+----------------------------------------------+------------------------+------------+
| updated_at          | Last update date                             | xs:dateTime            | 1-1        |
+---------------------+----------------------------------------------+------------------------+------------+
| answer              | Message answer                               | xs:string              | 0-1        |
+---------------------+----------------------------------------------+------------------------+------------+
| answer_at           | Message anwser date                          | xs:dateTime            | 0-1        |
+---------------------+----------------------------------------------+------------------------+------------+
| message_file        | File associated to message                   | :ref:`message_file`    | 0-unbounded|
+---------------------+----------------------------------------------+------------------------+------------+


.. _message_referer:

message_referer
===============

Represent object the message come from : Order uid or Offer uid

* Extends: xs:string

Attributes
----------

+------+-----------------+---------------------+----------+
| Name | Description     | Type                | Required |
+======+=================+=====================+==========+
| type | Type of referer | :ref:`message_type` | required |
+------+-----------------+---------------------+----------+

.. _message_from:

message_from
============

Represent author of the message

* Extends: xs:string

Attributes
----------

+------+----------------+------------------+----------+
| Name | Description    | Type             | Required |
+======+================+==================+==========+
| type | Type of author | :ref:`from_type` | required |
+------+----------------+------------------+----------+

.. _message_file:

message_file
============

The file associated to the message

Elements
--------

+---------------------+----------------------------------------------+------------------------+------------+
| Name                | Description                                  | Type                   | Occurrence |
+=====================+==============================================+========================+============+
| name                | File name                                    |  xs:string             | 0-1        |
+---------------------+----------------------------------------------+------------------------+------------+
| url                 | File url                                     |  xs:string             | 0-1        |
+---------------------+----------------------------------------------+------------------------+------------+
| filetype            | File type                                    |  xs:string             | 0-1        |
+---------------------+----------------------------------------------+------------------------+------------+
