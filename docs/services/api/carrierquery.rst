.. contents::
   :depth: 3
   :backlinks: none

.. _carriers_query_service:

Carriers Query
^^^^^^^^^^^^^^

Informations
============

Description
-----------

This is service is used to get the available carriers managed on Fnac Marketplace platform.

Process
-------

#. A valid authentification token is needed.
#. The partner sends the request to the service.
#. Service returns all available carriers with their name and code to use with the Fnac Markeplace API.

Request
=======

URL to use
----------

* Staging: https://partners-test.mp.fnacdarty.com/api.php/carriers_query
* Production: https://vendeur.fnac.com/api.php/carriers_query

Root element
------------

* Name: carriers_query
* Extends: :ref:`base_request`

Elements
--------

+-------+-------------+---------------------+-------------+-+
| Name  | Description | Type                | Occurrence  | |
+=======+=============+=====================+=============+=+
| query |             | :ref:`query_string` | 0-unbounded | |
+-------+-------------+---------------------+-------------+-+

XML Sample
----------

.. code-block:: xml
   
   <?xml version="1.0" encoding="utf-8"?>
   <carriers_query xmlns="http://www.fnac.com/schemas/mp-dialog.xsd" shop_id="73571111-4AC1-6B59-1AB5-98462FB4B3B1" partner_id="F0A7BCF5-945A-527F-3D1C-3C1D353A49D5" token="469E283A-4CD9-12A2-DDCD-CB319DDD52E7">
     <query><![CDATA[all]]></query>
   </carriers_query>
   

Response
========

Root element
------------

* Name: carriers_query_response
* Extends: :ref:`base_response`

Elements
--------

+---------+-------------+----------------+-------------+-+
| Name    | Description | Type           | Occurrence  | |
+=========+=============+================+=============+=+
| error   | Error list  | :ref:`error`   | 0-unbounded | |
+---------+-------------+----------------+-------------+-+
| carrier |             | :ref:`carrier` | 0-unbounded | |
+---------+-------------+----------------+-------------+-+

XML Sample
----------

.. code-block:: xml
    
   <?xml version="1.0" encoding="utf-8"?>
   <carriers_query_response xmlns="http://www.fnac.com/schemas/mp-dialog.xsd" status="OK">
     <carrier code="CHRONOPOST" global="1">
       <name><![CDATA[Chronopost]]></name>
     </carrier>
     <carrier code="DHL" global="1">
       <name><![CDATA[DHL]]></name>
     </carrier>
     <carrier code="FEDEX" global="1">
       <name><![CDATA[Fedex]]></name>
     </carrier>
     <carrier code="GLS" global="1">
       <name><![CDATA[GLS]]></name>
     </carrier>
     <carrier code="LAPOSTE" global="1">
       <name><![CDATA[La Poste]]></name>
     </carrier>
     <carrier code="MONDIALRELAY" global="1">
       <name><![CDATA[Mondial Relay]]></name>
     </carrier>
     <carrier code="SERNAM" global="1">
       <name><![CDATA[Sernam]]></name>
     </carrier>
     <carrier code="TAT" global="1">
       <name><![CDATA[TAT Express]]></name>
     </carrier>
     <carrier code="TNT" global="1">
       <name><![CDATA[TNT]]></name>
     </carrier>
     <carrier code="UPS" global="1">
       <name><![CDATA[UPS]]></name>
     </carrier>
   </carriers_query_response>
   

.. _carrier:

carrier
=======

Attributes
----------

+--------+----------------------------------------------------------------+-----------+----------+
| Name   | Description                                                    | Type      | Required |
+========+================================================================+===========+==========+
| global | '1' specifies that the carrier code is usable by every seller  | xs:int    | required |
+--------+----------------------------------------------------------------+-----------+----------+
| code   | Carrier short string identifier                                | xs:string | required |
+--------+----------------------------------------------------------------+-----------+----------+

Elements
--------

+------+--------------+-----------+------------+-+
| Name | Description  | Type      | Occurrence | |
+======+==============+===========+============+=+
| name | Carrier name | xs:string | 1-1        | |
+------+--------------+-----------+------------+-+
