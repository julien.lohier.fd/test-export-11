.. contents::
   :depth: 3
   :backlinks: none

.. _orders_update_service:

Orders Update
^^^^^^^^^^^^^

Informations
============

Description
-----------

This is service is used to update the status of your orders (accepting, shipping or updating shipping information).

Process
-------

#. A valid authentification token is needed.
#. The partners calls the service by sending list of orders and the action to make on the related order details (accept, refuse, etc.).
#. The service returns a report of the update processing.

Limitation
----------

You can update up to 25 orders at once.

Request
=======

URL to use
----------

* Staging: https://partners-test.mp.fnacdarty.com/api.php/orders_update
* Production: https://vendeur.fnac.com/api.php/orders_update

Root element
------------

* Name: orders_update
* Extends: :ref:`base_request`


Elements
--------

+-------+------------------+---------------------+-------------+-+
| Name  | Description      | Type                | Occurrence  | |
+=======+==================+=====================+=============+=+
| order | Orders to update | :ref:`order_update` | 1-unbounded | |
+-------+------------------+---------------------+-------------+-+

XML Sample
----------

.. code-block:: xml

   <!-- Update an order, accepting the first item and refusing the second one -->
   <?xml version="1.0" encoding="utf-8"?>
   <orders_update xmlns="http://www.fnac.com/schemas/mp-dialog.xsd" shop_id="BBBFA40E-3A94-2EE1-762A-2858EDE4F9BB" partner_id="C906104B-9B13-611D-6104-261780F88E38" token="AE2F633D-48E4-CEB8-39C7-8713AAE28DE3">
     <order order_id="1P3HCS77E4QYE" action="accept_order">
       <order_detail>
	 <order_detail_id>1</order_detail_id>
         <action><![CDATA[Accepted]]></action>
         <expedition_country_code>FRA</expedition_country_code>
       </order_detail>
       <order_detail>
	 <order_detail_id>2</order_detail_id>
         <action><![CDATA[Refused]]></action>
       </order_detail>
     </order>
   </orders_update>


.. code-block:: xml
   
   <!-- Accept all the items of an order at once -->
   <?xml version="1.0" encoding="utf-8"?>
   <orders_update xmlns="http://www.fnac.com/schemas/mp-dialog.xsd" shop_id="BBBFA40E-3A94-2EE1-762A-2858EDE4F9BB" partner_id="C906104B-9B13-611D-6104-261780F88E38" token="AE2F633D-48E4-CEB8-39C7-8713AAE28DE3">
     <order order_id="1P3HCS98E4QYE" action="accept_all_orders">
       <order_detail>
         <action><![CDATA[Accepted]]></action>
         <expedition_country_code>FRA</expedition_country_code>
       </order_detail>
     </order>
   </orders_update>


.. code-block:: xml

   <!-- Ship an order -->
   <?xml version="1.0" encoding="utf-8"?>
   <orders_update xmlns="http://www.fnac.com/schemas/mp-dialog.xsd" shop_id="BBBFA40E-3A94-2EE1-762A-2858EDE4F9BB" partner_id="C906104B-9B13-611D-6104-261780F88E38" token="AE2F633D-48E4-CEB8-39C7-8713AAE28DE3">
     <order order_id="1P3HCS77E4QYE" action="confirm_to_send">
       <order_detail>
	 <order_detail_id>1</order_detail_id>
         <action><![CDATA[Shipped]]></action>
       </order_detail>
     </order>
   </orders_update>


.. code-block:: xml

   <!-- Ship all the items of an order at once -->
   <?xml version="1.0" encoding="utf-8"?>
   <orders_update xmlns="http://www.fnac.com/schemas/mp-dialog.xsd" shop_id="BBBFA40E-3A94-2EE1-762A-2858EDE4F9BB" partner_id="C906104B-9B13-611D-6104-261780F88E38" token="AE2F633D-48E4-CEB8-39C7-8713AAE28DE3">
     <order order_id="1P3HCS77E4QYE" action="confirm_all_to_send">
       <order_detail>
         <action><![CDATA[Shipped]]></action>
       </order_detail>
     </order>
   </orders_update>


.. code-block:: xml

   <!-- Update an order to enter a tracking number -->
   <?xml version="1.0" encoding="utf-8"?>
   <orders_update xmlns="http://www.fnac.com/schemas/mp-dialog.xsd" shop_id="BBBFA40E-3A94-2EE1-762A-2858EDE4F9BB" partner_id="C906104B-9B13-611D-6104-261780F88E38" token="AE2F633D-48E4-CEB8-39C7-8713AAE28DE3">
     <order order_id="1P3HCS77E4QYE" action="update">
       <order_detail>
         <order_detail_id>1</order_detail_id>
         <action><![CDATA[Updated]]></action>
         <tracking_number><![CDATA[77894512348]]></tracking_number>
         <tracking_company><![CDATA[UPS]]></tracking_company>
       </order_detail>
     </order>
   </orders_update>


Response
========

Root element
------------

* Name: orders_update_response
* Extends: :ref:`base_response`

Elements
--------

+-------+----------------+------------------------------+-------------+-+
| Name  | Description    | Type                         | Occurrence  | |
+=======+================+==============================+=============+=+
| error | Errors         | :ref:`error`                 | 0-unbounded | |
+-------+----------------+------------------------------+-------------+-+
| order | Orders updated | :ref:`order_update_response` | 0-unbounded | |
+-------+----------------+------------------------------+-------------+-+

XML Sample
----------

.. code-block:: xml
   
   <?xml version="1.0" encoding="utf-8"?>
   <orders_update_response xmlns="http://www.fnac.com/schemas/mp-dialog.xsd" status="OK">
     <order>
       <status>OK</status>
       <order_id>1P3HCS98E4QYE</order_id>
       <state>Accepted</state>
       <order_detail>
         <order_detail_id>1</order_detail_id>
         <status>OK</status>
         <state>Accepted</state>
       </order_detail>
     </order>
   </orders_update_response>

.. _order_update:

order_update
============

Attributes
----------

+----------+-------------------------------------------+-----------------------------+----------+---------+
| Name     | Description                               | Type                        | Required | Default |
+==========+===========================================+=============================+==========+=========+
| order_id | Order unique identifier from fnac         | :ref:`order_id_restriction` | required |         |
+----------+-------------------------------------------+-----------------------------+----------+---------+
| action   | Group action type for order detail action | :ref:`order_update_action`  | required |         |
+----------+-------------------------------------------+-----------------------------+----------+---------+


Elements
--------

+--------------+--------------------------+----------------------------+-------------+-+
| Name         | Description              | Type                       | Occurrence  | |
+==============+==========================+============================+=============+=+
| order_detail | Orders details to update | :ref:`order_detail_update` | 1-unbounded | |
+--------------+--------------------------+----------------------------+-------------+-+

.. _order_detail_update:

order_detail_update
===================

Elements
--------

+-------------------------+----------------------------------------------------------------------+----------------------------------+------------+-+
| Name                    | Description                                                          | Type                             | Occurrence | |
+=========================+======================================================================+==================================+============+=+
| order_detail_id         | Order detail unique identifier from fnac                             | xs:positiveInteger               | 0-1        | |
+-------------------------+----------------------------------------------------------------------+----------------------------------+------------+-+
| action                  | Action to do on order detail                                         | :ref:`order_detail_state_update` | 1-1        | |
+-------------------------+----------------------------------------------------------------------+----------------------------------+------------+-+
| tracking_number         | Order detail tracking number                                         | xs:string                        | 0-1        | |
+-------------------------+----------------------------------------------------------------------+----------------------------------+------------+-+
| tracking_company        | Order detail tracking company                                        | :ref:`string100`                 | 0-1        | |
+-------------------------+----------------------------------------------------------------------+----------------------------------+------------+-+
| tracking_client_id      | Order detail tracking client id (needed for shipping method 55 only) | :ref:`string100`                 | 0-1        | |
+-------------------------+----------------------------------------------------------------------+----------------------------------+------------+-+
| expedition_country_code | Order detail Expedition country Alpha3 (refer to CountryQuery code)  | :ref:`string3`                   | 0-1        | |
+-------------------------+----------------------------------------------------------------------+----------------------------------+------------+-+

.. note::

   The expedition country of this product. To get the code of any country, use the :ref:`country_query service <country_query_service>`. If not specified, your default expedition country will be taken into account.

.. note::
   
   The expected tracking_company value is not the "name" of the tracking company, but its "code" that can be read as an identifier. To get the code of any of our compliant carriers, use the :ref:`carriers_query service <carriers_query_service>`.

.. note::

   When shipping the order with action "confirm_to_send", or updating the order with action "update", the tracking_company field is mandatory whenever the tracking_number is filled, but when the tracking_company field is filled, the tracking_number is optional.

.. code-block:: xml

   <!-- This request will fail as the tracking company field is missing -->
   <?xml version="1.0" encoding="utf-8"?>
   <orders_update xmlns="http://www.fnac.com/schemas/mp-dialog.xsd" shop_id="BBBFA40E-3A94-2EE1-762A-2858EDE4F9BB" partner_id="C906104B-9B13-611D-6104-261780F88E38" token="AE2F633D-48E4-CEB8-39C7-8713AAE28DE3">
     <order order_id="1P3HCS77E4QYE" action="confirm_to_send">
       <order_detail>
	 <order_detail_id>1</order_detail_id>
         <action><![CDATA[Shipped]]></action>
         <tracking_number><![CDATA[77894512348]]></tracking_number>
       </order_detail>
     </order>
   </orders_update>

.. _order_update_response:

order_update_response
=====================

Elements
--------

+--------------+-----------------------------------+------------------------------+-------------+-+
| Name         | Description                       | Type                         | Occurrence  | |
+==============+===================================+==============================+=============+=+
| status       | Update status of order            | :ref:`status_code`           | 1-1         | |
+--------------+-----------------------------------+------------------------------+-------------+-+
| order_id     | Order unique identifier from fnac | xs:string                    | 1-1         | |
+--------------+-----------------------------------+------------------------------+-------------+-+
| state        | Order state                       | :ref:`order_state`           | 0-1         | |
+--------------+-----------------------------------+------------------------------+-------------+-+
| error        | Errors                            | :ref:`error`                 | 0-unbounded | |
+--------------+-----------------------------------+------------------------------+-------------+-+
| order_detail | Orders details updated            | :ref:`order_detail_response` | 1-unbounded | |
+--------------+-----------------------------------+------------------------------+-------------+-+

.. _order_detail_response:

order_detail_response
=====================

Elements
--------

+-----------------+------------------------------------------+---------------------------+-------------+-+
| Name            | Description                              | Type                      | Occurrence  | |
+=================+==========================================+===========================+=============+=+
| order_detail_id | Order detail unique identifier from fnac | xs:positiveInteger        | 1-1         | |
+-----------------+------------------------------------------+---------------------------+-------------+-+
| status          | Update status of order detail            | :ref:`status_code`        | 1-1         | |
+-----------------+------------------------------------------+---------------------------+-------------+-+
| state           | Order detail state                       | :ref:`order_detail_state` | 0-1         | |
+-----------------+------------------------------------------+---------------------------+-------------+-+
| error           | Errors                                   | :ref:`error`              | 0-unbounded | |
+-----------------+------------------------------------------+---------------------------+-------------+-+
