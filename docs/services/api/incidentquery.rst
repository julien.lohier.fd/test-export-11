.. contents::
   :depth: 3
   :backlinks: none

Incidents Query
^^^^^^^^^^^^^^^

Informations
============

Description
-----------

This is service is used to retrieve incidents related to your orders.

Process
-------

#. A valid authentification token is needed.
#. The partner calls the service with criteria on incidents to retrieve
#. The service returns a list of incidents accordingly to the submitted criteria

Limitation
----------

Request
=======

URL to use
----------

* Staging: https://partners-test.mp.fnacdarty.com/api.php/incidents_query
* Production: https://vendeur.fnac.com/api.php/incidents_query


Root element
------------

* Name: incidents_query
* Extends: :ref:`base_request`

Attributes
----------

+---------------+-----------------------------------------------+--------------------+----------+---------+
| Name          | Description                                   | Type               | Required | Default |
+===============+===============================================+====================+==========+=========+
| results_count | Maximum limit of incidents retrieved per call | xs:positiveInteger | Optional | 100     |
+---------------+-----------------------------------------------+--------------------+----------+---------+


Elements
--------

+---------------------------+--------------------------------------------------------+-------------------------------------------------+------------+-+
| Name                      | Description                                            | Type                                            | Occurrence | |
+===========================+========================================================+=================================================+============+=+
| paging                    | Allows paging offers following a limit defined by fnac | xs:int                                          | 0-1        | |
+---------------------------+--------------------------------------------------------+-------------------------------------------------+------------+-+
| date                      | Define a range of date                                 | :ref:`incidents_date_constraints_query_request` | 0-1        | |
+---------------------------+--------------------------------------------------------+-------------------------------------------------+------------+-+
| status                    | Status of the incident                                 | :ref:`incident_state`                           | 0-1        | |
+---------------------------+--------------------------------------------------------+-------------------------------------------------+------------+-+
| type                      | Incident order detail opening reason                   | :ref:`incident_open_state`                      | 0-1        | |
+---------------------------+--------------------------------------------------------+-------------------------------------------------+------------+-+
|                           | Define the opening reasons of the incidents to be      | :ref:`incident_open_states`                     | 0-1        | |
| types                     | retrieved.                                             |                                                 |            | |
+---------------------------+--------------------------------------------------------+-------------------------------------------------+------------+-+
| incident_id               | A unique incident id                                   | :ref:`uuid`                                     | 0-1        | |
+---------------------------+--------------------------------------------------------+-------------------------------------------------+------------+-+
| incidents_id              | Can contain multiple incident_id node (up to 50 max)   | :ref:`incident_id`                              | 0-1        | |
+---------------------------+--------------------------------------------------------+-------------------------------------------------+------------+-+
| closed_statuses           | Can contain multiple closed_status node (up to 50 max) | :ref:`incident_close_states`                    | 0-1        | |
+---------------------------+--------------------------------------------------------+-------------------------------------------------+------------+-+
| closed_status             | Incident closing reason                                | :ref:`incident_close_state`                     | 0-1        | |
+---------------------------+--------------------------------------------------------+-------------------------------------------------+------------+-+
| waiting_for_seller_answer | Fnac customer service can ask  the seller to deal      | :ref:`incident_seller_waiting_answer`           | 0-1        | |
|                           | specifically with an incident.                         |                                                 |            | |
+---------------------------+--------------------------------------------------------+-------------------------------------------------+------------+-+
| opened_by                 | Who opened the incident                                | :ref:`incident_by`                              | 0-1        | |
+---------------------------+--------------------------------------------------------+-------------------------------------------------+------------+-+
| closed_by                 | Who closed the incident                                | :ref:`incident_by`                              | 0-1        | |
+---------------------------+--------------------------------------------------------+-------------------------------------------------+------------+-+
| sort_by                   | Define the sort type. By default there are no sort.    | :ref:`sort_constraint_type`                     | 0-1        | |
+---------------------------+--------------------------------------------------------+-------------------------------------------------+------------+-+
| order                     | A unique  order id                                     | :ref:`order_id_restriction`                     | 0-1        | |
+---------------------------+--------------------------------------------------------+-------------------------------------------------+------------+-+
| orders                    | Can contains multiple order node (up to 50 max)        | :ref:`incidents_fnac_id_type`                   | 0-1        | |
+---------------------------+--------------------------------------------------------+-------------------------------------------------+------------+-+

XML Sample
----------

.. code-block:: xml
   
  <?xml version="1.0" encoding="utf-8"?>
  <incidents_query results_count="10" partner_id="00000000-E02B-62CC-2E8C-433E125B16FE" shop_id="00000000-94EB-A769-1CF1-77C4EBC68EB9"   token="00000000-B147-F8DE-9EDF-EB6D28D1D478" xmlns="http://www.fnac.com/schemas/mp-dialog.xsd">
  
	  <paging>10</paging>	
	  <date type="CreatedAt">
      	<min>2011-06-28T16:25:38+02:00</min>
      	<max>2011-06-28T16:25:38+02:00</max>
    </date>  
	  
	  <sort_by>ASC</sort_by>
	  <status>OPENED</status>
    
    <orders>
      <order>0MYA2X266KIKM</order>
      <order>0MYA2X266KIKT</order>
    </orders>  

    <types>
      <type>10</type>
      <type>11</type>
    </types>

    <closed_statuses>
      <closed_status>6</closed_status>
      <closed_status>7</closed_status>
    </closed_statuses>
     
    <incidents_id>
      <incident_id>00000000-94EB-A769-AAAA-77C4EBC68EB9</incident_id>
      <incident_id>00000000-94EB-A769-ZZZZ-77C4EBC68EB9</incident_id>
      <incident_id>00000000-94EB-A769-DDDD-77C4EBC68EB9</incident_id>
    </incidents_id>

    <opened_by>CALLCENTER</opened_by>
    <closed_by>CALLCENTER</closed_by>
    <waiting_for_seller_answer>TRUE</waiting_for_seller_answer>
  </incidents_query>   

.. _incidents_fnac_id_type:

incidents_fnac_id_type
======================

Elements
--------

+-------+--------------------+-----------------------------+------------+
| Name  | Description        | Type                        | Occurrence |
+=======+====================+=============================+============+
| order | A unique  order id | :ref:`order_id_restriction` | 0-1        |
+-------+--------------------+-----------------------------+------------+

.. _incident_open_states:

incident_open_states
====================

Elements
--------

+------+----------------------------------------+----------------------------+------------+-+
| Name | Description                            | Type                       | Occurrence | |
+======+========================================+============================+============+=+
| type | Incident order detail's opening reason | :ref:`incident_open_state` | 0-1        | |
+------+----------------------------------------+----------------------------+------------+-+

.. _incident_id:

incident_id
===========

Elements
--------

+-------------+----------------------+-------------+------------+
| Name        | Description          | Type        | Occurrence |
+=============+======================+=============+============+
| incident_id | A unique incident id | :ref:`uuid` | 0-1        |
+-------------+----------------------+-------------+------------+


.. _incident_close_states:

incident_close_states
=====================

Elements
--------

+-------------+-------------------------+-----------------------------+------------+
| Name        | Description             | Type                        | Occurrence |
+=============+=========================+=============================+============+
| close_state | Incident closing reason | :ref:`incident_close_state` | 0-1        |
+-------------+-------------------------+-----------------------------+------------+

.. _incidents_date_constraints_query_request:

incidents_date_constraints_query_request
========================================

Attributes
----------

+------+---------------------------------------------+---------------------------------+----------+
| Name | Description                                 | Type                            | Required |
+======+=============================================+=================================+==========+
| type | specify date/time constraints when querying | :ref:`incident_date_constraint` | required |
|      | incidents                                   |                                 |          |
+------+---------------------------------------------+---------------------------------+----------+

Elements
--------

+------+------------------------------------------------+-------------+------------+-+
| Name | Description                                    | Type        | Occurrence | |
+======+================================================+=============+============+=+
| min  | minimum date (date are in xml format dateTime) | xs:dateTime | 0-1        | |
+------+------------------------------------------------+-------------+------------+-+
| max  | maximum date (date are in xml format dateTime) | xs:dateTime | 0-1        | |
+------+------------------------------------------------+-------------+------------+-+

Response
========

Root element
------------

* Name: incidents_query_response
* Extends: :ref:`base_response`

Elements
--------

+-------------------+----------------------------------------------+--------------------------------+-------------+
| Name              | Description                                  | Type                           | Occurrence  |
+===================+==============================================+================================+=============+
| error             | Errors when querying incidents (if existing) | :ref:`error`                   | 0-unbounded |
+-------------------+----------------------------------------------+--------------------------------+-------------+
| page              | Number of the requested page                 | xs:int                         | 0-1         |
+-------------------+----------------------------------------------+--------------------------------+-------------+
| total_paging      | Number of pages available                    | xs:int                         | 0-1         |
+-------------------+----------------------------------------------+--------------------------------+-------------+
| nb_total_per_page | Total number of orders requested             | xs:int                         | 0-1         |
+-------------------+----------------------------------------------+--------------------------------+-------------+
| nb_total_result   | Total items per page (the same value as      | xs:int                         | 0-1         |
|                   | results_count given in the request)          |                                |             |
+-------------------+----------------------------------------------+--------------------------------+-------------+
| incident          | Incident information                         | :ref:`incident_query_response` | 0-unbounded |
+-------------------+----------------------------------------------+--------------------------------+-------------+

XML Sample
----------

.. code-block:: xml

   <?xml version="1.0" encoding="utf-8"?>
   <incidents_query_response xmlns="http://www.fnac.com/schemas/mp-dialog.xsd" status="OK">
     <page>0</page>
     <total_paging>0</total_paging>
     <nb_total_per_page>0</nb_total_per_page>
     <nb_total_result>1</nb_total_result>
     <incident id="644D00B4-3D61-ECFD-0D62-589AE7B39CEA">
       <header>
         <status>OPEN</status>
         <waiting_for_seller_answer>FALSE</waiting_for_seller_answer>
         <message>
                  Une réclamation a été ouverte sur cette commande concernant le(s) article(s) suivant(s) :
                  - Le Trésor de Rackham le Rouge, Tintin - cartonné - Bande dessinée [Livre] - Article non reçu
                  JE SUIS PAS CONTENT.
         </message>
         <opened_by>client</opened_by>
         <created_at>2010-06-02T10:57:51+02:00</created_at>
         <updated_at>2011-03-24T15:27:39+01:00</updated_at>
         <order_id>1A4JMPE5PG02W</order_id>
       </header>
       <order_details_incident>
         <order_detail_incident>
           <order_detail_id>2</order_detail_id>
           <type>1</type>
           <status>1</status>
           <created_at>2010-06-02T10:57:51+02:00</created_at>
           <updated_at>2010-06-02T10:57:51+02:00</updated_at>
         </order_detail_incident>
       </order_details_incident>
     </incident>
   </incidents_query_response>

.. _incident_query_response:

incident_query_response
==============================

Elements
--------

+------------------------+----------------------------------------------+----------------------------------------+------------+
| Name                   | Description                                  | Type                                   | Occurrence |
+========================+==============================================+========================================+============+
| header                 | Incident Information                         | :ref:`incident_header_response`        | 1-1        |
+------------------------+----------------------------------------------+----------------------------------------+------------+
| order_details_incident | Incident on related order detail information | :ref:`incident_order_details_response` | 1-1        |
+------------------------+----------------------------------------------+----------------------------------------+------------+


.. _incident_header_response:

incident_header_response
========================

Elements
--------

+---------------------------+----------------------------------------------------+---------------------------------------+------------+
| Name                      | Description                                        | Type                                  | Occurrence |
+===========================+====================================================+=======================================+============+
| status                    | Status of the incident                             | :ref:`incident_state`                 | 1-1        |
+---------------------------+----------------------------------------------------+---------------------------------------+------------+
| waiting_for_seller_answer | Has incident waiting for an answer from the seller | :ref:`incident_seller_waiting_answer` | 1-1        |
+---------------------------+----------------------------------------------------+---------------------------------------+------------+
| message                   | Seller message                                     | xs:string                             | 1-1        |
+---------------------------+----------------------------------------------------+---------------------------------------+------------+
| opened_by                 | Who opened the incident                            | :ref:`incident_by`                    | 1-1        |
+---------------------------+----------------------------------------------------+---------------------------------------+------------+
| closed_by                 | Who closed the incident                            | :ref:`incident_by`                    | 0-1        |
+---------------------------+----------------------------------------------------+---------------------------------------+------------+
| closed_status             | Reason given at the incident’s closing             | :ref:`incident_close_state`           | 0-1        |
+---------------------------+----------------------------------------------------+---------------------------------------+------------+
| created_at                | Creation date of the incident                      | xs:dateTime                           | 1-1        |
+---------------------------+----------------------------------------------------+---------------------------------------+------------+
| updated_at                | Modification date of the incident                  | xs:dateTime                           | 1-1        |
+---------------------------+----------------------------------------------------+---------------------------------------+------------+
| closed_at                 | Closed date of the incident                        | xs:dateTime                           | 0-1        |
+---------------------------+----------------------------------------------------+---------------------------------------+------------+
| order_id                  | Order attached to the incident                     | xs:string                             | 1-1        |
+---------------------------+----------------------------------------------------+---------------------------------------+------------+

.. _incident_order_details_response:

incident_order_details_response
===============================

Elements
--------

+------------------------+--------------------------+---------------------------------------+------------+
| Name                   | Description              | Type                                  | Occurrence |
+========================+==========================+=======================================+============+
| order_detail_incident  | Order Detail information | :ref:`incident_order_detail_incident` | 1-1        |
+------------------------+--------------------------+---------------------------------------+------------+


.. _incident_order_detail_incident:

incident_order_detail_incident
==============================

Elements
--------

+-----------------+------------------------------------------------+----------------------------+------------+
| Name            | Description                                    | Type                       | Occurrence |
+=================+================================================+============================+============+
| order_detail_id | Order detail involved in the incident          | xs:integer                 | 1-1        |
+-----------------+------------------------------------------------+----------------------------+------------+
| type            | Type of the order detail incident              | :ref:`incident_open_state` | 1-1        |
+-----------------+------------------------------------------------+----------------------------+------------+
| status          | Status of the order detail incident            | :ref:`incident_state`      | 1-1        |
+-----------------+------------------------------------------------+----------------------------+------------+
| created_at      | Creation date of the order detail incident     | xs:dateTime                | 1-1        |
+-----------------+------------------------------------------------+----------------------------+------------+
| updated_at      | Modification date of the order detail incident | xs:dateTime                | 1-1        |
+-----------------+------------------------------------------------+----------------------------+------------+
| refund          | Can contain refund informations                | :ref:`incident_refund`     | 0-1        |
+-----------------+------------------------------------------------+----------------------------+------------+

.. _incident_refund:

incident_refund
===============

Elements
--------

+-----------------+--------------------------------+-------------+------------+
| Name            | Description                    | Type        | Occurrence |
+=================+================================+=============+============+
| product_amount  | Product amount refunded        | xs:decimal  | 1-1        |
+-----------------+--------------------------------+-------------+------------+
| shipping_amount | Shipping amount refunded       | xs:decimal  | 1-1        |
+-----------------+--------------------------------+-------------+------------+
| fee_ht_amount   | Fee amount refunded (tax free) | xs:decimal  | 1-1        |
+-----------------+--------------------------------+-------------+------------+
| created_at      | Creation date of the refund    | xs:dateTime | 1-1        |
+-----------------+--------------------------------+-------------+------------+
