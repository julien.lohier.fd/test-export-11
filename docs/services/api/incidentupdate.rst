.. contents::
   :depth: 3
   :backlinks: none

Incidents Update
^^^^^^^^^^^^^^^^

Informations
============

Description
-----------

This is service is used to handle incidents created on orders.


Process
-------

#. A valid authentification token is needed.
#. The partner sends a list of incidents to update.
#. The service returns the processing results.


Limitation
----------

Refund is the only available action for the moment.

Request
=======

URL to use
----------

* Staging: https://partners-test.mp.fnacdarty.com/api.php/incidents_update
* Production: https://vendeur.fnac.com/api.php/incidents_update

Root element
------------

* Name: incidents_update
* Type: incidents_update_request
* Extends: :ref:`base_request`

Elements
--------

+-------+----------------------------------+---------------------------------------+-------------+-+
| Name  | Description                      | Type                                  | Occurrences | |
+=======+==================================+=======================================+=============+=+
| order | Seller Order in fnac marketplace | :ref:`incidents_order_update_request` | 1-unbounded | |
+-------+----------------------------------+---------------------------------------+-------------+-+

.. _incidents_order_update_request:

incidents_order_update_request
==============================

Attributes
----------

+----------+-------------------------------------+-------------------------------------+----------+---------+
| Name     | Description                         | Type                                | Required | Default |
+==========+=====================================+=====================================+==========+=========+
| order_id | Unique Fnac identifier for an order | :ref:`order_id_restriction`         | required |         |
+----------+-------------------------------------+-------------------------------------+----------+---------+
| action   | Action to process                   | :ref:`incident_order_update_action` | required |         |
+----------+-------------------------------------+-------------------------------------+----------+---------+

Elements
--------

+--------------+--------------------------------------+----------------------------------------------+-------------+-+
| Name         | Description                          | Type                                         | Occurrence  | |
+==============+======================================+==============================================+=============+=+
| order_detail | Order detail involved in this action | :ref:`incidents_order_detail_update_request` | 1-unbounded | |
+--------------+--------------------------------------+----------------------------------------------+-------------+-+

.. _incidents_order_detail_update_request:

incidents_order_detail_update_request
=====================================

Elements
--------

+-----------------+-------------------------------+----------------------+------------+-+
| Name            | Description                   | Type                 | Occurrence | |
+=================+===============================+======================+============+=+
| order_detail_id | Order attached to this action | xs:positiveinteger   | 0-1        | |
+-----------------+-------------------------------+----------------------+------------+-+
| refund_reason   | Incident refund reason        | :ref:`refund_reason` | 0-1        | |
+-----------------+-------------------------------+----------------------+------------+-+

XML Sample
----------

.. literalinclude:: ../examples/incidents_update_request.xml
   :language: xml

Response
========

Root element
------------

* Name: incidents_update_response
* Type: incidents_update_response
* Extends: :ref:`base_response`

Elements
--------

+-------+--------------------------------------+----------------------------------+-------------+-+
| Name  | Description                          | Type                             | Occurrences | |
+=======+======================================+==================================+=============+=+
| error | Errors for the action (if exists)    | :ref:`error`                     | 0-unbounded | |
+-------+--------------------------------------+----------------------------------+-------------+-+
| order | Order Detail involved in this action | :ref:`incidents_update_response` | 0-unbounded | |
+-------+--------------------------------------+----------------------------------+-------------+-+

.. _incidents_update_response:

incidents_update_response
=========================

Elements
--------

+-----------------+--------------------------------------+----------------------------------------------+-------------+-+
| Name            | Description                          | Type                                         | Occurrence  | |
+=================+======================================+==============================================+=============+=+
| status          | Status of the action                 | :ref:`status_code`                           | 1-1         | |
+-----------------+--------------------------------------+----------------------------------------------+-------------+-+
| incident_id     | Incident id                          | :ref:`uuid`                                  | 0-1         | |
+-----------------+--------------------------------------+----------------------------------------------+-------------+-+
| incident_status | Incident status                      | :ref:`incident_state`                        | 0-1         | |
+-----------------+--------------------------------------+----------------------------------------------+-------------+-+
| order_id        | Order attached to this incident      | xs:string                                    | 1-1         | |
+-----------------+--------------------------------------+----------------------------------------------+-------------+-+
| state           | Order State                          | :ref:`order_state`                           | 0-1         | |
+-----------------+--------------------------------------+----------------------------------------------+-------------+-+
| error           | Errors for the action (if exists)    | :ref:`error`                                 | 0-unbounded | |
+-----------------+--------------------------------------+----------------------------------------------+-------------+-+
| order_detail    | Order Detail involved in this action | :ref:`incident_order_detail_update_response` | 0-unbounded | |
+-----------------+--------------------------------------+----------------------------------------------+-------------+-+

.. _incident_order_detail_update_response:

incident_order_detail_update_response
=====================================

Elements
--------

+-----------------+----------------------------------------+---------------------------+-------------+-+
| Name            | Description                            | Type                      | Occurrence  | |
+=================+========================================+===========================+=============+=+
| order_detail_id | Order Detail attached to this incident | xs:positiveInteger        | 1-1         | |
+-----------------+----------------------------------------+---------------------------+-------------+-+
| status          | Status of the Order Detail             | :ref:`status_code`        | 1-1         | |
+-----------------+----------------------------------------+---------------------------+-------------+-+
| state           | Order Detail state                     | :ref:`order_detail_state` | 0-1         | |
+-----------------+----------------------------------------+---------------------------+-------------+-+
| error           | Errors for the action (if exists)      | :ref:`error`              | 0-unbounded | |
+-----------------+----------------------------------------+---------------------------+-------------+-+

XML Sample
----------

.. literalinclude:: ../examples/incidents_update_response.xml
   :language: xml
