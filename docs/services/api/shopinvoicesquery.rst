.. contents::
   :depth: 3
   :backlinks: none

.. _shop_invoices_query_service:

Shop Invoices Query
^^^^^^^^^^^^^^^^^^^

Informations
============

Description
-----------

This service is used to retrieve download links to the shop's invoices

Process
-------

#. A valid authentification token is needed.
#. The partner sends the request to the service. Filters (as creation date) can be used.
#. The service returns the list of the existing invoices ids and download links

Request
=======

URL to use
----------

* Staging: https://partners-test.mp.fnacdarty.com/api.php/shop_invoices_query
* Production: https://vendeur.fnac.com/api.php/shop_invoices_query

Root element
------------

* Name: shop_invoices_query
* Extends: :ref:`base_request`

Attributes
----------

+---------------+--------------------------------+--------------------+----------+---------+
| Name          | Description                    | Type               | Required | Default |
+===============+================================+====================+==========+=========+
| results_count | Max number of results per page | xs:positiveInteger | optional | 100     |
+---------------+--------------------------------+--------------------+----------+---------+


Elements
--------

+-----------------+------------------------------------------+-----------------------------------+------------+
| Name            | Description                              | Type                              | Occurrence |
+=================+==========================================+===================================+============+
| paging          | Page number to fetch                     | xs:int                            | 0-1        |
+-----------------+------------------------------------------+-----------------------------------+------------+
| date            | Invoice creation date filter             | :ref:`order_date_constraint`      | 0-1        |
+-----------------+------------------------------------------+-----------------------------------+------------+



XML Sample
----------

.. code-block:: xml
   
   <?xml version="1.0" encoding="utf-8"?>
   <shop_invoices_query xmlns="http://www.fnac.com/schemas/mp-dialog.xsd" shop_id="D8622A57-04D4-3C1C-6555-B5F633E808BC" partner_id="AD2B0514-0DFB-0723-3D41-F8171E8F5F20" token="4DB98B1A-6E78-9BF6-9FA7-9194BBA13D35" results_count="10">
     <paging>1</paging>
     <date>
       <min>2011-01-01T12:13:00</min>
       <max>2014-12-01T12:13:00</max>
     </date>
   </shop_invoices_query>

Response
========

Root element
------------

* Name: shop_invoices_query_response
* Extends: :ref:`base_response`

Elements
--------

+-------------------+----------------------------+---------------------+-------------+-+
| Name              | Description                | Type                | Occurrence  | |
+===================+============================+=====================+=============+=+
| error             | Errors                     | :ref:`error`        | 0-unbounded | |
+-------------------+----------------------------+---------------------+-------------+-+
| page              | Page number                | xs:int              | 0-1         | |
+-------------------+----------------------------+---------------------+-------------+-+
| total_paging      | Number of pages available  | xs:int              | 0-1         | |
+-------------------+----------------------------+---------------------+-------------+-+
| nb_total_per_page | Number of results per page | xs:int              | 0-1         | |
+-------------------+----------------------------+---------------------+-------------+-+
| nb_total_result   | Number of results          | xs:int              | 0-1         | |
+-------------------+----------------------------+---------------------+-------------+-+
| shop_invoice      | Shop invoices              | :ref:`shop_invoice` | 0-unbounded | |
+-------------------+----------------------------+---------------------+-------------+-+

XML Sample
----------

.. code-block:: xml
   
   <?xml version="1.0" encoding="utf-8"?>
   <shop_invoices_query_response status="OK" xmlns="http://www.fnac.com/schemas/mp-dialog.xsd">
     <page>1</page>
     <total_paging>1</total_paging>
     <nb_total_per_page>1</nb_total_per_page>
     <nb_total_result>3</nb_total_result>
     <shop_invoice>
       <invoice_id>48D7A183-6071-A944-6545-5FA8CD7696CC</invoice_id>
       <url><![CDATA[https://vendeur.fnac.com/api.php/shop-invoice/AD2B0514-0DFB-0723-3D41-F8171E8F5F20/D8622A57-04D4-3C1C-6555-B5F633E808BC/DE03A4CD-7E97-E259-637A-44074FADB059/48D7A183-6071-A944-6545-5FA8CD7696CC]]></url>
       <created_at>2014-06-13T15:05:52+02:00</created_at>
     </shop_invoice>
     <shop_invoice>
       <invoice_id>24FAF4FB-BF18-83F1-C064-17FAF9BDA47D</invoice_id>
       <url><![CDATA[https://vendeur.fnac.com/api.php/shop-invoice/AD2B0514-0DFB-0723-3D41-F8171E8F5F20/D8622A57-04D4-3C1C-6555-B5F633E808BC/DE03A4CD-7E97-E259-637A-44074FADB059/24FAF4FB-BF18-83F1-C064-17FAF9BDA47D]]></url>
       <created_at>2014-05-13T15:05:50+02:00</created_at>
     </shop_invoice>
     <shop_invoice>
       <invoice_id>E3D55903-12E9-E506-FB78-EF47E1AF0723</invoice_id>
       <url><![CDATA[https://vendeur.fnac.com/api.php/shop-invoice/AD2B0514-0DFB-0723-3D41-F8171E8F5F20/D8622A57-04D4-3C1C-6555-B5F633E808BC/DE03A4CD-7E97-E259-637A-44074FADB059/E3D55903-12E9-E506-FB78-EF47E1AF0723]]></url>
       <created_at>2013-11-15T14:23:02+01:00</created_at>
     </shop_invoice>
   </shop_invoices_query_response>


.. _shop_invoice:

shop_invoice
============

Elements
--------

+--------------------------------------------------+-------------------------------------+-------------+------------+
| Name                                             | Description                         | Type        | Occurrence |
+==================================================+=====================================+=============+============+
| invoice_id                                       | Invoice unique identifier from fnac | :ref:`uuid` | 1-1        |
+--------------------------------------------------+-------------------------------------+-------------+------------+
| url                                              | Invoice PDF download link           | xs:string   | 1-1        |
+--------------------------------------------------+-------------------------------------+-------------+------------+
| invoice_related_orders_url                       | Invoice related orders csv link     | xs:string   | 0-1        |
+--------------------------------------------------+-------------------------------------+-------------+------------+
| created_at                                       | Creation date of the invoice        | xs:dateTime | 1-1        |
+--------------------------------------------------+-------------------------------------+-------------+------------+
| transfer_amount                                  | Amount transfered                   | xs:decimal  | 1-1        |
+--------------------------------------------------+-------------------------------------+-------------+------------+
| tva                                              | tax rate                            | xs:decimal  | 1-1        |
+--------------------------------------------------+-------------------------------------+-------------+------------+
| order_amount                                     | Order amount                        | xs:decimal  | 1-1        |
+--------------------------------------------------+-------------------------------------+-------------+------------+
| shipping_amount                                  | Shipping amount                     | xs:decimal  | 1-1        |
+--------------------------------------------------+-------------------------------------+-------------+------------+
| fee_tax_inc_amount                               | Fee amount with tax                 | xs:decimal  | 1-1        |
+--------------------------------------------------+-------------------------------------+-------------+------------+
| subscription_tax_inc_amount                      | Subscription amount with tax        | xs:decimal  | 1-1        |
+--------------------------------------------------+-------------------------------------+-------------+------------+
| refund_fee_tax_inc_amount_payable                | Refund fee amount payable with tax  | xs:decimal  | 1-1        |
+--------------------------------------------------+-------------------------------------+-------------+------------+
| refund_fee_tax_inc_amount_paid                   | Refund fee amount paid with tax     | xs:decimal  | 1-1        |
+--------------------------------------------------+-------------------------------------+-------------+------------+
| refund_subscription_period_tax_inc_amount_payable| Refund subscription payable with tax| xs:decimal  | 1-1        |
+--------------------------------------------------+-------------------------------------+-------------+------------+
| refund_subscription_period_tax_inc_amount_paid   | Refund subscription paid with tax   | xs:decimal  | 1-1        |
+--------------------------------------------------+-------------------------------------+-------------+------------+
