.. contents::
   :depth: 3
   :backlinks: none

.. _batch_query_service:

Batch Query
^^^^^^^^^^^

Informations
============

Description
-----------

This service is used to get information about your currently processing import batches.

Process
-------

#. A valid authentification token is needed.
#. The partner sends the request to the service. No parameter is needed.
#. The service returns the list of the batches that are currently enqueued (active) or running.

Request
=======

URL to use
----------

* Staging: https://partners-test.mp.fnacdarty.com/api.php/batch_query
* Production: https://vendeur.fnac.com/api.php/batch_query

Root element
------------

* Name: batch_query
* Extends: :ref:`base_request`

XML Sample
----------

.. code-block:: xml
   
   <?xml version="1.0" encoding="utf-8"?>
   <batch_query xmlns="http://www.fnac.com/schemas/mp-dialog.xsd" shop_id="9B6AF071-4AC1-6B59-1AB5-98462FB4B3B1" partner_id="C3027A77-945A-527F-3D1C-3C1D353A49D5" token="295A06D8-E7AB-4329-EE70-0A84A22CCA57"/>


Response
========

Root element
------------

* Name: batch_query_response
* Extends: :ref:`base_response`

Elements
--------

+------------------+-------------------------------------------+--------------+-------------+-+
| Name             | Description                               | Type         | Occurrence  | |
+==================+===========================================+==============+=============+=+
| error            | List of error                             | :ref:`error` | 0-unbounded | |
+------------------+-------------------------------------------+--------------+-------------+-+
| batch            | List of batches                           | :ref:`batch` | 0-unbounded | |
+------------------+-------------------------------------------+--------------+-------------+-+
| nb_batch_running | Number of batches being processed         | xs:int       | 1-1         | |
+------------------+-------------------------------------------+--------------+-------------+-+
| nb_batch_active  | Number of batches waiting to be processed | xs:int       | 1-1         | |
+------------------+-------------------------------------------+--------------+-------------+-+

XML Sample
----------

.. code-block:: xml

   <?xml version="1.0" encoding="utf-8"?>
   <batch_query_response xmlns="http://www.fnac.com/schemas/mp-dialog.xsd" status="OK">
     <batch status="ACTIVE">
       <batch_id>42BFB911-8000-2F2E-7B13-BD20304365CF</batch_id>
       <nb_lines>3</nb_lines>
       <created_at>2011-06-15T14:50:30+02:00</created_at>
     </batch>
     <nb_batch_running>0</nb_batch_running>
     <nb_batch_active>1</nb_batch_active>
   </batch_query_response>


.. _batch:

batch
=====

Attributes
----------

+--------+--------------+--------------------+----------+
| Name   | Description  | Type               | Required |
+========+==============+====================+==========+
| status | Batch status | :ref:`status_code` | required |
+--------+--------------+--------------------+----------+

Elements
--------

+------------+-----------------------------------+-----------+------------+
| Name       | Description                       | Type      | Occurrence |
+============+===================================+===========+============+
| batch_id   | Batch unique identifier from fnac | xs:string | 0-1        |
+------------+-----------------------------------+-----------+------------+
| nb_lines   | Number of lines to process        | xs:int    | 0-1        |
+------------+-----------------------------------+-----------+------------+
| created_at | Creation date of the batch        | xs:string | 1-1        |
+------------+-----------------------------------+-----------+------------+
