:orphan:

Fnac Special fields
^^^^^^^^^^^^^^^^^^^

Informations
============

Description
-----------

This page describe the fields used only by fnac shop 

Service OfferQuery
==================

Root element
------------

* Name: offer_query_response

Elements
--------

+----------------------------+------------------------------------------------+------------------------+-------------+
| Name                       | Description                                    | Type                   | Occurrences |
+============================+================================================+========================+=============+
| promotion_code_id          | Fnac promotion code                            | xs:integer             | 0-1         |
+----------------------------+------------------------------------------------+------------------------+-------------+
| promotion_code_adherent_id | Fnac promotion code for fnac member (adhérent) | xs:integer             | 0-1         |
+----------------------------+------------------------------------------------+------------------------+-------------+
| adherent_price             | Fnac shop unique id                            | :ref:`price_090_20000` | 0-1         |
+----------------------------+------------------------------------------------+------------------------+-------------+

XML Sample
----------

.. code-block:: xml
   


Service OfferUpdate
===================

Root element
------------

* Name: offer_update_request

Elements
--------

+-------------------+---------------------+------------------------+-------------+-+
| Name              | Description         | Type                   | Occurrences | |
+===================+=====================+========================+=============+=+
| promotion_code_id | Fnac promotion code | xs:integer             | 0-1         | |
+-------------------+---------------------+------------------------+-------------+-+
| adherent_price    | Fnac shop unique id | :ref:`price_090_20000` | 0-1         | |
+-------------------+---------------------+------------------------+-------------+-+

XML Sample
----------

.. code-block:: xml
   

