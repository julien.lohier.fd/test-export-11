.. contents::
   :depth: 3
   :backlinks: none

.. _sales_periods_query_service:

Sales Periods Query
^^^^^^^^^^^^^^^^^^^

Informations
============

Description
-----------

This service is used to retrieve the reference (or its dates) of the available sales period managed by our teams to match legal dates.

Process
-------

#. A valid authentification token is needed.
#. The partner sends the request to the service. You can add parameters to retrieve only what you're looking for.
#. The service returns the list of the available sales periods that you can use to create a promotion of type Sales.

Request
=======

URL to use
----------

* Staging: https://partners-test.mp.fnacdarty.com/api.php/sales_periods_query
* Production: https://vendeur.fnac.com/api.php/sales_periods_query

Root element
------------

* Name: sales_periods_query
* Extends: :ref:`base_request`

Attributes
----------

+---------------+--------------------------------+--------------------+----------+---------+
| Name          | Description                    | Type               | Required | Default |
+===============+================================+====================+==========+=========+
| results_count | Max number of results per page | xs:positiveInteger | optional | 100     |
+---------------+--------------------------------+--------------------+----------+---------+

Elements
--------

+-----------+--------------------------------------------+--------------------------------------+------------+-+
| Name      | Description                                | Type                                 | Occurrence | |
+===========+============================================+======================================+============+=+
| paging    | Page number to fetch                       | xs:int                               | 0-1        | |
+-----------+--------------------------------------------+--------------------------------------+------------+-+
| date      | Date filter                                | :ref:`sales_period_date_constraint`  | 0-2        | |
+-----------+--------------------------------------------+--------------------------------------+------------+-+
| reference | Sales period's reference                   | xs:string                            | 0-1        | |
+-----------+--------------------------------------------+--------------------------------------+------------+-+

XML Sample
----------

.. code-block:: xml
   
    <sales_periods_query partner_id="PartnerId" shop_id="ShopId" token="Token" xmlns="http://www.fnac.com/schemas/mp-dialog.xsd" results_count="5">
        <paging>1</paging>
        <date type="StartAt">
            <min>2017-01-01T00:00:00+02:00</min>
            <max>2017-12-30T23:59:59+02:00</max>
        </date>
    </sales_periods_query>


Response
========

Root element
------------

* Name: sales_periods_query_response
* Extends: :ref:`base_response`

Elements
--------

+----------------------+---------------------------+-----------------------------+-------------+-+
| Name                 | Description               | Type                        | Occurrence  | |
+======================+===========================+=============================+=============+=+
| error                | Error                     | :ref:`error`                | 0-unbounded | |
+----------------------+---------------------------+-----------------------------+-------------+-+
| page                 | Page number               | xs:int                      | 0-1         | |
+----------------------+---------------------------+-----------------------------+-------------+-+
| total_paging         | Number of pages available | xs:int                      | 0-1         | |
+----------------------+---------------------------+-----------------------------+-------------+-+
| nb_total_per_page    | result_count value        | xs:int                      | 0-1         | |
+----------------------+---------------------------+-----------------------------+-------------+-+
| nb_total_result      | Number of results         | xs:int                      | 0-1         | |
+----------------------+---------------------------+-----------------------------+-------------+-+
| sales_periods        | Sales Periods             | :ref:`sales_period`         | 0-unbounded | |
+----------------------+---------------------------+-----------------------------+-------------+-+

XML Sample
----------

.. code-block:: xml
   
   <sales_periods_query_response xmlns="http://www.fnac.com/schemas/mp-dialog.xsd" status="OK">
        <page>1</page>
        <total_paging>1</total_paging>
        <nb_total_per_page>5</nb_total_per_page>
        <nb_total_result>1</nb_total_result>
        <sales_periods>
            <sales_period>
                <label culture="fr_FR">
                    <![CDATA[hiver 2017]]>
                </label>
                <label culture="en_US">
                    <![CDATA[winter 2017]]>
                </label>
                <reference>
                    <![CDATA[SLD_HIVER_2017]]>
                </reference>
                <starts_at>2017-01-11T00:00:00+01:00</starts_at>
                <ends_at>2017-02-14T23:59:00+01:00</ends_at>
            </sales_period>
        </sales_periods>
    </sales_periods_query_response>

.. _sales_period:

sales_period
============

Elements
--------

+------------+-------------------------------------------------+-------------------+-------------+
| Name       | Description                                     | Type              | Occurrence  |
+============+=================================================+===================+=============+
| label      | Translated label                                | :ref:`label_i18n` | 1-unbounded |
+------------+-------------------------------------------------+-------------------+-------------+
| reference  | Reference to use when creating Sales promotion  | xs:string         | 1-1         |
+------------+-------------------------------------------------+-------------------+-------------+
| starts_at  | Start date                                      | xs:dateTime       | 1-1         |
+------------+-------------------------------------------------+-------------------+-------------+
| ends_at    | End date                                        | xs:dateTime       | 1-1         |
+------------+-------------------------------------------------+-------------------+-------------+

.. _label_i18n:

label_i18n
==========

Restriction
-----------

* Extends: xs:string

Attributes
----------

+------------+------------------------------+-------------------+-------------+
| Name       | Description                  | Type              | Occurrence  |
+============+==============================+===================+=============+
| culture    | Culture of the label         | :ref:`string5`    | 1-1         |
+------------+------------------------------+-------------------+-------------+
