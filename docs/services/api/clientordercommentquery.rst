.. contents::
   :depth: 3
   :backlinks: none

Client Order Comments Query
^^^^^^^^^^^^^^^^^^^^^^^^^^^

Informations
============

Description
-----------

This service is used to retrieve customers comments and ratings about your orders.

Process
-------

#. A valid authentification token is needed.
#. The partner calls the service with criteria on comments to retrieve.
#. The service returns all information about client comments.

Request
=======

URL to use
----------

* Staging: https://partners-test.mp.fnacdarty.com/api.php/client_order_comments_query
* Production: https://vendeur.fnac.com/api.php/client_order_comments_query

Root element
------------

* Name: client_order_comments_query
* Extends: :ref:`base_request`

Attributes
----------

+---------------+--------------------------------+--------------------+----------+---------+
| Name          | Description                    | Type               | Required | Default |
+===============+================================+====================+==========+=========+
| results_count | Max number of results per page | xs:positiveInteger | optional | 100     |
+---------------+--------------------------------+--------------------+----------+---------+


Elements
--------

+-------------------------+--------------------------------------------+---------------------------------------------+------------+-+
| Name                    | Description                                | Type                                        | Occurrence | |
+=========================+============================================+=============================================+============+=+
| paging                  | Page number to fetch                       | xs:int                                      | 0-1        | |
+-------------------------+--------------------------------------------+---------------------------------------------+------------+-+
| date                    | Date filter                                | :ref:`client_order_comment_date_constraint` | 0-1        | |
+-------------------------+--------------------------------------------+---------------------------------------------+------------+-+
| rate                    | Order rate filter                          | :ref:`client_order_comment_rate_constraint` | 0-1        | |
+-------------------------+--------------------------------------------+---------------------------------------------+------------+-+
| client_order_comment_id | Comment unique identifier filter from fnac | :ref:`uuid`                                 | 0-1        | |
+-------------------------+--------------------------------------------+---------------------------------------------+------------+-+
| order_fnac_id           | Order unique identifier filter from fnac   | xs:string                                   | 0-1        | |
+-------------------------+--------------------------------------------+---------------------------------------------+------------+-+

XML Sample
----------

.. code-block:: xml
   
   <?xml version="1.0" encoding="utf-8"?>
   <client_order_comments_query xmlns="http://www.fnac.com/schemas/mp-dialog.xsd" shop_id="73571111-4AC1-6B59-1AB5-98462FB4B3B1" partner_id="F0A7BCF5-945A-527F-3D1C-3C1D353A49D5" token="469E283A-61F4-87D8-241D-4C92DB90E1BC" results_count="2">
     <paging>1</paging>
   </client_order_comments_query>


Response
========

Root element
------------

* Name: client_order_comments_query_response
* Extends: :ref:`base_response`


Elements
--------

+----------------------+---------------------------+-----------------------------+-------------+-+
| Name                 | Description               | Type                        | Occurrence  | |
+======================+===========================+=============================+=============+=+
| error                | Error                     | :ref:`error`                | 0-unbounded | |
+----------------------+---------------------------+-----------------------------+-------------+-+
| page                 | Page number               | xs:int                      | 0-1         | |
+----------------------+---------------------------+-----------------------------+-------------+-+
| total_paging         | Number of pages available | xs:int                      | 0-1         | |
+----------------------+---------------------------+-----------------------------+-------------+-+
| nb_total_result      | Number of results         | xs:int                      | 0-1         | |
+----------------------+---------------------------+-----------------------------+-------------+-+
| client_order_comment | Client order comments     | :ref:`client_order_comment` | 0-unbounded | |
+----------------------+---------------------------+-----------------------------+-------------+-+

XML Sample
----------

.. code-block:: xml
   
   <?xml version="1.0" encoding="utf-8"?>
   <client_order_comments_query_response xmlns="http://www.fnac.com/schemas/mp-dialog.xsd" status="OK">
     <page>1</page>
     <total_paging>3</total_paging>
     <nb_total_per_page>2</nb_total_per_page>
     <nb_total_result>6</nb_total_result>
     <client_order_comment>
       <rate>5</rate>
       <order_fnac_id>1WZWM7QSHLVFI</order_fnac_id>
       <client_order_comment_id/>
       <client_name> .</client_name>
       <comment><![CDATA[]]></comment>
       <reply><![CDATA[]]></reply>
       <is_received>1</is_received>
       <is_fast>1</is_fast>
       <is_well_packed>1</is_well_packed>
       <is_good_shape>1</is_good_shape>
       <created_at>2011-01-19T14:35:59+01:00</created_at>
     </client_order_comment>
     <client_order_comment>
       <rate>5</rate>
       <order_fnac_id>00P1H6LE3431A</order_fnac_id>
       <client_order_comment_id/>
       <client_name> .</client_name>
       <comment><![CDATA[]]></comment>
       <reply><![CDATA[]]></reply>
       <is_received>1</is_received>
       <is_fast>1</is_fast>
       <is_well_packed>1</is_well_packed>
       <is_good_shape>1</is_good_shape>
       <created_at>2011-01-19T14:36:01+01:00</created_at>
     </client_order_comment>
   </client_order_comments_query_response>

.. _client_order_comment_date_constraint:

client_order_comment_date_constraint
====================================

Attributes
----------

+------+----------------------+-------------------------------+----------+
| Name | Description          | Type                          | Required |
+======+======================+===============================+==========+
| type | Date type for filter | :ref:`offer_state_constraint` | required |
+------+----------------------+-------------------------------+----------+


Elements
--------

+------+-------------+-------------+------------+-+
| Name | Description | Type        | Occurrence | |
+======+=============+=============+============+=+
| min  | Min date    | xs:dateTime | 0-1        | |
+------+-------------+-------------+------------+-+
| max  | Max date    | xs:dateTime | 0-1        | |
+------+-------------+-------------+------------+-+

.. _client_order_comment_rate_constraint:

client_order_comment_rate_constraint
====================================

Attributes
----------

+-------+------------------------+----------------------------------+----------+---------+
| Name  | Description            | Type                             | Required | Default |
+=======+========================+==================================+==========+=========+
| mode  | Comparison type to use | :ref:`offer_quantity_constraint` | optional | Equals  |
+-------+------------------------+----------------------------------+----------+---------+
| value | Rate value             | xs:nonNegativeInteger            | optional | 0       |
+-------+------------------------+----------------------------------+----------+---------+

.. _client_order_comment:

client_order_comment
====================

Elements
--------

+-------------------------+--------------------------------------------+-------------+------------+-+
| Name                    | Description                                | Type        | Occurrence | |
+=========================+============================================+=============+============+=+
| rate                    | Rate given by client                       | xs:integer  | 1-1        | |
+-------------------------+--------------------------------------------+-------------+------------+-+
| order_fnac_id           | Order unique identifier from fnac          | :ref:`uuid` | 1-1        | |
+-------------------------+--------------------------------------------+-------------+------------+-+
| client_order_comment_id | Comment unique identifier from fnac        | xs:string   | 1-1        | |
+-------------------------+--------------------------------------------+-------------+------------+-+
| client_name             | Client name                                | xs:string   | 1-1        | |
+-------------------------+--------------------------------------------+-------------+------------+-+
| comment                 | Comment content                            | xs:string   | 1-1        | |
+-------------------------+--------------------------------------------+-------------+------------+-+
| reply                   | Comment reply from seller                  | xs:string   | 0-1        | |
+-------------------------+--------------------------------------------+-------------+------------+-+
| is_received             | Order has been received by client ?        | xs:boolean  | 1-1        | |
+-------------------------+--------------------------------------------+-------------+------------+-+
| is_fast                 | Order has been received in a short delay ? | xs:boolean  | 1-1        | |
+-------------------------+--------------------------------------------+-------------+------------+-+
| is_well_packed          | Order was well packaged ?                  | xs:boolean  | 1-1        | |
+-------------------------+--------------------------------------------+-------------+------------+-+
| is_good_shape           | Order is in good shape ?                   | xs:boolean  | 1-1        | |
+-------------------------+--------------------------------------------+-------------+------------+-+
| created_at              | Creation date of comment                   | xs:dateTime | 1-1        | |
+-------------------------+--------------------------------------------+-------------+------------+-+
