.. contents::
   :depth: 3
   :backlinks: none

Messages Update
^^^^^^^^^^^^^^^

Informations
============

Description
-----------

This is service is used to update message sent on your offers or orders : reply, set as read, etc...

.. note::
   You can only have one reply by offer message while you can have several replies for one order message. 

Process
-------

#. A valid authentification token is needed.
#. The partner sends a list of messages to reply to, set as read, archive, etc ...
#. The service returns the processing result.

Request
=======

URL to use
----------

* Staging: https://partners-test.mp.fnacdarty.com/api.php/messages_update
* Production: https://vendeur.fnac.com/api.php/messages_update

Root element
------------

* Name: messages_update
* Extends: :ref:`base_request`

Elements
--------

+---------+--------------------+-----------------------+-------------+-+
| Name    | Description        | Type                  | Occurrence  | |
+=========+====================+=======================+=============+=+
| message | Messages to update | :ref:`message_update` | 1-unbounded | |
+---------+--------------------+-----------------------+-------------+-+

XML Sample
----------

.. code-block:: xml
   
   <?xml version="1.0" encoding="utf-8"?>
   <messages_update xmlns="http://www.fnac.com/schemas/mp-dialog.xsd" shop_id="73571111-3A94-2EE1-762A-2858EDE4F9BB" partner_id="F0A7BCF5-9B13-611D-6104-261780F88E38" token="469E283A-A9A2-7B82-B5C2-07B89B4AF32A">
      <message action="mark_as_read" id="AE7A5365-79CF-71C6-BEB4-FA1492C1FB4D"/>
      <message action="reply" id="AE7A5365-79CF-71C6-BEB4-FA1492C1FB4D">
        <message_to><![CDATA[ALL]]></message_to>
        <message_subject><![CDATA[product_information]]></message_subject>
        <message_description><![CDATA[My response to the message]]></message_description>
        <message_type><![CDATA[ORDER]]></message_type>
        <message_file>
            <filename>filename.ext</filename>
            <data>File encoded in Base 64</data>
            <message_filetype>FileType Constant</message_filetype>
        </message_file>
      </message>
    </messages_update>

.. code-block:: xml
   
   <?xml version="1.0" encoding="utf-8"?>
   <messages_update xmlns="http://www.fnac.com/schemas/mp-dialog.xsd" shop_id="73571111-3A94-2EE1-762A-2858EDE4F9BB" partner_id="F0A7BCF5-9B13-611D-6104-261780F88E38" token="469E283A-A9A2-7B82-B5C2-07B89B4AF32A">
      <message action="create" id="1PETPWFNJNS6Y">
        <message_to><![CDATA[ALL]]></message_to>
        <message_subject><![CDATA[order_information]]></message_subject>
        <message_description><![CDATA[Your order has been shipped.]]></message_description>
        <message_type><![CDATA[ORDER]]></message_type>
        <message_file>
            <filename>filename.ext</filename>
            <data>File encoded in Base 64</data>
            <message_filetype>FileType Constant</message_filetype>
        </message_file>
        <message_file>
            <filename>filename.ext</filename>
            <data>File encoded in Base 64</data>
            <message_filetype>FileType Constant</message_filetype>
        </message_file>
      </message>
    </messages_update>

Response
========

Root element
------------

* Name: messages_update_response
* Extends: :ref:`base_response`


Elements
--------

+---------+------------------+---------------------------------+-------------+-+
| Name    | Description      | Type                            | Occurrence  | |
+=========+==================+=================================+=============+=+
| error   | Errors           | :ref:`error`                    | 0-unbounded | |
+---------+------------------+---------------------------------+-------------+-+
| message | Messages updated | :ref:`messages_update_response` | 0-unbounded | |
+---------+------------------+---------------------------------+-------------+-+

XML Sample
----------

.. code-block:: xml
   
   <?xml version="1.0" encoding="utf-8"?>
   <messages_update_response xmlns="http://www.fnac.com/schemas/mp-dialog.xsd" status="OK">
     <message status="OK" id="AE7A5365-79CF-71C6-BEB4-FA1492C1FB4D"/>
     <message status="OK" id="92E6410E-F981-881E-2334-17353E5A8561"/>
   </messages_update_response>
   

.. _message_action:

message_action
==============

Available actions on message

* Extends: xs:string
* Enumerate:
  
  * mark_as_read: Set the message as read
  * mark_as_unread: Set the message as unread
  * archive: Set the message as archived
  * mark_as_read_and_archive: Set the message as read and archived
  * unarchive: Remove the message from archived
  * reply: Reply to the message
  * create: Create a message (works only on orders related messages)
  
.. _message_to:

message_to
==========

Recipient of the message

* Extends: xs:string
* Enumerate:
  
  * CALLCENTER: Send the message to call center
  * CLIENT: Send the message to client
  * ALL: Send a message to both

.. _message_subject:

message_subject
===============

* Extends: xs:string
* Enumerate:
  
  * product_information: Message or question about related product
  * shipping_information: Message or question about related shipping
  * order_information: Message or question about related order general info
  * offer_problem: Message or question about specific problem with related offer
  * offer_not_received: Message or question about related offer not received
  * other_question: Message or question about any other subject not listed above

.. _message_files:

message_files
============

To add 0 to 5 attachments to your message. The file should be encoded in base 64
* Extends: xs:complexType

  
.. _message_update:

message_update
==============

Attributes
----------

+--------+-------------------------------------------------------------------------+-----------------------+----------+---------+
| Name   | Description                                                             | Type                  | Required | Default |
+========+=========================================================================+=======================+==========+=========+
| id     | Message unique identifier for replying, or order ID on message creation | xs:string             | required |         |
+--------+-------------------------------------------------------------------------+-----------------------+----------+---------+
| action | Action to do on message                                                 | :ref:`message_action` | required |         |
+--------+-------------------------------------------------------------------------+-----------------------+----------+---------+


Elements
--------

+---------------------+-------------------------+------------------------+------------+-+
| Name                | Description             | Type                   | Occurrence | |
+=====================+=========================+========================+============+=+
| message_to          | Message recipient       | :ref:`message_to`      | 0-1        | |
+---------------------+-------------------------+------------------------+------------+-+
| message_subject     | Reply subject           | :ref:`message_subject` | 0-1        | |
+---------------------+-------------------------+------------------------+------------+-+
| message_description | Reply content           | xs:string              | 0-1        | |
+---------------------+-------------------------+------------------------+------------+-+
| message_type        | Message type            | :ref:`message_type`    | 0-1        | |
+---------------------+-------------------------+------------------------+------------+-+
| message_file        | Message File            | :ref:`message_files`   | 0-5        | |
+---------------------+-------------------------+------------------------+------------+-+

.. _message_file:

message_file
==============

.. _filename:

filename
==============

The name of your file

* Extends: xs:string

.. _data:

data
==============

Your file, base64 encoded

* Extends: xs:base64Binary

.. _message_filetype:

message_filetype
==============

The type of your file. Important to attach invoices to your order. List of constants : :ref:`filename_constant`

* Extends: xs:simpleType
* Enumerate:

  * TYPE_INVOICE: To attach an invoice to your message. This invoice will also be linked to your Order
  * TYPE_RETURN_DOC: Your file is a return label
  * TYPE_INSTRUCTIONS: Your file is a user manual or user instructions
  * TYPE_RETURN_CONDITIONS: Your file details the return conditions
  * TYPE_PHOTO: Your file is an image or a picture
  * TYPE_PROTEST_DOC: Your file is for / about / linked to a shipping dispute
  * TYPE_OTHER_OR_BLANK: For all other type or untyped files

Elements
--------

+---------------------+------------------------------+---------------------------+------------+-+
| Name                | Description                  | Type                      | Occurrence | |
+=====================+==============================+===========================+============+=+
| filename            | Name + Ext of the file       | :ref:`filename`           | 1-1        | |
+---------------------+------------------------------+---------------------------+------------+-+
| data                | Base64 encoded file          | :ref:`data`               | 1-1        | |
+---------------------+------------------------------+---------------------------+------------+-+
| message_filetype    | Constant to type your file   | :ref:`message_filetype`   | 1-1        | |
+---------------------+------------------------------+---------------------------+------------+-+

.. _filename_constant:

FileType Constant
--------

+---------------------------+------------------------------------------------------------------------------------------+
| Name                      | Description                                                                              |
+===========================+==========================================================================================+
| TYPE_INVOICE              | To attach an invoice to your message. This invoice will also be linked to your Order     |
+---------------------------+------------------------------------------------------------------------------------------+
| TYPE_RETURN_DOC           | Your file is a return label                                                              |
+---------------------------+------------------------------------------------------------------------------------------+
| TYPE_INSTRUCTIONS         | Your file is a user manual or user instructions                                          |
+---------------------------+------------------------------------------------------------------------------------------+
| TYPE_RETURN_CONDITIONS    | Your file details the return conditions                                                  |
+---------------------------+------------------------------------------------------------------------------------------+
| TYPE_PHOTO                | Your file is an image or a picture                                                       |
+---------------------------+------------------------------------------------------------------------------------------+
| TYPE_PROTEST_DOC          | Your file is for / about / linked to a shipping dispute                                  |
+---------------------------+------------------------------------------------------------------------------------------+
| TYPE_OTHER_OR_BLANK       | For all other type or untyped files                                                      |
+---------------------------+------------------------------------------------------------------------------------------+

.. _messages_update_response:

messages_update_response
========================

Attributes
----------

+--------+-----------------------------+---------------------+----------+---------+
| Name   | Description                 | Type                | Required | Default |
+========+=============================+=====================+==========+=========+
| id     | Message's unique identifier | xs:string           | optional |         |
+--------+-----------------------------+---------------------+----------+---------+
| status | Update status               | :ref:`status_code`  | required |         |
+--------+-----------------------------+---------------------+----------+---------+


Elements
--------

+-------+-------------+--------------+-------------+-+
| Name  | Description | Type         | Occurrence  | |
+=======+=============+==============+=============+=+
| error | Errors      | :ref:`error` | 0-unbounded | |
+-------+-------------+--------------+-------------+-+
