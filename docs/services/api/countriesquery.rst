.. contents::
   :depth: 3
   :backlinks: none

.. _countries_query_service:

Countries Query
^^^^^^^^^^^^^^

Informations
============

Description
-----------

This is service is used to get the available countries and their Alpha3 code.

Process
-------

#. A valid authentification token is needed.
#. The partner sends the request to the service.
#. Service returns all available countries with their name and code to use with the Fnac Markeplace API.

Request
=======

URL to use
----------

* Staging: https://partners-test.mp.fnacdarty.com/api.php/countries_query
* Production: https://vendeur.fnac.com/api.php/countries_query

Root element
------------

* Name: countries_query
* Extends: :ref:`base_request`

Elements
--------

+-------+-------------+---------------------+-------------+-+
| Name  | Description | Type                | Occurrence  | |
+=======+=============+=====================+=============+=+
| query |             | :ref:`query_string` | 0-unbounded | |
+-------+-------------+---------------------+-------------+-+

XML Sample
----------

.. code-block:: xml
   
   <?xml version="1.0" encoding="utf-8"?>
   <countries_query xmlns="http://www.fnac.com/schemas/mp-dialog.xsd" shop_id="73571111-4AC1-6B59-1AB5-98462FB4B3B1" partner_id="F0A7BCF5-945A-527F-3D1C-3C1D353A49D5" token="469E283A-4CD9-12A2-DDCD-CB319DDD52E7">
     <query><![CDATA[all]]></query>
   </countries_query>
   

Response
========

Root element
------------

* Name: countries_query_response
* Extends: :ref:`base_response`

Elements
--------

+---------+-------------+----------------+-------------+-+
| Name    | Description | Type           | Occurrence  | |
+=========+=============+================+=============+=+
| error   | Error list  | :ref:`error`   | 0-unbounded | |
+---------+-------------+----------------+-------------+-+
| country |             | :ref:`country` | 0-unbounded | |
+---------+-------------+----------------+-------------+-+

XML Sample
----------

.. code-block:: xml
    
   <?xml version="1.0" encoding="utf-8"?>
   <countries_query_response xmlns="http://www.fnac.com/schemas/mp-dialog.xsd" status="OK">
   <country>
     <name><![CDATA[Irlande du Nord]]></name>
     <code><![CDATA[NIR]]></code>
   </country>
   <country>
     <name><![CDATA[Îles Aland (Finlande)]]></name>
     <code><![CDATA[IA]]></code>
   </country>
   <country>
     <name><![CDATA[Afghanistan]]></name>
     <code><![CDATA[AFG]]></code>
   </country>
   <country>
     <name><![CDATA[Albania]]></name>
     <code><![CDATA[ALB]]></code>
   </country>
   ...
   <country>
     <name><![CDATA[Belgium]]></name>
     <code><![CDATA[BEL]]></code>
   </country>
   ...
   <country>
     <name><![CDATA[Metropolitan France]]></name>
     <code><![CDATA[FRA]]></code>
   </country>
   ...
   <country>
     <name><![CDATA[China]]></name>
     <code><![CDATA[CHN]]></code>
   </country>
   ...
   <country>
     <name><![CDATA[Portugal]]></name>
     <code><![CDATA[PRT]]></code>
   </country>
   ...
   <country>
     <name><![CDATA[Spain]]></name>
     <code><![CDATA[ESP]]></code>
   </country>
   ...
   <country>
     <name><![CDATA[Western Sahara]]></name>
     <code><![CDATA[ESH]]></code>
   </country>
   <country>
     <name><![CDATA[Yemen]]></name>
     <code><![CDATA[YEM]]></code>
   </country>
   <country>
     <name><![CDATA[Zambia]]></name>
     <code><![CDATA[ZMB]]></code>
   </country>
   <country>
     <name><![CDATA[Zimbabwe]]></name>
     <code><![CDATA[ZIM]]></code>
   </country>
   </countries_query_response>
   

.. _country:

country
=======

Elements
--------

+------+---------------------------+------------+------------+-+
| Name | Description               | Type       | Occurrence | |
+======+===========================+============+============+=+
| name | Country name              | xs:string  | 1-1        | |
+------+---------------------------+------------+------------+-+
| code | Country Alpha3 identifier | xs:string  | 1-1        | |
+------+---------------------------+------------+------------+-+
