.. contents::
   :depth: 3
   :backlinks: none

.. _pricing-query-v2:

Pricing Query (V2)
^^^^^^^^^^^^^^^^^^

.. note::
   
   Be advised that requests regarding shops outside of France have accessed only to :ref:`pricing_query v1 <pricing-query-v1>`. Shops operating in France have access to :ref:`pricing_query v2 <pricing-query-v2>` and :ref:`pricing_query v3 <pricing-query-v3>` as well.

Informations
============

Description
-----------

Retrieve the best prices applied to a given product within all Fnac marketplace sellers (Fnac included)

Process
-------

#. A valid authentification token is needed.
#. The partner calls service with a list of product reference
#. The service returns for each product reference a list of the lowest prices suggested by Fnac Marketplace sellers.

The number of product references to request is limited to 10.

Request
=======

URL to use
----------

* Staging: https://partners-test.mp.fnacdarty.com/api.php/pricing_query
* Production: https://vendeur.fnac.com/api.php/pricing_query

Root element
------------

* Name: pricing_query
* Extends: :ref:`base_request`


Elements
--------

+-------------------+---------------------+--------------------------+-------------+-+
| Name              | Description         | Type                     | Occurrence  | |
+===================+=====================+==========================+=============+=+
| product_reference | Products references | :ref:`product_reference` | 1-10        | |
+-------------------+---------------------+--------------------------+-------------+-+

XML Sample
----------

.. code-block:: xml
   
   <?xml version="1.0" encoding="utf-8"?>
   <pricing_query xmlns="http://www.fnac.com/schemas/mp-dialog.xsd" shop_id="BBBFA40E-3A94-2EE1-762A-2858EDE4F9BB" partner_id="C906104B-9B13-611D-6104-261780F88E38" token="6EB5F43F-33CF-5C33-D204-278907C0FD9D">
     <product_reference type="Ean">0886971942323</product_reference>
   </pricing_query>


Response
========

Root element
------------

* Name: pricing_query_response
* Extends: :ref:`base_response`

Elements
--------

+-----------------+---------------------+-----------------------------------------------------+-------------+-+
| Name            | Description         | Type                                                | Occurrence  | |
+=================+=====================+=====================================================+=============+=+
| error           | Errors              | :ref:`error`                                        | 0-unbounded | |
+-----------------+---------------------+-----------------------------------------------------+-------------+-+
| pricing_product | Pricings of product | :ref:`pricing_product <pricing_product_v2>`         | 0-unbounded | |
+-----------------+---------------------+-----------------------------------------------------+-------------+-+

XML Sample
----------

.. code-block:: xml
   
    <?xml version="1.0" encoding="utf-8"?> 
    <pricing_query_response xmlns="http://www.fnac.com/schemas/mp-dialog.xsd" status="OK">
        <pricing_product>
            <product_reference type="Ean">0886971942323</product_reference>
            <ean>0886971942323</ean>
            <product_url><![CDATA[http://www4.rec1.fnac.dev/Shelf/Article4.aspx?prid=2066124]]></product_url>
            <seller_price>14</seller_price>
            <seller_shipping>2.39</seller_shipping>
            <seller_offer_sku>4F1A-28E-8BE</seller_offer_sku>
            <seller_offer_state>2</seller_offer_state>
            <seller_adherent_price>10</seller_adherent_price>
            <seller_adherent_shipping>2.39</seller_adherent_shipping>
            <seller_adherent_offer_state>2</seller_adherent_offer_state>
            <seller_adherent_offer_sku>4F1A-28E-8BE</seller_adherent_offer_sku>
            <new_price>0.90</new_price>
            <new_shipping>2.39</new_shipping>
            <refurbished_price/>
            <refurbished_shipping/>
            <used_price>2.90</used_price>
            <used_shipping>2.39</used_shipping>
            <new_adherent_price/>
            <new_adherent_shipping/>
            <refurbished_adherent_price/>
            <refurbished_adherent_shipping/>
            <used_adherent_price>1.50</used_adherent_price>
            <used_adherent_shipping>2.39</used_adherent_shipping>
        </pricing_product>
    </pricing_query_response>


If no price is found for a product, the following error will be sent as a response :

.. code-block:: xml

    <?xml version="1.0" encoding="utf-8"?>
    <pricing_query_response status="OK" xmlns="http://www.fnac.com/schemas/mp-dialog.xsd">
        <pricing_product>
            <product_reference type="Ean">4719072296902</product_reference>
            <error severity="ERROR" code="ERR_120">Service Pricing : No prices found.</error>                            
        </pricing_product>
    </pricing_query_response>

.. _pricing_product_v2:

pricing_product
===============

Attributes
----------

+------+---------------------+---------------------+----------+---------+
| Name | Description         | Type                | Required | Default |
+======+=====================+=====================+==========+=========+
| type | Product seller type | :ref:`pricing_type` | required |         |
+------+---------------------+---------------------+----------+---------+


Elements
--------

+-------------------------------+----------------------------------------------------------------+--------------------------+-------------+-+
| Name                          | Description                                                    | Type                     | Occurrence  | |
+===============================+================================================================+==========================+=============+=+
| product_reference             | Product reference                                              | :ref:`product_reference` | 1-1         | |
+-------------------------------+----------------------------------------------------------------+--------------------------+-------------+-+
| ean                           | Product EAN                                                    | :ref:`string255`         | 1-1         | |
+-------------------------------+----------------------------------------------------------------+--------------------------+-------------+-+
| product_url                   | Product page Fnac URL                                          | :ref:`string255`         | 1-1         | |
+-------------------------------+----------------------------------------------------------------+--------------------------+-------------+-+
| product_name                  | Product name                                                   | :ref:`string255`         | 1-1         | |
+-------------------------------+----------------------------------------------------------------+--------------------------+-------------+-+
| seller_price                  | Seller own price                                               | xs:decimal               | 0-1         | |
+-------------------------------+----------------------------------------------------------------+--------------------------+-------------+-+
| seller_shipping               | Seller own offer shipping costs                                | xs:decimal               | 0-1         | |
+-------------------------------+----------------------------------------------------------------+--------------------------+-------------+-+
| seller_offer_sku              | Seller own offer SKU                                           | xs:string                | 0-1         | |
+-------------------------------+----------------------------------------------------------------+--------------------------+-------------+-+
| seller_offer_state            | Seller own offer state                                         | :ref:`product_state`     | 0-1         | |
+-------------------------------+----------------------------------------------------------------+--------------------------+-------------+-+
| seller_adherent_price         | Seller own offer adherent price                                | xs:decimal               | 0-1         | |
+-------------------------------+----------------------------------------------------------------+--------------------------+-------------+-+
| seller_adherent_shipping      | Seller own offer shipping costs for adherent offer             | xs:decimal               | 0-1         | |
+-------------------------------+----------------------------------------------------------------+--------------------------+-------------+-+
| seller_adherent_offer_state   | Seller own adherent offer state                                | :ref:`product_state`     | 0-1         | |
+-------------------------------+----------------------------------------------------------------+--------------------------+-------------+-+
| seller_adherent_offer_sku     | Seller own adherent offer sku                                  | xs:string                | 0-1         | |
+-------------------------------+----------------------------------------------------------------+--------------------------+-------------+-+
| new_price                     | Best price for a new product                                   | xs:decimal               | 1-1         | |
+-------------------------------+----------------------------------------------------------------+--------------------------+-------------+-+
| new_shipping                  | Shipping costs for the best new product price                  | xs:decimal               | 1-1         | |
+-------------------------------+----------------------------------------------------------------+--------------------------+-------------+-+
| refurbished_price             | Best price for a refurbished product                           | xs:decimal               | 1-1         | |
+-------------------------------+----------------------------------------------------------------+--------------------------+-------------+-+
| refurbished_shipping          | Shipping costs for the best refurbished product price          | xs:decimal               | 1-1         | |
+-------------------------------+----------------------------------------------------------------+--------------------------+-------------+-+
| used_price                    | Best price for a used product                                  | xs:decimal               | 1-1         | |
+-------------------------------+----------------------------------------------------------------+--------------------------+-------------+-+
| used_shipping                 | Shipping costs for the best used product price                 | xs:decimal               | 1-1         | |
+-------------------------------+----------------------------------------------------------------+--------------------------+-------------+-+
| new_adherent_price            | Best adherent price for a new product                          | xs:decimal               | 1-1         | |
+-------------------------------+----------------------------------------------------------------+--------------------------+-------------+-+
| new_adherent_shipping         | Shipping costs for the best new product adherent price         | xs:decimal               | 1-1         | |
+-------------------------------+----------------------------------------------------------------+--------------------------+-------------+-+
| refurbished_adherent_price    | Best adherent price for a refurbished product                  | xs:decimal               | 1-1         | |
+-------------------------------+----------------------------------------------------------------+--------------------------+-------------+-+
| refurbished_adherent_shipping | Shipping costs for the best refurbished product adherent price | xs:decimal               | 1-1         | |
+-------------------------------+----------------------------------------------------------------+--------------------------+-------------+-+
| used_adherent_price           | Best adherent price for a used product                         | xs:decimal               | 1-1         | |
+-------------------------------+----------------------------------------------------------------+--------------------------+-------------+-+
| used_adherent_shipping        | Shipping costs for the best used product adherent price        | xs:decimal               | 1-1         | |
+-------------------------------+----------------------------------------------------------------+--------------------------+-------------+-+

Please note that the fields prefixed with ``"``seller_``"`` will appear only if the calling seller has an offer on the requested product.