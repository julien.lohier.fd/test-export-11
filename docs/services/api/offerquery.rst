.. contents::
   :depth: 3
   :backlinks: none

.. _offers_query_service:

Offers Query
^^^^^^^^^^^^

Informations
============

Description
-----------

This is service is used to retrieve offers from your shop catalog according to submitted criteria.

Process
-------

#. A valid authentification token is needed.
#. The partner calls the service with criteria.
#. The service returns the selected offers.

Limitation
----------

Query is limited to 10 000 offers per call. Above this limit, an error will be thrown in the response.

Request
=======

URL to use
----------

* Staging: https://partners-test.mp.fnacdarty.com/api.php/offers_query
* Production: https://vendeur.fnac.com/api.php/offers_query

Root element
------------

* Name: offers_query
* Extends: :ref:`base_request`

Attributes
----------

+---------------+------------------------------------+--------------------+----------+---------+
| Name          | Description                        | Type               | Required | Default |
+===============+====================================+====================+==========+=========+
| results_count | Max number of results per page     | xs:positiveInteger | optional | 100     |
+---------------+------------------------------------+--------------------+----------+---------+
| with_fees     | display fees amount for each offer | xs:boolean         | optional | false   |
+---------------+------------------------------------+--------------------+----------+---------+


Elements
--------

+-----------------+---------------------------------------+------------------------------+------------+-+
| Name            | Description                           | Type                         | Occurrence | |
+=================+=======================================+==============================+============+=+
| paging          | Page number to fetch                  | xs:int                       | 0-1        | |
+-----------------+---------------------------------------+------------------------------+------------+-+
| date            | Date filter                           | :ref:`offer_date_constraint` | 0-1        | |
+-----------------+---------------------------------------+------------------------------+------------+-+
| quantity        | Quantity filter                       | :ref:`offer_quantity`        | 0-1        | |
+-----------------+---------------------------------------+------------------------------+------------+-+
| product_fnac_id | Product's unique identifier from fnac | xs:string                    | 0-1        | |
+-----------------+---------------------------------------+------------------------------+------------+-+
| offer_fnac_id   | Offer unique identifier from fnac     | xs:string                    | 0-1        | |
+-----------------+---------------------------------------+------------------------------+------------+-+
| offer_seller_id | Offer unique identifier from seller   | xs:string                    | 0-1        | |
+-----------------+---------------------------------------+------------------------------+------------+-+
| promotion_types | Offer unique identifier from fnac     | :ref:`promotion_types`       | 0-1        | |
+-----------------+---------------------------------------+------------------------------+------------+-+
| promotion_uids  | Offer unique identifier from seller   | :ref:`promotion_uids`        | 0-1        | |
+-----------------+---------------------------------------+------------------------------+------------+-+

XML Samples
-----------

Retrieve the two first items of the catalog.

.. code-block:: xml
   
   <?xml version="1.0" encoding="utf-8"?>
   <offers_query results_count="2" xmlns="http://www.fnac.com/schemas/mp-dialog.xsd" shop_id="0000000-4AC1-6B59-1AB5-98462FB4B3B1" partner_id="0000000-945A-527F-3D1C-3C1D353A49D5" token="0000000-5814-B0A9-6AFB-EDDB32253631">
     <paging>1</paging>
   </offers_query>


Retrieve products that are out of stock.

.. code-block:: xml

  <?xml version="1.0" encoding="utf-8"?>
  <offers_query results_count="2" shop_id="0000000-4AC1-6B59-1AB5-98462FB4B3B1" partner_id="0000000-945A-527F-3D1C-3C1D353A49D5" token="0000000-5814-B0A9-6AFB-EDDB32253631" xmlns="http://www.fnac.com/schemas/mp-dialog.xsd">
    <quantity mode="Equals" value="0"/>
  </offers_query>


Retrieve products modified between 2 dates

.. code-block:: xml

    <?xml version="1.0" encoding="utf-8"?>
    <offers_query results_count="2" shop_id="0000000-4AC1-6B59-1AB5-98462FB4B3B1" partner_id="0000000-945A-527F-3D1C-3C1D353A49D5" token="0000000-5814-B0A9-6AFB-EDDB32253631" xmlns="http://www.fnac.com/schemas/mp-dialog.xsd">
      <paging>1</paging>
      <date type="Modified">
        <min>2013-12-16T00:00:00+01:00</min>
        <max>2013-12-18T00:00:00+01:00</max>
      </date>
    </offers_query>


Retrieve an offer with its associated fees

.. code-block:: xml

    <?xml version="1.0" encoding="utf-8"?>
    <offers_query with_fees="true" shop_id="0000000-4AC1-6B59-1AB5-98462FB4B3B1" partner_id="0000000-945A-527F-3D1C-3C1D353A49D5" token="0000000-5814-B0A9-6AFB-EDDB32253631" xmlns="http://www.fnac.com/schemas/mp-dialog.xsd">
      <offer_seller_id>MYSKU</offer_seller_id>
    </offers_query>

Response
========

Root element
------------

* Name: offers_query_response
* Extends: :ref:`base_response`

Elements
--------

+-------------------+----------------------------+--------------+-------------+-+
| Name              | Description                | Type         | Occurrence  | |
+===================+============================+==============+=============+=+
| error             | Errors                     | :ref:`error` | 0-unbounded | |
+-------------------+----------------------------+--------------+-------------+-+
| page              | Page number                | xs:int       | 0-1         | |
+-------------------+----------------------------+--------------+-------------+-+
| total_paging      | Number of page available   | xs:int       | 0-1         | |
+-------------------+----------------------------+--------------+-------------+-+
| nb_total_per_page | Number of results per page | xs:int       | 0-1         | |
+-------------------+----------------------------+--------------+-------------+-+
| nb_total_result   | Number of results          | xs:int       | 0-1         | |
+-------------------+----------------------------+--------------+-------------+-+
| offer             | Offers                     | :ref:`offer` | 0-unbounded | |
+-------------------+----------------------------+--------------+-------------+-+

XML Sample
----------

Retrieve the two first items of the catalog.

.. code-block:: xml
   
  <?xml version="1.0" encoding="utf-8"?>
  <offers_query_response status="OK" xmlns="http://www.fnac.com/schemas/mp-dialog.xsd">

    <page>1</page>
    <total_paging>9659</total_paging>
    <nb_total_per_page>2</nb_total_per_page>
    <nb_total_result>19317</nb_total_result>

    <offer>
      <product_name><![CDATA[Olympus SZ-15 Argent ( Appareil photo numérique ) - ]]></product_name>
      <product_fnac_id>20253789</product_fnac_id>
      <offer_fnac_id>4FA37F03-6D12-F6DA-7ED7-BCD83BC13788</offer_fnac_id>
      <offer_seller_id><![CDATA[1000116186]]></offer_seller_id>
      <product_state>11</product_state>
      <price>239.3</price>
      <fee_excluding_taxes>10.85</fee_excluding_taxes>
      <fee_including_all_taxes>13.72</fee_including_all_taxes>
      <quantity>4</quantity>
      <description><![CDATA[Nouveau. Emballage d`origine.]]></description>
      <description_fr><![CDATA[Nouveau. Emballage d`origine.]]></description_fr>
      <description_nl><![CDATA[Nieuw. Originele verpakking.]]></description_nl>
      <internal_comment><![CDATA[DGH-669501]]></internal_comment>
      <product_url><![CDATA[https://www.fnac.com/marketplace/articleoffers.aspx?prid=5586892&ref=Fnac.com]]></product_url>
      <image><![CDATA[]]></image>
      <nb_messages>0</nb_messages>
      <showcase></showcase>
    </offer>

    <offer>
      <product_name><![CDATA[Olympus SZ-15 Noir ( Appareil photo numérique ) - ]]></product_name>
      <product_fnac_id>20253836</product_fnac_id>
      <offer_fnac_id>A24FE81E-DD5D-F483-250B-AAC117860121</offer_fnac_id>
      <offer_seller_id><![CDATA[1000116187]]></offer_seller_id>
      <product_state>11</product_state>
      <price>239.3</price>
      <fee_excluding_taxes>10.85</fee_excluding_taxes>
      <fee_including_all_taxes>13.72</fee_including_all_taxes>
      <quantity>3</quantity>
      <description><![CDATA[Nouveau. Emballage d`origine.]]></description>
      <description_fr><![CDATA[Nouveau. Emballage d`origine.]]></description_fr>
      <description_nl><![CDATA[Nieuw. Originele verpakking.]]></description_nl>
      <internal_comment><![CDATA[DGH-669522]]></internal_comment>
      <product_url><![CDATA[https://www.fnac.com/marketplace/articleoffers.aspx?prid=5586891&ref=Fnac.com]]></product_url>
      <image><![CDATA[]]></image>
      <nb_messages>0</nb_messages>
      <showcase></showcase>
    </offer>

  </offers_query_response>


Retrieve products that are out of stock.

.. code-block:: xml

  <?xml version="1.0" encoding="utf-8"?>
  <offers_query_response status="OK" xmlns="http://www.fnac.com/schemas/mp-dialog.xsd">

    <offer>
      <product_name><![CDATA[Pad Circulaire Pro Nintendo pour Nintendo 3DS XL ( Accessoire console de jeux ) - ]]></product_name>
      <product_fnac_id>20209283</product_fnac_id>
      <offer_fnac_id>4CA5F344-82D3-C46F-349C-FB2C3C11389F</offer_fnac_id>
      <offer_seller_id><![CDATA[1000118422]]></offer_seller_id>
      <product_state>11</product_state>
      <price>28.98</price>
      <fee_excluding_taxes>1.65</fee_excluding_taxes>
      <fee_including_all_taxes>1.89</fee_including_all_taxes>
      <quantity>0</quantity>
      <description><![CDATA[Nouveau. Emballage d`origine.]]></description>
      <description_fr><![CDATA[Nouveau. Emballage d`origine.]]></description_fr>
      <description_nl><![CDATA[Nieuw. Originele verpakking.]]></description_nl>
      <internal_comment><![CDATA[DGH-677705]]></internal_comment>
      <product_url><![CDATA[https://www.fnac.com/marketplace/articleoffers.aspx?prid=5526397&ref=Fnac.com]]></product_url>
      <image><![CDATA[]]></image>
      <nb_messages>0</nb_messages>
      <showcase></showcase>
    </offer>

    <offer>
      <product_name><![CDATA[Plantronics kit d'embouts auriculaires ( Accessoire image & son ) - ]]></product_name>
      <product_fnac_id>20218050</product_fnac_id>
      <offer_fnac_id>BE5211F0-EAC8-D13F-63D7-82566D893011</offer_fnac_id>
      <offer_seller_id><![CDATA[1000120207]]></offer_seller_id>
      <product_state>11</product_state>
      <price>8.39</price>
      <fee_excluding_taxes>0.85</fee_excluding_taxes>
      <fee_including_all_taxes>1.12</fee_including_all_taxes>
      <quantity>0</quantity>
      <description><![CDATA[Nouveau. Emballage d`origine.]]></description>
      <description_fr><![CDATA[Nouveau. Emballage d`origine.]]></description_fr>
      <description_nl><![CDATA[Nieuw. Originele verpakking.]]></description_nl>
      <internal_comment><![CDATA[KOM-40-05-4352]]></internal_comment>
      <product_url><![CDATA[https://www.fnac.com/marketplace/articleoffers.aspx?prid=20218050&ref=MarketPlace]]></product_url>
      <image><![CDATA[]]></image>
      <nb_messages>0</nb_messages>
      <showcase></showcase>
    </offer>

  </offers_query_response>

.. _promotion_types:

promotion_types
===============

Elements
--------

+-------------------------+-------------------------+-----------------------+-------------+-+
| Name                    | Description             | Type                  | Occurrence  | |
+=========================+=========================+=======================+=============+=+
| promotion_type          | Type of promotion       | :ref:`promotion_type` | 1-unbounded | |
+-------------------------+-------------------------+-----------------------+-------------+-+

.. _promotion_uids:

promotion_uids
==============

Elements
--------

+-------------------------+-------------------------+-----------------------+-------------+-+
| Name                    | Description             | Type                  | Occurrence  | |
+=========================+=========================+=======================+=============+=+
| promotion_uid           | A reference of yours    | :ref:`promotion_uid`  | 1-unbounded | |
+-------------------------+-------------------------+-----------------------+-------------+-+

.. _offer:

offer
=====

Elements
--------

+---------------------------+-------------------------------------------------------------+-----------------------+------------+-+
| Name                      | Description                                                 | Type                  | Occurrence | |
+===========================+=============================================================+=======================+============+=+
| product_name              | Name, category and a short description                      | xs:string             | 1-1        | |
+---------------------------+-------------------------------------------------------------+-----------------------+------------+-+
| product_fnac_id           | Product unique identifier from fnac                         | xs:string             | 1-1        | |
+---------------------------+-------------------------------------------------------------+-----------------------+------------+-+
| offer_fnac_id             | Offer unique identifier from fnac                           | xs:string             | 1-1        | |
+---------------------------+-------------------------------------------------------------+-----------------------+------------+-+
| offer_seller_id           | Offer unique identifier from seller                         | xs:string             | 1-1        | |
+---------------------------+-------------------------------------------------------------+-----------------------+------------+-+
| price                     | Product price for this offer                                | :ref:`price_090_20000`| 1-1        | |
+---------------------------+-------------------------------------------------------------+-----------------------+------------+-+
| instead_of_price          |                                                             | :ref:`price_090_20000`| 0-1        | |
+---------------------------+-------------------------------------------------------------+-----------------------+------------+-+
| fee_excluding_taxes       | Fee amount with no tax applied                              | xs:decimal            | 0-1        | |
+---------------------------+-------------------------------------------------------------+-----------------------+------------+-+
| fee_including_all_taxes   | Fee amount including all taxes                              | xs:decimal            | 0-1        | |
+---------------------------+-------------------------------------------------------------+-----------------------+------------+-+
| promotion_code_id         |                                                             | xs:integer            | 0-1        | |
+---------------------------+-------------------------------------------------------------+-----------------------+------------+-+
| adherent_price            | Fnac member price for this offer (see :ref:`offers_update`) | :ref:`price_090_20000`| 0-1        | |
+---------------------------+-------------------------------------------------------------+-----------------------+------------+-+
| promotion_code_adherent_id|                                                             | xs:integer            | 0-1        | |
+---------------------------+-------------------------------------------------------------+-----------------------+------------+-+
| product_state             | Product state in this offer                                 | :ref:`product_state`  | 1-1        | |
+---------------------------+-------------------------------------------------------------+-----------------------+------------+-+
| quantity                  | Product quantity in offer                                   | xs:nonNegativeInteger | 1-1        | |
+---------------------------+-------------------------------------------------------------+-----------------------+------------+-+
| description_fr            | Offer description in French (FR and BE Market)              | :ref:`string255`      | 0-1        | |
+---------------------------+-------------------------------------------------------------+-----------------------+------------+-+
| description_nl            | Offer description in Dutch (BE Market)                      | :ref:`string255`      | 0-1        | |
+---------------------------+-------------------------------------------------------------+-----------------------+------------+-+
| description_pt            | Offer description in Portuguese (PT Market)                 | :ref:`string255`      | 0-1        | |
+---------------------------+-------------------------------------------------------------+-----------------------+------------+-+
| description_es            | Offer description in Spanish (ES Market)                    | :ref:`string255`      | 0-1        | |
+---------------------------+-------------------------------------------------------------+-----------------------+------------+-+
| internal_comment          | Offer internal comment for personal use only                | :ref:`string255`      | 1-1        | |
+---------------------------+-------------------------------------------------------------+-----------------------+------------+-+
| product_url               | Product url on fnac.com                                     | xs:string             | 0-1        | |
+---------------------------+-------------------------------------------------------------+-----------------------+------------+-+
| image                     | Product image url on fnac.com                               | xs:string             | 0-1        | |
+---------------------------+-------------------------------------------------------------+-----------------------+------------+-+
| nb_messages               | Number of messages related to this offer                    | xs:nonNegativeInteger | 0-1        | |
+---------------------------+-------------------------------------------------------------+-----------------------+------------+-+
| showcase                  | Offer position in shop showcase                             | xs:nonNegativeInteger | 0-1        | |
+---------------------------+-------------------------------------------------------------+-----------------------+------------+-+
| is_shipping_free          | Is shipping free for this offer ?                           | xs:boolean            | 0-1        | |
+---------------------------+-------------------------------------------------------------+-----------------------+------------+-+
| promotion                 | Offer's promotion information                               | :ref:`promotion`      | 0-1        | |
+---------------------------+-------------------------------------------------------------+-----------------------+------------+-+
| type_label                | Category of sold product                                    | xs:string             | 0-1        | |
+---------------------------+-------------------------------------------------------------+-----------------------+------------+-+
| time_to_ship              | Offer's time to ship information (if is activate)           | xs:positiveInteger    | 0-1        | |
+---------------------------+-------------------------------------------------------------+-----------------------+------------+-+

.. _offer_date_constraint:

offer_date_constraint
=====================

Attributes
----------

+------+----------------------+-------------------------------+----------+
| Name | Description          | Type                          | Required |
+======+======================+===============================+==========+
| type | Date type for filter | :ref:`offer_state_constraint` | required |
+------+----------------------+-------------------------------+----------+


Elements
--------

+------+-------------+-------------+------------+-+
| Name | Description | Type        | Occurrence | |
+======+=============+=============+============+=+
| min  | Min date    | xs:dateTime | 0-1        | |
+------+-------------+-------------+------------+-+
| max  | Max date    | xs:dateTime | 0-1        | |
+------+-------------+-------------+------------+-+

.. _offer_quantity:

offer_quantity
==============

Attributes
----------

+-------+------------------+----------------------------------+----------+---------+
| Name  | Description      | Type                             | Required | Default |
+=======+==================+==================================+==========+=========+
| mode  | Compare mode     | :ref:`offer_quantity_constraint` | optional | Equals  |
+-------+------------------+----------------------------------+----------+---------+
| value | Value to compare | xs:nonNegativeInteger            | optional | 0       |
+-------+------------------+----------------------------------+----------+---------+
