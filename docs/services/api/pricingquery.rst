.. contents::
   :depth: 3
   :backlinks: none

.. _pricing-query-v1:

Pricing Query
^^^^^^^^^^^^^

.. note::
   
   Be advised that requests regarding shops outside of France have accessed only to :ref:`pricing_query v1 <pricing-query-v1>`. Shops operating in France have access to :ref:`pricing_query v2 <pricing-query-v2>` and :ref:`pricing_query v3 <pricing-query-v3>` as well.

Informations
============

Description
-----------

Compare price between all marketplace shop and fnac for a specific product.

Process
-------

#. A valid authentification token is needed.
#. The partner calls service with a list of product reference
#. The service returns for each product reference a list of the lowest prices suggested by Fnac Marketplace sellers.

The number of product references to request is limited to 10.

Request
=======

URL to use
----------

* Staging: https://partners-test.mp.fnacdarty.com/api.php/pricing_query
* Production: https://vendeur.fnac.com/api.php/pricing_query

Root element
------------

* Name: pricing_query
* Extends: :ref:`base_request`

Attributes
----------

+---------+------------------------------------------+------------------------+----------+---------+
| Name    | Description                              | Type                   | Required | Default |
+=========+==========================================+========================+==========+=========+
| sellers | Defines which sellers prices to retrieve | :ref:`pricing_sellers` | optional |         |
+---------+------------------------------------------+------------------------+----------+---------+


Elements
--------

+-------------------+---------------------+--------------------------+-------------+-+
| Name              | Description         | Type                     | Occurrence  | |
+===================+=====================+==========================+=============+=+
| product_reference | Products references | :ref:`product_reference` | 1-10        | |
+-------------------+---------------------+--------------------------+-------------+-+

XML Sample
----------

.. code-block:: xml
   
   <?xml version="1.0" encoding="utf-8"?>
   <pricing_query xmlns="http://www.fnac.com/schemas/mp-dialog.xsd" shop_id="BBBFA40E-3A94-2EE1-762A-2858EDE4F9BB" partner_id="C906104B-9B13-611D-6104-261780F88E38" token="6EB5F43F-33CF-5C33-D204-278907C0FD9D" sellers="all">
     <product_reference type="Ean">0886971942323</product_reference>
   </pricing_query>


Response
========

Root element
------------

* Name: pricing_query_response
* Extends: :ref:`base_response`

Elements
--------

+-----------------+---------------------+------------------------+-------------+-+
| Name            | Description         | Type                   | Occurrence  | |
+=================+=====================+========================+=============+=+
| error           | Errors              | :ref:`error`           | 0-unbounded | |
+-----------------+---------------------+------------------------+-------------+-+
| pricing_product | Pricings of product | :ref:`pricing_product` | 0-unbounded | |
+-----------------+---------------------+------------------------+-------------+-+

XML Sample
----------

.. code-block:: xml
   
   <?xml version="1.0" encoding="utf-8"?>
   <pricing_query_response xmlns="http://www.fnac.com/schemas/mp_dialog.xsd" status="OK">
      <pricing_product>
        <product_reference type="Ean">0886971942323</product_reference>
        <product_name><![CDATA[2Lor en moi ( Audio Variété ) - CD album]]></product_name>
        <image_url>http://multimedia.fnac.com/multimedia/images_produits/Grandes/3/2/3/0886971942323.gif</image_url>
        <pricing type="not professional">
          <price>0.9</price>
          <shipping_price>2.39</shipping_price>
          <offer_status>1</offer_status>
        </pricing>
        <pricing type="professional">
          <price>1</price>
          <shipping_price>2.39</shipping_price>
          <offer_status>4</offer_status>
        </pricing>
      </pricing_product>
    </pricing_query_response>

.. _pricing_product:

pricing_product
===============

Attributes
----------

+------+---------------------+---------------------+----------+---------+
| Name | Description         | Type                | Required | Default |
+======+=====================+=====================+==========+=========+
| type | Product seller type | :ref:`pricing_type` | required |         |
+------+---------------------+---------------------+----------+---------+


Elements
--------

+-------------------+-------------------+--------------------------+-------------+-+
| Name              | Description       | Type                     | Occurrence  | |
+===================+===================+==========================+=============+=+
| product_reference | Product reference | :ref:`product_reference` | 1-1         | |
+-------------------+-------------------+--------------------------+-------------+-+
| product_name      | Product name      | :ref:`string255`         | 1-1         | |
+-------------------+-------------------+--------------------------+-------------+-+
| image_url         | Product image url | xs:string                | 1-1         | |
+-------------------+-------------------+--------------------------+-------------+-+
| pricing           | Pricings          | :ref:`pricing_items`     | 0-unbounded | |
+-------------------+-------------------+--------------------------+-------------+-+

.. _pricing_items:

pricing_items
=============

Attributes
----------

+------+---------------------+---------------------+----------+---------+
| Name | Description         | Type                | Required | Default |
+======+=====================+=====================+==========+=========+
| type | Product seller type | :ref:`pricing_type` | required |         |
+------+---------------------+---------------------+----------+---------+


Elements
--------

+----------------+----------------------------------------+-----------------------+------------+-+
| Name           | Description                            | Type                  | Occurrence | |
+================+========================================+=======================+============+=+
| evaluation     | Number of evaluation                   | xs:decimal            | 0-1        | |
+----------------+----------------------------------------+-----------------------+------------+-+
| nb_orders      | Number of orders                       | xs:nonNegativeInteger | 0-1        | |
+----------------+----------------------------------------+-----------------------+------------+-+
| seller         | Product seller name                    | :ref:`string255`      | 0-1        | |
+----------------+----------------------------------------+-----------------------+------------+-+
| seller_url     | Product seller url                     | xs:string             | 0-1        | |
+----------------+----------------------------------------+-----------------------+------------+-+
| price          | Product price for this seller          | xs:decimal            | 1-1        | |
+----------------+----------------------------------------+-----------------------+------------+-+
| shipping_price | Product shipping price for this seller | xs:decimal            | 1-1        | |
+----------------+----------------------------------------+-----------------------+------------+-+
| offer_status   | Product state type for this seller     | :ref:`product_state`  | 1-1        | |
+----------------+----------------------------------------+-----------------------+------------+-+

.. _pricing_type:

pricing_type
============

Defines the type of sellers

* Extends: xs:string
* Enumerate:
  
  * professional: Product sell by a professional
  * not professional: Product sell by a private seller
  * fnac: Product sell by fnac