.. contents::
   :depth: 3
   :backlinks: none

.. _offers_update_service:

Offers Update
^^^^^^^^^^^^^

Informations
============

Description
-----------

This service is used to manage your catalog. It allows you to add, update or delete offers.

Process
-------

#. A valid authentification token is needed.
#. The partner sends a list of offers to add, update or delete.
#. The service returns a batch identifier. With that id you can retrieve the process report of the sent feed by using :ref:`batch_status_service`

Limitation
----------

The maximum recommended number of offers to update at once is 10 000.
The service doesn't provide a "purge and replace" mode. To delete offers, you have to use the <treatment> element on each offer to remove.

Request
=======

URL to use
----------

* Staging: https://partners-test.mp.fnacdarty.com/api.php/offers_update
* Production: https://vendeur.fnac.com/api.php/offers_update

Root element
------------

* Name: offers_update
* Extends: :ref:`base_request`

Elements
--------

+-------+------------------+----------------------+-------------+-+
| Name  | Description      | Type                 | Occurrence  | |
+=======+==================+======================+=============+=+
| offer | Offers to update | :ref:`offers_update` | 1-unbounded | |
+-------+------------------+----------------------+-------------+-+

XML Sample
----------

.. code-block:: xml
   
   <?xml version="1.0" encoding="utf-8"?>
   <offers_update xmlns="http://www.fnac.com/schemas/mp-dialog.xsd" shop_id="12345678-4AC1-6B59-1AB5-98462FB4B3B1" partner_id="12345678-945A-527F-3D1C-3C1D353A49D5" token="12345678-1E88-2A96-7BAE-398CEA69FB7A">
     <offer>
       <!-- Add/update an offer and place it in first position on the showcase page -->
       <product_reference type="Ean">7321950286485</product_reference>
       <offer_reference type="SellerSku"><![CDATA[MYSKU_1]]></offer_reference>
       <price>10.5</price>
       <product_state>11</product_state>
       <quantity>10</quantity>
       <description_fr><![CDATA[A description in French of the product]]></description_fr>
       <description_nl><![CDATA[A description in Dutch of the product]]></description_nl>
       <showcase>1</showcase>
       <deee_tax>0.04</deee_tax>
     </offer>
     <offer>
       <product_reference type="Ean">7321950286485</product_reference>
       <offer_reference type="SellerSku"><![CDATA[MYSKU_2]]></offer_reference>
       <price>10.5</price>
       <product_state>2</product_state>
       <quantity>10</quantity>
       <description_fr><![CDATA[A description in French of the product]]></description_fr>
       <description_nl><![CDATA[A description in Dutch of the product]]></description_nl>
       <dea_tax>0.05</dea_tax>
     </offer>
     <offer>
       <!-- Adding an "adherent price" to an offer -->
       <product_reference type="Ean">0886971942323</product_reference>
       <offer_reference type="SellerSku"><![CDATA[MYSKU_3]]></offer_reference>
       <price>10.5</price>
       <adherent_price>9.5</adherent_price>
       <product_state>11</product_state>
       <quantity>10</quantity>
       <description_fr><![CDATA[A description in French of the product]]></description_fr>
       <description_nl><![CDATA[A description in Dutch of the product]]></description_nl>
     </offer>
     <offer>
       <!-- Deleting an offer -->
       <offer_reference type="SellerSku"><![CDATA[MYSKU_TO_DELETE]]></offer_reference>
       <treatment>delete</treatment>
     </offer>
   </offers_update>

.. note::
   
   You can create several offers on the same product on condition that each offer has a different SKU number and different product state from the others.
   If an offer is sent with the same information as the existing ones, the offer is not processed and is marked as "Skipped" in the processing report.

Response
========

Root element
------------

* Name: offers_update_response
* Extends: :ref:`base_response`


Elements
--------

+----------+-----------------------------------------------+--------------+-------------+-+
| Name     | Description                                   | Type         | Occurrence  | |
+==========+===============================================+==============+=============+=+
| batch_id | Batch unique identifier which executes update | :ref:`uuid`  | 0-1         | |
+----------+-----------------------------------------------+--------------+-------------+-+
| error    | Errors                                        | :ref:`error` | 0-unbounded | |
+----------+-----------------------------------------------+--------------+-------------+-+

XML Sample
----------

.. code-block:: xml
   
   <?xml version="1.0" encoding="utf-8"?>
   <offers_update_response xmlns="http://www.fnac.com/schemas/mp-dialog.xsd" status="OK">
       <batch_id>73169053-DD44-82E4-D3E7-E298D1F27B13</batch_id>                  
   </offers_update_response>

.. _offers_update:

offers_update
=============


Elements
--------

+-------------------+-------------------------------------------------+--------------------------+------------+-+
| Name              | Description                                     | Type                     | Occurrence | |
+===================+=================================================+==========================+============+=+
| product_reference | Product reference                               | :ref:`product_reference` | 0-1        | |
+-------------------+-------------------------------------------------+--------------------------+------------+-+
| offer_reference   | Offer reference                                 | :ref:`offer_reference`   | 1-1        | |
+-------------------+-------------------------------------------------+--------------------------+------------+-+
| price             | Offer price                                     | :ref:`price_090_20000`   | 0-1        | |
+-------------------+-------------------------------------------------+--------------------------+------------+-+
| adherent_price    | Fnac member (adherent) price (see notice below) | :ref:`price_090_20000`   | 0-1        | |
+-------------------+-------------------------------------------------+--------------------------+------------+-+
| product_state     | Product state of offer                          | :ref:`product_state`     | 0-1        | |
+-------------------+-------------------------------------------------+--------------------------+------------+-+
| quantity          | Offer quantity                                  | :ref:`quantity_0_9999`   | 0-1        | |
+-------------------+-------------------------------------------------+--------------------------+------------+-+
| description_fr    | Offer description in French (FR and BE Market)  | :ref:`string255`         | 0-1        | |
+-------------------+-------------------------------------------------+--------------------------+------------+-+
| description_nl    | Offer description in Dutch (BE Market)          | :ref:`string255`         | 0-1        | |
+-------------------+-------------------------------------------------+--------------------------+------------+-+
| description_pt    | Offer description in Portuguese (PT Market)     | :ref:`string255`         | 0-1        | |
+-------------------+-------------------------------------------------+--------------------------+------------+-+
| description_es    | Offer description in Spanish (ES Market)        | :ref:`string255`         | 0-1        | |
+-------------------+-------------------------------------------------+--------------------------+------------+-+
| internal_comment  | Offer internal comment for personal use         | xs:string255             | 0-1        | |
+-------------------+-------------------------------------------------+--------------------------+------------+-+
| showcase          | Offer position in shop's showcase               | xs:nonNegativeInteger    | 0-1        | |
+-------------------+-------------------------------------------------+--------------------------+------------+-+
| treatment         | Treatment to do on offer                        | :ref:`offer_treatment`   | 0-1        | |
+-------------------+-------------------------------------------------+--------------------------+------------+-+
| pictures          | Add pictures to offer                           | :ref:`offer_pictures`    | 0-1        | |
+-------------------+-------------------------------------------------+--------------------------+------------+-+
| logistic_type_id  | Logistic type associated to the offer           | :ref:`logistic_type_id`  | 0-1        | |
+-------------------+-------------------------------------------------+--------------------------+------------+-+
| is_shipping_free  | Is shipping free for this offer ?               | xs:boolean               | 0-1        | |
+-------------------+-------------------------------------------------+--------------------------+------------+-+
| promotion         | Add a promotion to offer                        | :ref:`promotion`         | 0-1        | |
+-------------------+-------------------------------------------------+--------------------------+------------+-+
| time_to_ship      | Add a specific time to ship (if is activate)    | xs:positiveInteger       | 0-1        | |
+-------------------+-------------------------------------------------+--------------------------+------------+-+
| deee_tax          | DEEE amount (already included in offer price)   | xs:decimal               | 0-1        | |
+-------------------+-------------------------------------------------+--------------------------+------------+-+
| dea_tax           | DEA amount (already included in offer price)    | xs:decimal               | 0-1        | |
+-------------------+-------------------------------------------------+--------------------------+------------+-+
| private_copy_tax  | PCT amount (already included in offer price)    | xs:decimal               | 0-1        | |
+-------------------+-------------------------------------------------+--------------------------+------------+-+


.. note::
  
  Notice on adherent_price element
  
  This price is available for qualified sellers only. This option allows the seller to suggest a special price for customers who own the Fnac member card.
  To remove the adherent price from an offer, simply remove the <adherent_price> element from the feed on selected offers. In the example below, we're removing the adherent price for the offer "MYSKU_3" from the example above.
  If the adherent price equals the normal price, the adherent price is not set, and the existing adherent price is erased.
  Please contact our commercial team for further information about this option eligibility.

.. code-block:: xml
   
   <?xml version="1.0" encoding="utf-8"?>
   <offers_update xmlns="http://www.fnac.com/schemas/mp-dialog.xsd" shop_id="12345678-4AC1-6B59-1AB5-98462FB4B3B1" partner_id="12345678-945A-527F-3D1C-3C1D353A49D5" token="12345678-1E88-2A96-7BAE-398CEA69FB7A">
     <offer>
       <product_reference type="Ean">0886971942323</product_reference>
       <offer_reference type="SellerSku"><![CDATA[MYSKU_3]]></offer_reference>
       <price>10.5</price>
       <product_state>11</product_state>
       <quantity>10</quantity>
       <description_fr><![CDATA[A description in French of the product]]></description_fr>
       <description_nl><![CDATA[A description in Dutch of the product]]></description_nl>
     </offer>
   </offers_update>

.. note::
  
  Notice on pictures
  
  The pictures upload is available for qualified sellers only. This option allows the seller to add pictures to illustrate an offer.
  To remove the pictures from an offer, simply remove the <pictures> element from the feed on selected offers. If you want to replace a picture, you just need to replace the old picture url by the new one in your feed. 

.. code-block:: xml
   
   <?xml version="1.0" encoding="utf-8"?>
   <offers_update xmlns="http://www.fnac.com/schemas/mp-dialog.xsd" shop_id="12345678-4AC1-6B59-1AB5-98462FB4B3B1" partner_id="12345678-945A-527F-3D1C-3C1D353A49D5" token="12345678-1E88-2A96-7BAE-398CEA69FB7A">
     <offer>
       <product_reference type="Ean">0886971942323</product_reference>
       <offer_reference type="SellerSku"><![CDATA[MYSKU_3]]></offer_reference>
       <price>10.5</price>
       <product_state>11</product_state>
       <quantity>10</quantity>
       <description_fr><![CDATA[A description in French of the product]]></description_fr>
       <description_nl><![CDATA[A description in Dutch of the product]]></description_nl>
       <pictures>
         <picture_1><![CDATA[http://url.to.picture.com/picture1.jpg]]></picture_1>
	 <picture_2><![CDATA[http://url.to.picture.com/picture2.jpg]]></picture_2>
	 <picture_3><![CDATA[http://url.to.picture.com/picture3.jpg]]></picture_3>
	 <picture_4><![CDATA[http://url.to.picture.com/picture4.jpg]]></picture_4>
       </pictures>
     </offer>
   </offers_update>

.. note::
  
  Notice on logistic types
  
  An item (or product) is associated to a "logistic type" which defines the shipping costs associated to that item. The shipping costs can be set according to the size of the weight of the product.
  As shipping costs can be customized from the Fnac seller account, they can be overriden with a specific logistic type. It allows flexibility on the shipping costs management, especially on large or heavy products.
  To reset the logistic type of a product, send "RESET" keyword instead of value in this field.

.. code-block:: xml
   
    // Associate a product to logistic type G
    <?xml version="1.0" encoding="utf-8"?>
    <offers_update xmlns="http://www.fnac.com/schemas/mp-dialog.xsd" shop_id="12345678-4AC1-6B59-1AB5-98462FB4B3B1" partner_id="12345678-945A-527F-3D1C-3C1D353A49D5" token="12345678-1E88-2A96-7BAE-398CEA69FB7A">
      <offer>
        <product_reference type="Ean">8007842766653</product_reference>
        <offer_reference type="SellerSku"><![CDATA[MYSKU_TYPE_G]]></offer_reference>
        <price>150</price>
        <product_state>11</product_state>
        <quantity>10</quantity>
        <description_fr><![CDATA[A description in French of the product]]></description_fr>
        <description_nl><![CDATA[A description in Dutch of the product]]></description_nl>
        <logistic_type_id>107</logistic_type_id>
      </offer>

      // Reset offer to default logistic_type
      <offer>
        <product_reference type="Ean">8007842766653</product_reference>
        <offer_reference type="SellerSku"><![CDATA[MYSKU_DEFAULT_TYPE]]></offer_reference>
        <price>150</price>
        <product_state>11</product_state>
        <quantity>10</quantity>
        <description_fr><![CDATA[A description in French of the product]]></description_fr>
        <description_nl><![CDATA[A description in Dutch of the product]]></description_nl>
        <logistic_type_id>RESET</logistic_type_id>
      </offer>

   </offers_update>

.. note::

  Notice on promotion

  There are different types of promotion resulting in different visuals on the website. Also, those different types do not all use the same elements or functionalities.
  
  In the example below the first offer will have a Destocking promotion which will apply a 10% discount on the offer if the client has at least €200 worth of your offers in his cart and gives the code RENTREE2017.
  
  The other offer has a FlashSale promotion which will apply a 50% discount on the offer for all clients who add this offer to their cart.
  To remove the promotion from the offer, simply update the offer without the <promotion> tag.

.. code-block:: xml

    <offers_update xmlns="http://www.fnac.com/schemas/mp-dialog.xsd" shop_id="12345678-4AC1-6B59-1AB5-98462FB4B3B1" partner_id="12345678-945A-527F-3D1C-3C1D353A49D5" token="12345678-1E88-2A96-7BAE-398CEA69FB7A">
      
      <offer>
        <product_reference type="Ean">8007842766653</product_reference>
        <offer_reference type="SellerSku"><![CDATA[SKU_FOR_PROMO_D]]></offer_reference>
        <price>150</price>
        <product_state>11</product_state>
        <quantity>10</quantity>
        <description_fr><![CDATA[A description in French of the product]]></description_fr>
        <description_nl><![CDATA[A description in Dutch of the product]]></description_nl>
        <!-- Set Destocking promotion type -->
        <promotion type="Destocking">
          <promotion_uid><![CDATA[DESTOCKPROMO]]></promotion_uid>
          <starts_at>2017-01-01T09:00:00+02:00</starts_at>
          <ends_at>2017-01-10T23:59:00+02:00</ends_at>
          <!-- Apply a -10% discount -->
          <discount_type><![CDATA[percentage]]></discount_type> 
          <discount_value>10</discount_value>
          <triggers>
              <!-- Promotion triggered on a €200+ order amount and with code RENTREE2017 entered -->
              <trigger_cart type="MinSellerAmount">200</trigger_cart>
              <trigger_promotion_code><![CDATA[RENTREE2017]]></trigger_promotion_code>
              <trigger_customer_type><![CDATA[all]]></trigger_customer_type>
          </triggers>
        </promotion>
      </offer>

      <offer>
        <product_reference type="Ean">8007842766653</product_reference>
        <offer_reference type="SellerSku"><![CDATA[SKU_FOR_PROMO_FS]]></offer_reference>
        <price>150</price>
        <product_state>11</product_state>
        <quantity>10</quantity>
        <description_fr><![CDATA[A description in French of the product]]></description_fr>
        <description_nl><![CDATA[A description in Dutch of the product]]></description_nl>
        <!-- Set FlashSale promotion type with a -50% discount during a given period -->
        <promotion type="FlashSale">
          <promotion_uid><![CDATA[FLASHPROMO]]></promotion_uid>
          <starts_at>2017-01-01T09:00:00+02:00</starts_at>
          <ends_at>2017-01-10T23:59:00+02:00</ends_at>
          <discount_type><![CDATA[percentage]]></discount_type>
          <discount_value>50</discount_value>
        </promotion>
      </offer>

      <offer>
        <product_reference type="Ean">8007842766653</product_reference>
        <offer_reference type="SellerSku"><![CDATA[SKU_FOR_FNAC_SALE]]></offer_reference>
        <price>150</price>
        <product_state>11</product_state>
        <quantity>10</quantity>
        <description_fr><![CDATA[A description in French of the product]]></description_fr>
        <description_nl><![CDATA[A description in Dutch of the product]]></description_nl>
        <!-- Set Sales promotion type with a -40% discount associated to Fnac.com sales -->
        <promotion type="Sales">
          <sales_period_reference><![CDATA[SUMMER_2017]]></sales_period_reference>
          <promotion_uid><![CDATA[FNACSUMMER2017]]></promotion_uid>
          <starts_at>2017-07-01T09:00:00+02:00</starts_at>
          <ends_at>2017-08-10T23:59:00+02:00</ends_at>
          <discount_type><![CDATA[percentage]]></discount_type>
          <discount_value>40</discount_value>
        </promotion>
      </offer>
    </offers_update>

.. note::

  Notice on time_to_ship

  The time to ship is the duration between the day of the order confirmation and the delivery of the package to the carrier. You can set one value per offer.
  This value will be added to delivery times to calculate the estimated delivery date.

.. code-block:: xml

   <?xml version="1.0" encoding="utf-8"?>
   <offers_update xmlns="http://www.fnac.com/schemas/mp-dialog.xsd" shop_id="12345678-4AC1-6B59-1AB5-98462FB4B3B1" partner_id="12345678-945A-527F-3D1C-3C1D353A49D5" token="12345678-1E88-2A96-7BAE-398CEA69FB7A">
     <offer>
       <product_reference type="Ean">0886971942323</product_reference>
       <offer_reference type="SellerSku"><![CDATA[MYSKU_3]]></offer_reference>
       <price>10.5</price>
       <product_state>11</product_state>
       <quantity>10</quantity>
       <description_fr><![CDATA[A description in French of the product]]></description_fr>
       <description_nl><![CDATA[A description in Dutch of the product]]></description_nl>
       <time_to_ship>25</time_to_ship>
     </offer>
   </offers_update>