.. _services:

Api documentation
=================

.. toctree::
   
   howtouse
   makingrequest

.. toctree::
   :maxdepth: 2

   tutorial


Services List
-------------

.. toctree::
   :glob:
   :titlesonly:
   
   api/authentification
   api/offerquery
   api/offerupdate
   api/batchquery
   api/batchstatus
   api/orderquery
   api/orderupdate
   api/carrierquery
   api/countriesquery
   api/clientordercommentquery
   api/clientordercommentupdate
   api/messagequery
   api/messageupdate
   api/incidentquery
   api/incidentupdate
   api/pricingquery
   api/pricingqueryV2
   api/pricingqueryV3
   api/shopinvoicesquery
   api/salesperiodquery
   
Types List
----------

.. toctree::
   :maxdepth: 2
   
   type/base
   type/common
   type/state
   
Appendices
----------

.. toctree::
   :maxdepth: 1
   
   errors/errors