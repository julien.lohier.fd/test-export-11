.. _test_environment_tools:

Test environment tools
======================

After having created offers on your account, you would want to test our orders managing services (orders_query, orders_update).
Your seller test account provides a special section which allows you to create as many orders as you wish, and any type of orders that you could have to handle (multiple items, large quantities, etc).

As you connect to your seller account, you will get this new section:

.. image:: devTools.png

"Création de commandes" - Create test orders
--------------------------------------------

.. image:: createOrders.png

By filling in this form, you will generate orders with products picked out randomly from your catalog.
You can set parameters to describe the way they have to be created.

* **"Nombre de commandes à générer":** number of orders to create. You can create up to 100 orders at once.
* **"Nombre minimal de produits/Commandes":** min. number of items per order. 
* **"Nombre maximal de produits/Commandes":** max. number of items per order.
* **"Quantité minimale/produit":** min. ordered quantity per item . 
* **"Quantité maximale/produit":** max. ordered quantity per item.


"Passer les commandes au statut "A expédier"" - Update orders to ToShip status
------------------------------------------------------------------------------

After having generated those orders, you will have to mark them as accepted (or refused) from your system. Then, the orders have to be set on ToShip status.
As a reminder, on the live environment, orders are automatically updated to the ToShip status once the billing process is done. It is not the case on the test environment, but you can do that manually by clicking this link.
You will then get the message below, and receive an email confirming that your orders have been updated.

.. image:: updateToShip.png


"Passer les commandes au statut "Reçue"" - Update orders to Received status
---------------------------------------------------------------------------

When you will have marked your orders as Shipped, customers will have to confirm them as Received. You can simulate that update by clicking this link.
Like the previous link, you will get a confirmation message, and an email will be sent to you.

.. image:: updateReceived.png