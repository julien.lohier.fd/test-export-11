var current_version = '2.4';

// Retrieved called version from URL
var pathname = window.location.pathname.substring(1).split('/');
var called_version = pathname[2];

// Loading redirecting header accordingly to the called version
if(current_version != called_version)
{
  $(document).ready(function() {

    var path = window.location.href.replace(called_version, current_version);
    
    path = path.replace("standard", "full");

    jQuery('<div/>', {
      id: 'redirect',
      text: 'This version of the documentation (' + called_version  + ') is out of date. Please find the last version here.',
      href: path
    }).prependTo('body');

    $('#redirect').click(function() {
      window.location.href = path;
    });
  });
}
