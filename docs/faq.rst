.. contents::
   :depth: 2
   :backlinks: none

FAQ
===

1. I cannot connect to the test platform
----------------------------------------

Here are checks to do when connection is impossible:

Global network related problems:

* Are you using the correct url ?: https://vendeur.fnac.com/api.php (for live environment) and/or https://partners-test.mp.fnacdarty.com/api.php (for staging environment)
	  	  
API calls related problems:

* Check your authentication details (shop_id, partner_id, key)
* Which HTTP method do you use ? Each call should use POST method.

2. I keep getting error 400 "bad request"
-----------------------------------------

Your xml file may not be valid against our XSD files.
    
3. I don't have any response or I have a request timeout when I use a query service (OffersQuery, OrdersQuery, etc...).
------------------------------------------------------------------------------------------------------------------------

Parameters for request are not accurate enough so loading time is higher than timeout delay. Use more specific parameters
like paging or dates on the request and increase your timeout delay.
      
4. I have added offers but I can't see them
-------------------------------------------

First import can sometimes be long depending on server load. Call batch query service to check your pending import.
If import is finished and if you have the same problem, then check the result with batch status service. There may be some
errors on offers. Most of the time these errors are:

* An unknown product reference
* A price lower than 0.90 €
* Wrong quantity, negative or superior to 9999
        
5. I want to sell products which are not present in Fnac catalog. What can I do?
--------------------------------------------------------------------------------

The API doesn't provide products creation.
Please contact our seller support, they will give you instructions on how to do this (info.mp@fnac.com).
      
6. What is the difference between product_reference and offer_reference in OffersUpdate service ?
-------------------------------------------------------------------------------------------------

Product reference is your way to identify a product, EAN barcode in most cases.
Offer reference is your way to identify an offer, your SKU in most cases.
    
7. What are the different states of an order?
---------------------------------------------

First you need to distinguish order state and order detail state.
Order states :

* Created: Someone placed an order to you
* Accepted: One of the order details has been accepted.
* Refused: All order details have been refused.
* ToShip: Client paid the order and you can now ship the product.
* Shipped: Order has been sent to client.
* NotReceived: Client didn't received the order and an incident has been created.
* Received: Client confirmed that he has received the order.
* Refunded: Order has been refunded.
* Cancelled: Order has been cancelled.

Remember you can only set orders to the Shipped state when it's in the ToShip state.
Client invoice and shipping addresses become available once the order has been paid (all statuses after ToShip).

Order Detail states :

* ToAccept: The order detail in order need to be accepted or refused by seller.
* Accepted
* Refused
* Shipped
* Updated
      
8. I have an order with 2 offers, can I accept one and wait to accept or refuse the other one?
-----------------------------------------------------------------------------------------------

No, you must specify if you accept or refuse for all offers in an order at once.
      
9. What does the error message "No matching global declaration available for the validation root" mean?
--------------------------------------------------------------------------------------------------------

This error occurs when you send a request feed to a wrong service URL. For example, you would get that error if you send an 'offers_update' file to the 'orders_update' service URL.
 
10. Why do I get a "Service not found" error?
---------------------------------------------

First reason: you may probably have a wrong URL set up. When calling a service, check if your URL does not contain a double slashes in it (for example: https://vendeur.fnac.com/api.php//batch_status).
Second reason: the service you are calling is not enabled on your account. Some services need to be activated from Fnac's end to work (as the different versions of pricing_query service). In this case, please contact us at dl-fd-api.mp@fnac.com.