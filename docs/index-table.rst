Book
====

.. _php_api_client_guide:

PHP API Client Guide
--------------------

.. toctree::
   :glob:
   :maxdepth: 2
   
   reference/guide/intro
   reference/guide/firststep
   reference/cookbook/carrier
   reference/cookbook/comment
   reference/cookbook/incident
   reference/cookbook/message
   reference/cookbook/offer
   reference/cookbook/order
   reference/cookbook/pricing
   
PHP API Documentation
---------------------

`PHP API Documentation <apiclient/index.html>`_
