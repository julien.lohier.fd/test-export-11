:orphan:

Changelog
=========
   2020-11-04 (v2.6)
        * PHP Client > Not supported anymore and removed from the documentation
        * Offers Update > Add localized comments on offers
        * Offers Query > Add localized comments on offers
        * Orders Query > Add type_label field to retrieve category of sold products

   2017-08-30 (v2.6)
        * PHP Client > Vendor packages updated, use Serializer object instead of Normalizer
        * PHP Client > Added methods to support promotion setting on offers_update service

   2017-04-12 (v2.6)
        * Introducing promotion system in Offers Query, Offers update and Orders query services
        * Introducing Sales Period Query, related to promotion system
        * PHP Client > Added PricesQuery service (Pricing V3) support methods
        * PHP Client > Added with_fees attribute support on OffersQuery service

   2017-04-12 (v2.5.3)
	* PHP Client > Fixed calling URLs double-slash bug by automatically removing extra slash in configured URL

   2017-04-12 (v2.5.3)
	* Orders Update > Made <tracking_company> value mandatory when <tracking_number> field is filled in when shipping an order

   2017-03-30 (v2.5.2)
	* Shop Invoices Query > Added new fields in response, giving all relevant amounts related to an invoice

   2016-10-20 (v2.5.1)
        * Offers Query > Added <fee_excluding_taxes> and <fee_including_all_taxes> optional fields to retrieve calculated fees amounts on offers
	* Orders Query > Added <nif> field needed by sellers located in Spain and Portugal

   2016-05-18 (v2.5)
        * Offers Update > Added logistic types updating support, allowing shipping costs customization

   2014-11-19 (v2.4)
        * Shop Invoices Query > Added new service, allowing sellers to retrieve their invoices
        * PHP Client > Pricing API V1/V2 auto handling
        * PHP Client GUI > Interface now based on Bootstrap
        * XSD > Added new shipping modes ids

   2014-10-22 (v2.3.1)
        * PHP Client > Pricing API V1/V2 auto handling
        * XSD > Added new shipping modes ids

   2014-09-10 (v2.3.1)
        * PHP Client > Fixed Pricing API response handling

   2014-06-04 (v2.3.1)
        * Messages Update > Added message creation feature. By then, sellers could only reply to messages
        * Offers Update > Added linked images adding feature, sellers can link up to 4 images on an offer
        * XSD > Updated MessagesUpdate and OffersUpdate XSDs

   2014-01-14 (v2.3)
        * PHP Client > Strict PHP compliancy fix

   2013-11-25 (v2.3)
        * PHP Client GUI > Optimization on pager

   2013-09-22 (v2.3)
        * API Pricing V2 > Pricing API updated, former service is still available

   2013-09-13 (v2.2)
        * PHP Client > Requests now handled with HttpFoundation

   2013-09-10 (v2.2)
        * API Orders Query > Added <refused_at> tag on orders

   2013-09-06 (v2.2)
        * PHP Client > Implemented Incidents Query API handling
        * PHP Client GUI > Added alternative responsive design
        * PHP Client GUI > Optimized pager

   2013-08-09 (v2.2)
	* PHP Client > Fixed updateOffer method options handling
        * PHP Client GUI > Improved paging system on offers and orders views + Misc. CSS changes

   2013-08-07 (v2.2)
        * PHP Client > Fixed orders selection by orders' state on page changing
	* XSD > Fixed state label in Incidents Query XSD

   2013-07-19 (v2.2)
	* PHP Client > Fixed order details initializing on order_update call
        * XSD > Updated Incidents Query XSD
        