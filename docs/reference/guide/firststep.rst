Your first call
===============

We can now begin to use the library and retrieves information from the server.

Create a test script under your project root::

  /var/www/my_project/test.php
  
.. highlight:: php

First thing to do is to include the autoload script to load class on the fly::

  <?php
  require_once __DIR__.'/lib/vendor/FnacMarketplaceApiClient/autoload.php';

We want to use the SimpleClient and call OfferQuery service so we need to import these classes::
  
  <?php
  use FnacApiClient\Client\SimpleClient;
  use FnacApiClient\Service\Request\OfferQuery;
  
.. warning::

  This library use the concept of namespace introduced in php 5.3 if you are not aware with it we recommend you to read the documentation about it at http://www.php.net/manual/en/language.namespaces.rationale.php
  
Then you need to create a SimpleClient and give your configuration::
  
  <?php
  $myClient = new SimpleClient();
  $myClient->init(__DIR__.'/config/config.yml');
  
.. note::

  If you have your own configuration system you can also use other methods to configure you client::
  
    <?php
    //In the constructor
    $myClient = new SimpleClient($host, $shop_id, $partner_id, $key);
    
    //With an array in the init method
    $myClient->init(array(
      'host' => $host,
      'shop_id' => $shop_id,
      'partner_id' => $partner_id,
      'key' => $key
    ));
  
Then create OfferQuery service and configure it::
  
  <?php
  $offerQuery = new OfferQuery();
  //We get the first page
  $offerQuery->setPaging(1);
  //With 10 results per page
  $offerQuery->setResultsCount(10);
  
.. warning::

  As you can see we specify a number of results in our query, this is optional but we highly encouraged you to do it as this will certainly be mandatory in future releases of the api.
  
Once your service is created you just have to call it and use the answer:: 

  <?php
  $offerQueryResponse = $myClient->callService($offerQuery);
  
  //Print r the object to see what you get
  print_r($offerQueryResponse);
  

Here is the content of the final file

.. literalinclude:: ../examples/first_step.php

To see the result of the test, execute this command where your test.php is located::

  php test.php

This is as easy as that. SimpleClient will take care of all authentification process : you don't need to know the xml. You just have to create, configure and call services.

Next chapters of this guide will show some common use cases on different services that are available to you.