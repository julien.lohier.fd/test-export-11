Introduction
============

Requirements
------------

Client API of Fnac Marketplace requires at least PHP 5.3.2

The following librairies are also needed:

* php5-xsl

.. note::

  This library use existing components :
  
   * Symfony2 Components : ClassLoader, Finder, Serializer, Yaml http://www.symfony.com
   * Monolog : https://github.com/Seldaek/monolog
   * Zend Framework zf2 Components : Http, Uri, Validator https://github.com/zendframework/zf2

Download
--------

Download last version of or api client `here <../../download/fnacapiclient.tgz>`__.

You can also clone this client from GitHub with this command:

  git clone https://github.com/FnacMarketplace/fnacapiclient.git

Installation
------------

Extract the downloaded archive on your server. It is highly recommended to put this library in a "not public" directory. For example if your project is  at::

  /var/www/my_project
  
And your public html directory is::

  /var/www/my_project/public_html
  
A good practice is to put the library under the following directory::

  /var/www/my_project/lib/vendor/FnacMarketplaceApiClient
  
We will keep this directory as an example all through the guide.

Configuration
-------------

.. note::

  You can skip this part if you want to use the library in an advanced mode by using your own object to deal with xml streaming, log, etc... But if this is the first time you are using this library, we highly recommend you to follow our guide step by step.
  
Create a YAML config file in your config directory if you have one or in the library directory::

  /var/www/my_project/lib/vendor/FnacMarketplaceApiClient/config/config.yml
  
Put the following content inside and replace shop_id, partner_id and key values by the ones we provided you

.. highlight:: yaml
.. code-block:: yaml
  
  # /var/www/my_project/config/config.yml
  fnac_api_client:
      shop_id:      00000000-0000-0000-0000-000000000000
      partner_id:   00000000-0000-0000-0000-000000000000
      key:          00000000-0000-0000-0000-000000000000
      host:         https://partners-test.mp.fnacdarty.com/api.php

In all of our examples we will be using this path for our configuration::

  /var/www/my_project/config/config.yml

