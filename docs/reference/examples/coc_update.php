<?php
require_once __DIR__.'/lib/vendor/FnacMarketplaceApiClient/autoload.php';

use FnacApiClient\Client\SimpleClient;

use FnacApiClient\Service\Request\ClientOrderCommentUpdate;

use FnacApiClient\Entity\Comment;

use FnacApiClient\Type\ResponseStatusType;

use Monolog\Logger;
use Monolog\Handler\StreamHandler;

$myClient = new SimpleClient();
$myClient->init(__DIR__.'/config/config.yml');

$logger = new Logger('api_log');
$logger->pushHandler(new StreamHandler('php://stdout', Logger::WARNING));

$myClient->setLogger($logger);

//Comment id to update
$clientOrderComentId = "D3CD0824-6A32-1D1C-2397-8C130DB64DBE";
$clientOrderComment = new Comment();

//Reply something for this comment
$clientOrderComment->setId($clientOrderComentId);
$clientOrderComment->setReply("My reply to the comment");

//Create service
$clientOrderCommentUpdate = new ClientOrderCommentUpdate();

//Add comment to reply in update service (you can have more)
$clientOrderCommentUpdate->addComment($clientOrderComment);

//Call service
$commentUpdateResponse = $myClient->callService($clientOrderCommentUpdate);

//If status of response is ok all comment have been replied to
if ($commentUpdateResponse->getStatus() == ResponseStatusType::OK)
{
  //Everything is fine
  echo sprintf("All my reply have been set");
}
else
{
  //Otherwise something append show error
  foreach ($commentUpdateResponse->getComments() as $comment)
  {
    foreach($comment->getErrors() as $error)
    {
      echo sprintf("Error for comment %s with code = %s and message = %s \n", $comment->getId(), $error->getCode(), $error->getMessage());
    }
  }
}