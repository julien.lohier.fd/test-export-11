<?php
require_once __DIR__.'/lib/vendor/FnacMarketplaceApiClient/autoload.php';

use FnacApiClient\Client\SimpleClient;

use FnacApiClient\Service\Request\OrderQuery;
use FnacApiClient\Service\Request\OrderUpdate;

use FnacApiClient\Entity\OrderDetail;

use FnacApiClient\Type\OrderStateType;
use FnacApiClient\Type\OrderDetailActionType;
use FnacApiClient\Type\OrderActionType;

use Monolog\Logger;
use Monolog\Handler\StreamHandler;

$myClient = new SimpleClient();
$myClient->init(__DIR__.'/config/config.yml');

$logger = new Logger('api_log');
$logger->pushHandler(new StreamHandler('php://stdout', Logger::WARNING));

$myClient->setLogger($logger);

//Create order query service
$orderQuery = new OrderQuery();

//With 10 results per page
$orderQuery->setResultsCount(100);

//We want to retrieve all the shipped order
$orderQuery->setStates(array(
  OrderStateType::SHIPPED
));

$page = 1;

do {
  //Create the OrderUpdate
  $orderUpdateService = new OrderUpdate();
  
  //We get the current page
  $orderQuery->setPaging($page);
  
  //Call service
  $orderQueryResponse = $myClient->callService($orderQuery);
  
  //For each order
  $has_order = false;
  foreach($orderQueryResponse->getOrders() as $order)
  {
    //Depending on some condition
    if (true)
    {
      //We want to update each order detail 
      $order->setOrderAction(OrderActionType::UPDATE_ALL);
      
      //Clear the existing orders details
      $order->clearOrdersDetail();

      //Create a generic order detail to set the massive action
      $orderDetail = new OrderDetail();
      
      //We set the tracking number and company for each order detail
      $orderDetail->setAction(OrderDetailActionType::UPDATED);
      $orderDetail->setTrackingNumber('007');
      $orderDetail->setTrackingCompany('UPS');
      //Add this generic order detail to order
      $order->addOrderDetail($orderDetail);

      //We add this order to the order update
      $orderUpdateService->addOrder($order);
      
      $has_order = true;
    }
  }
  
  //If we have orders to update
  if ($has_order)
  {
    //We call the service to update orders
    $orderUpdateResponse = $myClient->callService($orderUpdateService);

    //For each order update we have done
    foreach($orderUpdateResponse->getOrdersUpdate() as $orderUpdate)
    {
      //If we get errors
      if ($orderUpdate->getErrors()->count() > 0)
      {
        //Do whatever you want when there are errors, here we simply log errors
        foreach($orderUpdate->getErrors() as $error)
        {
          $logger->addError(sprintf("An error occured updating Order #%s with code %s and message : %s", $orderUpdate->getOrderId(), $error->getCode(), $error->getMessage()));        
        }
      }
      else
      {
        //Status Ok Do whatever you want here
      }
    }
  }
  
  $page++;
} while($orderQueryResponse->hasNextPage());
