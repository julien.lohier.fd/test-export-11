<?php
require_once __DIR__.'/lib/vendor/FnacMarketplaceApiClient/autoload.php';

use FnacApiClient\Client\SimpleClient;

use FnacApiClient\Service\Request\OfferQuery;
use FnacApiClient\Service\Request\OfferUpdate;
use FnacApiClient\Service\Request\BatchStatus;

use FnacApiClient\Entity\Offer;

use FnacApiClient\Type\ResponseStatusType;
use FnacApiClient\Type\ProductType;
use FnacApiClient\Type\OfferReferenceType;
use FnacApiClient\Type\ProductStateType;

use Monolog\Logger;
use Monolog\Handler\StreamHandler;

$myClient = new SimpleClient();
$myClient->init(__DIR__.'/config/config.yml');

$logger = new Logger('api_log');
$logger->pushHandler(new StreamHandler('php://stdout', Logger::INFO));

$myClient->setLogger($logger);

//Sku : your unique identifier for each offer
//An offer is unique by the product, seller and state
$offer1Sku = "MYSKU_1";

/**
 * If your sku is not unique : Use Offer Fnac Id, this is faster and safier
 * When you create an offer, once the job is done you can save the offer fnac id we will return you in your system
 * Then use it
 */ 

//First we create the offers to update
$offer1 = new Offer();
$offer1->setOfferReferenceType(OfferReferenceType::SELLER_SKU);
$offer1->setOfferReference($offer1Sku);
//I want to show this offer beyond all other so this is first one in showcase
$offer1->setTreatment('delete');

//Create service
$offerUpdate = new OfferUpdate();

//Add offer to service
$offerUpdate->addOffer($offer1);

//Call the service
$offerUpdateResponse = $myClient->callService($offerUpdate);

/**
 * We get after the update a batch id which references a job which will process the update
 * 
 * This task can be long so best way it's to save batch id on your server, and 
 */  

//Get the batch identifier which will process the update
$batch_id = $offerUpdateResponse->getBatchId();

//Create batch status service
$batchStatus = new BatchStatus();

//Set the id we want to retrieve
$batchStatus->setBatchId($batch_id);

do {
  echo "Requesting batch status in 5 sec";
  
  sleep(5);
  $batchStatusResponse = $myClient->callService($batchStatus);
  
} while($batchStatusResponse->getStatus() != ResponseStatusType::OK && $batchStatusResponse->getStatus() != ResponseStatusType::ERROR && $batchStatusResponse->getStatus() != ResponseStatusType::WARNING);

foreach ($batchStatusResponse->getOffersUpdate() as $offerUpdate)
{
  if ($offerUpdate->getStatus() == ResponseStatusType::ERROR)
  {
    foreach($offerUpdate->getErrors() as $error)
    {
      $logger->addError(sprintf("An error occured updating Offer #%s with code %s and message : %s", $offerUpdate->getOfferFnacId(), $error->getCode(), $error->getMessage()));
    }
  }
  else
  {
    //Offer has been deleted
    echo "Offer delete with id = ".$offerUpdate->getOfferFnacId()."\n";
  }
}