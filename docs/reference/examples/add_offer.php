<?php
require_once __DIR__.'/lib/vendor/FnacMarketplaceApiClient/autoload.php';

use FnacApiClient\Client\SimpleClient;

use FnacApiClient\Service\Request\OfferQuery;
use FnacApiClient\Service\Request\OfferUpdate;
use FnacApiClient\Service\Request\BatchStatus;

use FnacApiClient\Entity\Offer;

use FnacApiClient\Type\ResponseStatusType;
use FnacApiClient\Type\ProductType;
use FnacApiClient\Type\OfferReferenceType;
use FnacApiClient\Type\ProductStateType;

use Monolog\Logger;
use Monolog\Handler\StreamHandler;

$myClient = new SimpleClient();
$myClient->init(__DIR__.'/config/config.yml');

$logger = new Logger('api_log');
$logger->pushHandler(new StreamHandler('php://stdout', Logger::INFO));

$myClient->setLogger($logger);

//Ean of product
$productAEan = "7321950286485";
$productBEan = "0886971942323";

//Sku : your unique identifier for each offer
//An offer is unique by the product, seller and state
$offer1Sku = "MYSKU_1";
$offer2Sku = "MYSKU_2";
$offer3Sku = "MYSKU_3";

//First we create the offers to add
$offer1 = new Offer();
$offer1->setProductReferenceType(ProductType::EAN);
$offer1->setProductReference($productAEan);
$offer1->setOfferReferenceType(OfferReferenceType::SELLER_SKU);
$offer1->setOfferReference($offer1Sku);
$offer1->setPrice(10.5);
$offer1->setProductState(ProductStateType::UNUSED);
$offer1->setQuantity(10);
$offer1->setDescription("My offer description");
$offer1->setInternalComment("A personal comment to stock everything you want");
//I want to show this offer beyond all other so this is first one in showcase
$offer1->setShowcase(1);


$offer2 = new Offer();
$offer2->setProductReferenceType(ProductType::EAN);
$offer2->setProductReference($productAEan);
$offer2->setOfferReferenceType(OfferReferenceType::SELLER_SKU);
$offer2->setOfferReference($offer2Sku);
$offer2->setPrice(10.5);
$offer2->setProductState(ProductStateType::USED_VERY_GOOD);
$offer2->setQuantity(10);
$offer2->setDescription("My offer description");
$offer2->setInternalComment("A personal comment to stock everything you want");


$offer3 = new Offer();
$offer3->setProductReferenceType(ProductType::EAN);
$offer3->setProductReference($productBEan);
$offer3->setOfferReferenceType(OfferReferenceType::SELLER_SKU);
$offer3->setOfferReference($offer3Sku);
$offer3->setPrice(10.5);
$offer3->setProductState(ProductStateType::UNUSED);
$offer3->setQuantity(10);
$offer3->setDescription("My offer description");
$offer3->setInternalComment("A personal comment to stock everything you want");

//Create service
$offerUpdate = new OfferUpdate();

//Add offer to service
$offerUpdate->addOffer($offer1);
$offerUpdate->addOffer($offer2);
$offerUpdate->addOffer($offer3);

//Call the service
$offerUpdateResponse = $myClient->callService($offerUpdate);

//Get the batch identifier related to the update
$batch_id = $offerUpdateResponse->getBatchId();

/**
 * Batch id references a job which will process the update
 * 
 * This task can be long, so the best way is to save batch id on your server, and call the following code later
 * 
 * However, we just add 3 offer here so this will be quick and we can wait a few minutes before the job ends
 */

//Create batch status service
$batchStatus = new BatchStatus();

//Set the id we want to retrieve
$batchStatus->setBatchId($batch_id);

do {
  echo "Requesting batch status in 5 sec";
  
  sleep(5);
  $batchStatusResponse = $myClient->callService($batchStatus);
  
} while($batchStatusResponse->getStatus() != ResponseStatusType::OK && $batchStatusResponse->getStatus() != ResponseStatusType::ERROR && $batchStatusResponse->getStatus() != ResponseStatusType::WARNING);

foreach ($batchStatusResponse->getOffersUpdate() as $offerUpdate)
{
  if ($offerUpdate->getStatus() == ResponseStatusType::ERROR)
  {
    foreach($offerUpdate->getErrors() as $error)
    {
      $logger->addError(sprintf("An error occured updating Offer #%s with code %s and message : %s", $offerUpdate->getOfferFnacId(), $error->getCode(), $error->getMessage()));
    }
  }
  else
  {
    //Offer has been added
    echo "Offer add with id = ".$offerUpdate->getOfferFnacId()."\n";
  }
}