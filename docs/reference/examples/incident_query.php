<?php
require_once __DIR__.'/lib/vendor/FnacMarketplaceApiClient/autoload.php';

use FnacApiClient\Client\SimpleClient;
use FnacApiClient\Service\Request\IncidentQuery;

use FnacApiClient\Type\StatusType;

use Monolog\Logger;
use Monolog\Handler\StreamHandler;

// Create the client
$myClient = new SimpleClient();
$myClient->init(__DIR__.'/config/config.yml');

// Create a logger @see Monolog : https://github.com/Seldaek/monolog
$logger = new Logger('api_log');
$logger->pushHandler(new StreamHandler('php://stdout', Logger::INFO));
$myClient->setLogger($logger);

// Create the IncidentQuery service.
$serviceIncidentQuery = new IncidentQuery();

// We want only the first page
$serviceIncidentQuery->setPaging(1);

// We want only opened Incidents
$serviceIncidentQuery->setStatus(StatusType::OPENED);

// Call the service with the client.
$incidentQueryResponse = $myClient->callService($serviceIncidentQuery);

// for each incident , we print some informations 
foreach($incidentQueryResponse->getIncidents() as $incident)
{
	echo sprintf("Incident : %s  \n", $incident->getId());
	echo sprintf("With message %s \n", $incident->getMessage());
	echo sprintf("For the order %s  \n", $incident->getOrderId());
	echo sprintf("Open By %s , %s \n", $incident->getOpenedBy(), $incident->getCreatedAt());	
}
