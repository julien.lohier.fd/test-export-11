<?php
require_once __DIR__.'/lib/vendor/FnacMarketplaceApiClient/autoload.php';

use FnacApiClient\Client\SimpleClient;
use FnacApiClient\Service\Request\OfferQuery;

$myClient = new SimpleClient();
$myClient->init(__DIR__.'/config/config.yml');

$offerQuery = new OfferQuery();
//We get the first page
$offerQuery->setPaging(1);
//With 10 results per page
$offerQuery->setResultsCount(10);

$offerQueryResponse = $myClient->callService($offerQuery);

//Print_r the object to see what you get
print_r($offerQueryResponse);