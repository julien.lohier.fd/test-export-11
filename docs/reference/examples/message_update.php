<?php
require_once __DIR__.'/lib/vendor/FnacMarketplaceApiClient/autoload.php';

use FnacApiClient\Client\SimpleClient;

use FnacApiClient\Service\Request\MessageUpdate;

use FnacApiClient\Entity\Message;

use FnacApiClient\Type\MessageType;
use FnacApiClient\Type\MessageToType;
use FnacApiClient\Type\MessageSubjectType;
use FnacApiClient\Type\MessageActionType;
use FnacApiClient\Type\ResponseStatusType;

use Monolog\Logger;
use Monolog\Handler\StreamHandler;

$myClient = new SimpleClient();
$myClient->init(__DIR__.'/config/config.yml');

$logger = new Logger('api_log');
$logger->pushHandler(new StreamHandler('php://stdout', Logger::INFO));

$myClient->setLogger($logger);

//Message id
$messageId = "5CF06527-13D2-7C8D-A5C9-392B1458530C";

//Create a message to update
$message1 = new Message();
$message1->setMessageId($messageId);
//We want to mark him as read
$message1->setAction(MessageActionType::MARK_AS_READ);

//For second message we want to reply
$message2 = new Message();
$message2->setMessageId($messageId);
$message2->setAction(MessageActionType::REPLY);
//Reply to all 
$message2->setMessageTo(MessageToType::ALL);
//Set message subject
$message2->setMessageSubject(MessageSubjectType::PRODUCT_INFORMATION);
//It's a order message
$message2->setMessageType(MessageType::ORDER);
//My reply
$message2->setMessageDescription("My response to the message");

//For third message, we create a new message on an order
$message3 = new Message();
$message3->setMessageId("565AF66339934"); // specify an order id
$message3->setAction(MessageActionType::CREATE);
//Send to client only
$message3->setMessageTo(MessageToType::CLIENT);
//Set message subject
$message3->setMessageSubject(MessageSubjectType::SHIPPING_INFORMATION);
//It's an order message
$message3->setMessageType(MessageType::ORDER);
//My message
$message3->setMessageDescription("Hello. Thanks for your order. It will be shipped shortly.");


//Create service
$messageUpdate = new MessageUpdate();
$messageUpdate->addMessage($message1);
$messageUpdate->addMessage($message2);
$messageUpdate->addMessage($message3);

//Call service
$messageUpdateResponse = $myClient->callService($messageUpdate);

if ($messageUpdateResponse->getStatus() == ResponseStatusType::OK)
{
  echo sprintf("Update messages OK\n");
}
else
{
  foreach($messageUpdateResponse->getMessages() as $messageUpdate)
  {
    foreach($messageUpdate->getErrors() as $error)
    {
      echo sprintf("Error for message %s with code = %s and message = %s \n", $messageUpdate->getId(), $error->getCode(), $error->getMessage());
    }
  }
}