<?php
require_once __DIR__.'/lib/vendor/FnacMarketplaceApiClient/autoload.php';

use FnacApiClient\Client\SimpleClient;
use FnacApiClient\Service\Request\IncidentUpdate;
use FnacApiClient\Entity\IncidentOrder;
use FnacApiClient\Entity\IncidentOrderDetailUpdate;
use FnacApiClient\Entity\IncidentOrderDetail;
use FnacApiClient\Type\IncidentUpdateActionType;
use FnacApiClient\Type\IncidentRefundReasonType;
use FnacApiClient\Type\ResponseStatusType;

use Monolog\Logger;
use Monolog\Handler\StreamHandler;

// Create the client
$myClient = new SimpleClient();
$myClient->init(__DIR__.'/config/config.yml');

// Create a logger @see Monolog : https://github.com/Seldaek/monolog
$logger = new Logger('api_log');
$logger->pushHandler(new StreamHandler('php://stdout', Logger::INFO));
$myClient->setLogger($logger);

// Create the IncidentQuery service.
$serviceIncidentUpdate = new IncidentUpdate();

// Create the IncidentOrder -> Order  you want to refund.
// In real process , you get the info for the Incident and Order with the IncidentQuery service.
$orderIncident = new IncidentOrder();
// Set the unique identifier of the order (given by fnac)
$orderIncident->setOrderId('1PED8PGTQ26SC');
// Set the action to process.
$orderIncident->setAction(IncidentUpdateActionType::REFUND);

// Create IncidentOrderDetail -> order details to refund
$orderDetailIncident = new IncidentOrderDetail();
// Set the position of the order detail (id)
$orderDetailIncident->setOrderDetailId(1);

// Set the reason of the refund (@see IncidentRefundReasonType)
$orderDetailIncident->setRefundReason(IncidentRefundReasonType::OTHER);

// Other OrderDetails
$orderDetailIncident1 = new IncidentOrderDetail();
$orderDetailIncident1->setOrderDetailId(2);
$orderDetailIncident1->setRefundReason(IncidentRefundReasonType::NO_STOCK);

// Set the orderDetails to the order.
// 1) This one way to do it (@see further example below)
$orderIncident->addOrderDetail($orderDetailIncident);
$orderIncident->addOrderDetail($orderDetailIncident1);

// Other Order with OrderDetails
$orderIncident2 = new IncidentOrder();
$orderIncident2->setOrderId('1HSUUAITSXLUK');
$orderIncident2->setAction(IncidentUpdateActionType::REFUND);

$orderDetailIncident2 = new IncidentOrderDetail();
$orderDetailIncident2->setOrderDetailId(1);
$orderDetailIncident2->setRefundReason(IncidentRefundReasonType::OTHER);

$orderDetailIncident21 = new IncidentOrderDetail();
$orderDetailIncident21->setOrderDetailId(2);
$orderDetailIncident21->setRefundReason(IncidentRefundReasonType::OTHER);

$orderDetails = new \ArrayObject(array($orderDetailIncident2, $orderDetailIncident21));
// Set the orderDetails to the order.
// 2) other way to do it (@see further example above)
$orderIncident2->addOrderDetails($orderDetails);

// Set the order to the service
$serviceIncidentUpdate->addOrder($orderIncident);
$serviceIncidentUpdate->addOrder($orderIncident2);

/*
OR 
$orders = new ArrayObject(array($orderIncident, $orderIncident2));
$serviceIncidentUpdate->addOrders($orders);
*/

// Call the service with the client.
$incidentUpdateResponse = $myClient->callService($serviceIncidentUpdate);

// If the status of the service is OK
if ($incidentUpdateResponse->getStatus() == ResponseStatusType::OK)
{
	// For each order updated , we print the order's id and state
	foreach($incidentUpdateResponse->getOrdersUpdates() as $order)
	{
			echo sprintf("Update Incidents OK\n");
			echo sprintf("Order : %s -> %s \n", $order->getOrderId(), $order->getState());
	}  
}
else
{
	// for each error , we print the msessage and error code. 
  foreach($incidentUpdateResponse->getOrdersUpdates() as $orderUpdate)
  {
    foreach($orderUpdate->getErrors() as $error)
    {
      echo sprintf("Error for incident update : order %s with code = %s and message = %s \n", $orderUpdate->getOrderId(), $error->getCode(), $error->getMessage());
    }
  }
}
