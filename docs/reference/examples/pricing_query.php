<?php
require_once __DIR__.'/lib/vendor/FnacMarketplaceApiClient/autoload.php';

use FnacApiClient\Client\SimpleClient;

use FnacApiClient\Service\Request\PricingQuery;

use FnacApiClient\Entity\ProductReference;

use FnacApiClient\Type\ProductType;
use FnacApiClient\Type\SellerType;

use Monolog\Logger;
use Monolog\Handler\StreamHandler;

$myClient = new SimpleClient();
$myClient->init(__DIR__.'/config/config.yml');

$logger = new Logger('api_log');
$logger->pushHandler(new StreamHandler('php://stdout', Logger::WARNING));

$myClient->setLogger($logger);

//Create query
$pricingQuery = new PricingQuery();

//Set type of sellers to retrieve (all, fnac or pro)
$pricingQuery->setSellers(SellerType::ALL);

//Create a product reference
$productRef = new ProductReference();
$productRef->setType(ProductType::EAN);
$productRef->setValue("0886971942323");

//Add product reference to query (you can have more than one)
$pricingQuery->addProductReference($productRef);

//Call service
$pricingQueryResponse = $myClient->callService($pricingQuery);

//For each product reference, get pricing report
foreach($pricingQueryResponse->getPricingProducts() as $pricingProduct)
{
  //Do some logic to get the min price for a seller
  $minPrice = null;
  $minPricing = null;
  foreach($pricingProduct->getPricings() as $pricing)
  {   
    if ($minPrice === null || $pricing->getPrice() < $minPrice)
    {
      $minPrice = $pricing->getPrice();
      $minPricing = $pricing;
    }
  }
  
  echo sprintf("The product %s min price is %s sell by %s seller\n", $pricingProduct->getProductName(), $minPrice, $minPricing->getType());
}