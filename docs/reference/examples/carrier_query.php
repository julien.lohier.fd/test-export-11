<?php
require_once __DIR__.'/lib/vendor/FnacMarketplaceApiClient/autoload.php';

use FnacApiClient\Client\SimpleClient;

use FnacApiClient\Service\Request\CarrierQuery;

use FnacApiClient\Entity\ProductReference;

use FnacApiClient\Type\ProductType;
use FnacApiClient\Type\SellerType;

use Monolog\Logger;
use Monolog\Handler\StreamHandler;

$myClient = new SimpleClient();
$myClient->init(__DIR__.'/config/config.yml');

$logger = new Logger('api_log');
$logger->pushHandler(new StreamHandler('php://stdout', Logger::WARNING));

$myClient->setLogger($logger);

//Create query
$carrierQuery = new CarrierQuery();
//Call service
$carrierQueryResponse = $myClient->callService($carrierQuery);

//Get carriers
foreach($carrierQueryResponse->getCarriers() as $carrier)
{
  echo sprintf("Carrier %s available\n", $carrier->getName());
}