<?php
require_once __DIR__.'/lib/vendor/FnacMarketplaceApiClient/autoload.php';

use FnacApiClient\Client\SimpleClient;

use FnacApiClient\Service\Request\MessageQuery;

use Monolog\Logger;
use Monolog\Handler\StreamHandler;

$myClient = new SimpleClient();
$myClient->init(__DIR__.'/config/config.yml');

$logger = new Logger('api_log');
$logger->pushHandler(new StreamHandler('php://stdout', Logger::INFO));

$myClient->setLogger($logger);

//Create query
$messageQuery = new MessageQuery();

//Get first page
$messageQuery->setPaging(1);

//Get 100 results per page
$messageQuery->setResultsCount(100);

//Call service
$messageQueryResponse = $myClient->callService($messageQuery);

echo sprintf("You have %s unread message \n", $messageQueryResponse->getMessagesUnreadResult());