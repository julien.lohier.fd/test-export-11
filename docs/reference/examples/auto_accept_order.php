<?php
require_once __DIR__.'/lib/vendor/FnacMarketplaceApiClient/autoload.php';

use FnacApiClient\Client\SimpleClient;

use FnacApiClient\Service\Request\OrderQuery;
use FnacApiClient\Service\Request\OrderUpdate;

use FnacApiClient\Entity\OrderDetail;

use FnacApiClient\Type\OrderStateType;
use FnacApiClient\Type\OrderDetailActionType;
use FnacApiClient\Type\OrderActionType;

use Monolog\Logger;
use Monolog\Handler\StreamHandler;

$myClient = new SimpleClient();
$myClient->init(__DIR__.'/config/config.yml');

$logger = new Logger('api_log');
$logger->pushHandler(new StreamHandler('php://stdout', Logger::WARNING));

$myClient->setLogger($logger);

//Create order query service
$orderQuery = new OrderQuery();

//With 10 results per page
$orderQuery->setResultsCount(100);

//We want to retrieve all the created order (the others are already accepted)
$orderQuery->setStates(array(
  OrderStateType::CREATED
));

$page = 1;

do {
  //Create the OrderUpdate
  $orderUpdateService = new OrderUpdate();
  
  //We get the current page
  $orderQuery->setPaging($page);
  
  //Call service
  $orderQueryResponse = $myClient->callService($orderQuery);
  
  //For each order
  $has_order = false;
  foreach($orderQueryResponse->getOrders() as $order)
  {
    //We want to accept every order details
    $order->setOrderAction(OrderActionType::ACCEPT_ALL_ORDERS);
    
    //We add this order to the order update
    $orderUpdateService->addOrder($order);
    
    $has_order = true;
  }
  
  //If we have orders to update
  if ($has_order)
  {
    //We call the service to update order
    $orderUpdateResponse = $myClient->callService($orderUpdateService);

    //For each order update we have done
    foreach($orderUpdateResponse->getOrdersUpdate() as $orderUpdate)
    {
      //If we get error
      if ($orderUpdate->getErrors()->count() > 0)
      {
        //Do whatever you want when there are error, here we simply log errors
        foreach($orderUpdate->getErrors() as $error)
        {
          $logger->addError(sprintf("An error occured updating Order #%s with code %s and message : %s", $orderUpdate->getOrderId(), $error->getCode(), $error->getMessage()));        
        }
      }
      else
      {
        //Status Ok Do what ever you want here
      }
    }
  }
  
  $page++;
} while($orderQueryResponse->hasNextPage());



