<?php
require_once __DIR__.'/lib/vendor/FnacMarketplaceApiClient/autoload.php';

use FnacApiClient\Client\SimpleClient;

use FnacApiClient\Service\Request\ClientOrderCommentQuery;

use Monolog\Logger;
use Monolog\Handler\StreamHandler;

$myClient = new SimpleClient();
$myClient->init(__DIR__.'/config/config.yml');

$logger = new Logger('api_log');
$logger->pushHandler(new StreamHandler('php://stdout', Logger::WARNING));

$myClient->setLogger($logger);

//Create query service
$clientOrderCommentQuery = new ClientOrderCommentQuery();
//Get first page
$clientOrderCommentQuery->setPaging(1);
//100 Results per page
$clientOrderCommentQuery->setResultsCount(100);

//Call service
$clientOrderCommentQueryResponse = $myClient->callService($clientOrderCommentQuery);

//Get client order coment
foreach ($clientOrderCommentQueryResponse->getClientOrderComments() as $clientOrderComment)
{
  //Save in system
  echo sprintf("Save client order comment #%s in my system", $clientOrderComment->getClientOrderCommentId())."\n";
}
