Order
=============

Automatically accept order
--------------------------

Use case :

* You want to get all newly created orders
* Depending on some conditions, you want to accept or refuse order details inside orders

.. highlight:: php
.. literalinclude:: ../examples/auto_accept_order.php

Massive accept
--------------

Use case :

* You want to get all the newly created orders
* Depending on some conditions you want to accept or refuse all order details inside orders

.. literalinclude:: ../examples/massive_accept_order.php

Massive confirm send
---------------------

Use case :

* You want to get all orders that are debited and ready to be shipped
* Depending on some conditions you want to send all order details inside orders

.. literalinclude:: ../examples/massive_confirm_send_order.php

Massive update
----------------------------------------

Use case :

* You have some orders that are shipped
* Depending on some conditions you want to change the tracking number and / or company

.. literalinclude:: ../examples/massive_update_order.php
