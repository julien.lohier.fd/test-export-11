Message 
===============

Query
-----

.. highlight:: php
.. literalinclude:: ../examples/message_query.php

Update
------


.. literalinclude:: ../examples/message_update.php