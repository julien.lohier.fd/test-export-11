Offer
=============

Add offer with EAN
------------------

In this example you have 3 offers to add :

* product A with a UNUSED state
* product A with a GOOD_SHAPE state
* product B with a UNUSED state

.. highlight:: php
.. literalinclude:: ../examples/add_offer.php

Update offer with SKU
---------------------

Use case 

* Update product A with a UNUSED state by setting a new quantity

.. literalinclude:: ../examples/update_offer.php

Delete offer
------------

* Delete my product A with a UNUSED state

.. literalinclude:: ../examples/delete_offer.php

