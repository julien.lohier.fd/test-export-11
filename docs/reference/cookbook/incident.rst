Incident 
================

Query
-----

Use case :

* You want to get the first page of opened incidents.

.. highlight:: php
.. literalinclude:: ../examples/incident_query.php

Update
------

Use case :

* You want to update two incidents refunding order_details

.. highlight:: php
.. literalinclude:: ../examples/incident_update.php
