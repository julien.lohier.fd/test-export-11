Client order comment
==========================

Query
-----

.. highlight:: php
.. literalinclude:: ../examples/coc_query.php

Update
------


.. literalinclude:: ../examples/coc_update.php