Order Update
============

Description
-----------

This service is used to process orders.

Process
-------

#. The partner must be authenticated.
#. The partner send an XML which indicates the orders details he wants to update.
#. The partner receives a XML answer containing the orders details ant theirs status.

Limitation
----------

You can't update more than 25 orders in a row.

Request
=======

Root element
------------

* Name: orders_update
* Extends: :ref:`base_request`


Elements
--------

+------------------------+------------------------------------------------+-----------------------------+------------------+
|          Name          |                    Description                 |             Type            |    Occurrence    | 
+========================+================================================+=============================+==================+
| order                  | Orders to update                               | :ref:`order_update`         | 1-unbounded      |
+------------------------+------------------------------------------------+-----------------------------+------------------+

XML Sample
----------

.. code-block:: xml
   
   <orders_update xmlns="http://www.fnac.com/schemas/mp-dialog.xsd" shop_id="BBBFA40E-3A94-2EE1-762A-2858EDE4F9BB" partner_id="C906104B-9B13-611D-6104-261780F88E38" token="AE2F633D-48E4-CEB8-39C7-8713AAE28DE3">
     <order order_id="1P3HCS98E4QYE" action="accept_all_orders">
       <order_detail>
         <action><![CDATA[Accepted]]></action>
       </order_detail>
     </order>
   </orders_update>

Response
========

Root element
------------

* Name: orders_update_response
* Extends: :ref:`base_response`

Elements
--------

+------------------------+------------------------------------------------+-----------------------------+------------------+
|          Name          |                    Description                 |             Type            |    Occurrence    | 
+========================+================================================+=============================+==================+
| error                  | Errors                                         | :ref:`error`                | 0-unbounded      |
+------------------------+------------------------------------------------+-----------------------------+------------------+
| order                  | Orders updated                                 | :ref:`order_update_response`| 0-unbounded      |
+------------------------+------------------------------------------------+-----------------------------+------------------+

XML Sample
----------

.. code-block:: xml
   
   <orders_update_response xmlns="http://www.fnac.com/schemas/mp-dialog.xsd" status="OK">
     <order>
       <status>OK</status>
       <order_id>1P3HCS98E4QYE</order_id>
       <state>Accepted</state>
       <order_detail>
         <order_detail_id>1</order_detail_id>
         <status>OK</status>
         <state>Accepted</state>
       </order_detail>
     </order>
   </orders_update_response>

.. _order_update:

order_update
============

Attributes
----------

+------------------------+------------------------------------------------+----------------------------+----------+---------+
|          Name          |                    Description                 |            Type            | Required | Default |
+========================+================================================+============================+==========+=========+
| order_id               | Order's unique identifier from fnac            | :ref:`order_id_restriction`| required |         |
+------------------------+------------------------------------------------+----------------------------+----------+---------+
| action                 | Group action type for order detail action      | :ref:`order_update_action` | required |         |
+------------------------+------------------------------------------------+----------------------------+----------+---------+


Elements
--------

+------------------------+------------------------------------------------+-----------------------------+------------------+
|          Name          |                    Description                 |             Type            |    Occurrence    | 
+========================+================================================+=============================+==================+
| order_detail           | Orders details to update                       | :ref:`order_detail_update`  | 1-unbounded      |
+------------------------+------------------------------------------------+-----------------------------+------------------+

.. _order_detail_update:

order_detail_update
===================

Elements
--------

+------------------------+------------------------------------------------+----------------------------------+------------------+
|          Name          |                    Description                 |             Type                 |    Occurrence    | 
+========================+================================================+==================================+==================+
| order_detail_id        | Order detail unique identifier from fnac       | xs:positiveInteger               | 0-1              |
+------------------------+------------------------------------------------+----------------------------------+------------------+
| action                 | Action to do on order detail                   | :ref:`order_detail_state_update` | 1-1              |
+------------------------+------------------------------------------------+----------------------------------+------------------+
| tracking_number        | Order detail tracking number                   | xs:string                        | 0-1              |
+------------------------+------------------------------------------------+----------------------------------+------------------+
| tracking_company       | Order detail tracking company                  | :ref:`string100`                 | 0-1              |
+------------------------+------------------------------------------------+----------------------------------+------------------+

.. _order_update_response:

order_update_response
=====================

Elements
--------

+------------------------+------------------------------------------------+-----------------------------+------------------+
|          Name          |                    Description                 |             Type            |    Occurrence    | 
+========================+================================================+=============================+==================+
| status                 | Update status of order                         | :ref:`status_code`          | 1-1              |
+------------------------+------------------------------------------------+-----------------------------+------------------+
| order_id               | Order's unique identifier from fnac            | xs:string                   | 1-1              |
+------------------------+------------------------------------------------+-----------------------------+------------------+
| state                  | Order's state                                  | :ref:`order_state`          | 0-1              |
+------------------------+------------------------------------------------+-----------------------------+------------------+
| error                  | Errors                                         | :ref:`error`                | 0-unbounded      |
+------------------------+------------------------------------------------+-----------------------------+------------------+
| order_detail           | Orders details updated                         | :ref:`order_detail_response`| 1-unbounded      |
+------------------------+------------------------------------------------+-----------------------------+------------------+

.. _order_detail_response:

order_detail_response
=====================

Elements
--------

+------------------------+------------------------------------------------+-----------------------------+------------------+
|          Name          |                    Description                 |             Type            |    Occurrence    | 
+========================+================================================+=============================+==================+
| order_detail_id        | Order detail unique identifier from fnac       | xs:positiveInteger          | 1-1              |
+------------------------+------------------------------------------------+-----------------------------+------------------+
| status                 | Update status of order detail                  | :ref:`status_code`          | 1-1              |
+------------------------+------------------------------------------------+-----------------------------+------------------+
| state                  | Order detail state                             | :ref:`order_detail_state`   | 0-1              |
+------------------------+------------------------------------------------+-----------------------------+------------------+
| error                  | Errors                                         | :ref:`error`                | 0-unbounded      |
+------------------------+------------------------------------------------+-----------------------------+------------------+
