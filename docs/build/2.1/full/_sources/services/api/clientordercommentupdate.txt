.. contents::
   :depth: 3
   :backlinks: none

Client Order Comment Update
^^^^^^^^^^^^^^^^^^^^^^^^^^^

Informations
============

Description
-----------

Reply to client order comments.

Process
-------

#. A valid authentification token is needed.
#. The partner sends a list of comments ids to reply to with his own replies.
#. The service returns the processing results.

Request
=======

URL to use
----------

* https://partners-test.mp.fnacdarty.com/api.php/client_order_comments_update

Root element
------------

* Name: client_order_comments_update
* Extends: :ref:`base_request`

Elements
--------

+---------+-------------------+----------------+-------------+
| Name    | Description       | Type           | Occurrence  |
+=========+===================+================+=============+
| comment | Comments to reply | :ref:`comment` | 1-unbounded |
+---------+-------------------+----------------+-------------+

XML Sample
----------

.. code-block:: xml
   
   <client_order_comments_update xmlns="http://www.fnac.com/schemas/mp-dialog.xsd" shop_id="-3A94-2EE1-762A-2858EDE4F9BB" partner_id="-9B13-611D-6104-261780F88E38" token="-BB16-7228-8453-1C4EA5C89808">
     <comment id="474E54D6-E21E-F62E-0BE6-745FF40C5F2A">
       <comment_reply><![CDATA[My reply to the comment]]></comment_reply>
     </comment>
   </client_order_comments_update>


Response
========

Root element
------------

* Name: client_order_comments_update_response
* Extends: :ref:`base_response`

Elements
--------

+---------+------------------+-----------------------+-------------+-+
| Name    | Description      | Type                  | Occurrence  | |
+=========+==================+=======================+=============+=+
| error   | Errors           | :ref:`error`          | 0-unbounded | |
+---------+------------------+-----------------------+-------------+-+
| comment | Updated comments | :ref:`comment_update` | 0-unbounded | |
+---------+------------------+-----------------------+-------------+-+

XML Sample
----------

.. code-block:: xml
   
   <client_order_comments_update_response xmlns="http://www.fnac.com/schemas/mp-dialog.xsd" status="OK">
     <comment status="OK" id="474E54D6-E21E-F62E-0BE6-745FF40C5F2A"/>
   </client_order_comments_update_response>

.. _comment:

comment
=======

Attributes
----------

+------+-------------------------------------+-------------+----------+---------+
| Name | Description                         | Type        | Required | Default |
+======+=====================================+=============+==========+=========+
| id   | Comment unique identifier from fnac | :ref:`uuid` | required |         |
+------+-------------------------------------+-------------+----------+---------+


Elements
--------

+---------------+---------------+------------------+------------+
| Name          | Description   | Type             | Occurrence |
+===============+===============+==================+============+
| comment_reply | Comment reply | :ref:`string255` | 1-1        |
+---------------+---------------+------------------+------------+


.. _comment_update:

comment_update
==============

Attributes
----------

+--------+-------------------------------------+--------------------+----------+---------+
| Name   | Description                         | Type               | Required | Default |
+========+=====================================+====================+==========+=========+
| id     | Comment unique identifier from fnac | xs:string          | optional |         |
+--------+-------------------------------------+--------------------+----------+---------+
| status | Status of comment update            | :ref:`status_code` | required |         |
+--------+-------------------------------------+--------------------+----------+---------+


Elements
--------

+-------+-------------+--------------+-------------+
| Name  | Description | Type         | Occurrence  |
+=======+=============+==============+=============+
| error | Errors      | :ref:`error` | 0-unbounded |
+-------+-------------+--------------+-------------+
