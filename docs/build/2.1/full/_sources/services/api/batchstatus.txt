.. contents::
   :depth: 3
   :backlinks: none

.. _batch_status_service:

Batch Status
^^^^^^^^^^^^

Informations
============

Description
-----------

This service is used to get the processing report of your batch imports.

Process
-------

#. A valid authentification token is needed.
#. The partner calls the service with a valid batch id (given in response of offers_update service).
#. The service returns a report of the batch if has been processed, or the only batch id and its current status if it is not.

Request
=======

URL to use
----------

* https://partners-test.mp.fnacdarty.com/api.php/batch_status

Root element
------------

* Name: batch_status
* Extends: :ref:`base_request`

Elements
--------

+----------+-----------------------------------+-----------+------------+
| Name     | Description                       | Type      | Occurrence |
+==========+===================================+===========+============+
| batch_id | Batch unique identifier from fnac | xs:string | 1-1        |
+----------+-----------------------------------+-----------+------------+

XML Sample
----------

.. code-block:: xml
   
   <batch_status xmlns="http://www.fnac.com/schemas/mp-dialog.xsd" shop_id="-4AC1-6B59-1AB5-98462FB4B3B1" partner_id="-945A-527F-3D1C-3C1D353A49D5" token="-1E88-2A96-7BAE-398CEA69FB7A">
      <batch_id><![CDATA[73169053-DD44-82E4-D3E7-E298D1F27B13]]></batch_id>
   </batch_status>


Response
========

Root element
------------

* Name: batch_status_response
* Extends: :ref:`base_response`

Elements
--------

+----------+-----------------------------------+------------------------------+-------------+-+
| Name     | Description                       | Type                         | Occurrence  | |
+==========+===================================+==============================+=============+=+
| batch_id | Batch unique identifier from fnac | xs:string                    | 0-1         | |
+----------+-----------------------------------+------------------------------+-------------+-+
| error    | Error                             | :ref:`error`                 | 0-unbounded | |
+----------+-----------------------------------+------------------------------+-------------+-+
| offer    | Updated offers                    | :ref:`offer_status_response` | 0-unbounded | |
+----------+-----------------------------------+------------------------------+-------------+-+

XML Sample
----------

.. code-block:: xml
   
   <batch_status_response xmlns="http://www.fnac.com/schemas/mp-dialog.xsd" status="ACTIVE">
     <batch_id>73169053-DD44-82E4-D3E7-E298D1F27B13</batch_id>
   </batch_status_response>
   

.. code-block:: xml

   <batch_status_response xmlns="http://www.fnac.com/schemas/mp-dialog.xsd" status="OK">
     <batch_id>73169053-DD44-82E4-D3E7-E298D1F27B13</batch_id>
     <offer status="OK">
       <product_fnac_id><![CDATA[1468739]]></product_fnac_id>
       <offer_fnac_id>53E97F8F-9DE2-B495-B3C1-97DD3D083A90</offer_fnac_id>
       <offer_seller_id>MYSKU_1</offer_seller_id>
     </offer>
     <offer status="OK">
       <product_fnac_id><![CDATA[1468740]]></product_fnac_id>
       <offer_fnac_id>6DA9D41D-D3BE-2F75-B033-6CB1B2810F1F</offer_fnac_id>
       <offer_seller_id>MYSKU_2</offer_seller_id>
     </offer>
     <offer status="OK">
       <product_fnac_id><![CDATA[2065058]]></product_fnac_id>
       <offer_fnac_id>8ED85524-CF68-B037-FF2D-45C52A32DE02</offer_fnac_id>
       <offer_seller_id>MYSKU_3</offer_seller_id>
     </offer>
   </batch_status_response>

.. _offer_status_response:

Offer Status Response
=====================

Attributes
----------

+--------+------------------------+--------------------+----------+---------+
| Name   | Description            | Type               | Required | Default |
+========+========================+====================+==========+=========+
| status | Status of offer update | :ref:`status_code` | required |         |
+--------+------------------------+--------------------+----------+---------+

Elements
--------

+-----------------+---------------------------------------+--------------+-------------+-+
| Name            | Description                           | Type         | Occurrence  | |
+=================+=======================================+==============+=============+=+
| product_fnac_id | Product unique identifier from fnac   | xs:string    | 0-1         | |
+-----------------+---------------------------------------+--------------+-------------+-+
| offer_fnac_id   | Offer unique identifier from fnac     | xs:string    | 0-1         | |
+-----------------+---------------------------------------+--------------+-------------+-+
| offer_seller_id | Offer unique identifier from seller   | xs:string    | 0-1         | |
+-----------------+---------------------------------------+--------------+-------------+-+
| error           | Error description when updating offer | :ref:`error` | 0-unbounded | |
+-----------------+---------------------------------------+--------------+-------------+-+
