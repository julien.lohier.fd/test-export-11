#!/bin/bash

#Global var
SVN_API_HOST="http://fcxdevmpsrv1/svn/fnac-marketplace/api"

SERVER_DIR="/home/devadmin/mp.partners.fnac.dev/web/docs/api"
SERVER_NAME="fcxrecmpweb1"
SERVER_USER="devadmin"

VERSION="2.4"

OUTPUT_BEAUTIFUL_DIR="fnac-marketplace-api-$VERSION"
OUTPUT_DIR="marketplace-php-client"
#Output dir

echo "ATTENTION : Cette tâche va supprimer toutes les modifications locales sur le répertoire courant après son execution, êtes vous sur de vouloir continuer : [Y/N] Default N :"
read -e REPONSE

if [ $REPONSE != "Y" ]
then
  exit
fi

TYPE="full"
echo "Quel est le type de la documentation ? [full/standard] (full) :"
read -e TYPE

#Init options
HAS_MESSAGE="TRUE"
HAS_INCIDENT_UPDATE="TRUE"
HAS_PRICING="TRUE"

#echo "Inclure API de messagerie ? [Y/N] (Y)"
#read -e REPONSE_MESSAGE
#if [ $REPONSE_MESSAGE != "Y" ]
#then
#  HAS_MESSAGE="FALSE"
#fi

#echo "Inclure API des incidents ? [Y/N] (Y)"
#read -e REPONSE_INCIDENT_UPDATE
#if [ $REPONSE_INCIDENT_UPDATE != "Y" ]
#then
#  HAS_INCIDENT_UPDATE="FALSE"
#fi

#echo "Inclure API de pricing ? [Y/N] (N)"
#read -e REPONSE_PRICING
#if [ $REPONSE_PRICING != "N" ]
#then
#  HAS_PRICING="TRUE"
#fi

#Select patch to apply
#PATCH_NAME="remove"

#Merging the dir we want
#if [ $HAS_MESSAGE == "FALSE" ]
#then  
#  PATCH_NAME=$PATCH_NAME"-message"
#fi
#if [ $HAS_PRICING == "FALSE" ]
#then  
#  PATCH_NAME=$PATCH_NAME"-pricing"
#fi
#if [ $HAS_INCIDENT_UPDATE == "FALSE" ]
#then  
#  PATCH_NAME=$PATCH_NAME"-incident_update"
#fi

#if [ $PATCH_NAME != "remove" ]
#then
#  PATCH_NAME=$PATCH_NAME".patch"
#  if [ -e "patch/"$PATCH_NAME ]
#  then
#	echo "Patching with "$PATCH_NAME
#    patch -p0 --merge -Ei "patch/"$PATCH_NAME
#  else
#    echo "Patch : patch/"$PATCH_NAME" does not exist you must create it before continue"
#    exit
#  fi
#fi

#Export php-client
mkdir -p $OUTPUT_DIR
cp -R php-client/* $OUTPUT_DIR
find $OUTPUT_DIR -name ".svn" -type d -exec rm -rf {} \;
tar -cvzf marketplace-client-last.tgz $OUTPUT_DIR

#Generate xsd archive
tar -cvzf fnac_mp_api_xsd.tgz php-client/src/FnacApiClient/Resources/xsd

#Build doc
cd docs
make clean
make html
cd ..

#Copy client to doc
mkdir -p docs/_build/html/download
mv marketplace-client-last.tgz docs/_build/html/download/fnacapiclient.tgz

#Copy xsd archive
mv fnac_mp_api_xsd.tgz docs/_build/html/download/fnac_mp_api_xsd.tgz

#Build client doc
mkdir -p docs/_build/html/apiclient
cd phpdoctor
php phpdoc.php
cd ..

#Remove client dir
rm -rf $OUTPUT_DIR

#Creation du paquet
PACKAGE_NAME="fnac-marketplace-api"
if [ $HAS_MESSAGE == "TRUE" ]
then
  PACKAGE_NAME=$PACKAGE_NAME"-message"
fi
if [ $HAS_PRICING == "TRUE" ]
then
  PACKAGE_NAME=$PACKAGE_NAME"-pricing"
fi
if [ $HAS_INCIDENT_UPDATE == "TRUE" ]
then
  PACKAGE_NAME=$PACKAGE_NAME"-incident_update"
fi

mkdir $PACKAGE_NAME
cp -R docs/_build/html/* $PACKAGE_NAME/

#Synchronise
FINAL_DIR=$SERVER_DIR"/"$VERSION
rsync -r -a -v -e "ssh -l $SERVER_USER" --delete $PACKAGE_NAME $SERVER_NAME:$FINAL_DIR

rm -rf $PACKAGE_NAME

#Rename folder
cd $FINAL_DIR
rm -rf $TYPE
mv $PACKAGE_NAME/ $TYPE
cd /home/devadmin/api

echo "Copying files to front"
cp -R php-client/docs/html/apiclient/* $FINAL_DIR"/"$TYPE"/apiclient"

#Updating version warning banner
for file in $(find $SERVER_DIR -type f -name theme_extras.js)
do
   sed -i "s/var current_version = '.*';/var current_version = '$VERSION';/g" $file
done

#Generate pdf doc
echo "Generating pdf file"
cd docs
make latexpdf
mkdir -p $FINAL_DIR"/"$TYPE"/download"
cp _build/latex/FnacMarketplaceAPI.pdf $FINAL_DIR"/"$TYPE"/download/FnacMarketplaceAPI-Documentation.pdf"
cd /home/devadmin/api

#Revert svn
svn revert --depth infinity .
